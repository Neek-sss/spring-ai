extern crate proc_macro;
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, AttributeArgs, ItemFn, Lit, NestedMeta, ReturnType};

#[proc_macro_attribute]
pub fn ai_func(args: TokenStream, input: TokenStream) -> TokenStream {
    let args = parse_macro_input!(args as AttributeArgs);
    let input = parse_macro_input!(input as ItemFn);

    let kind = if let NestedMeta::Lit(Lit::Str(kind)) = &args[0] {
        kind.value()
    } else {
        String::new()
    };

    let func_name = input.sig.ident.clone();
    let func_body = input.block.clone();

    let expanded = if kind == "init" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::InitWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "release" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::ReleaseWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "message" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let player_arg = input.sig.inputs[2].clone();
        let message_arg = input.sig.inputs[3].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::MessageWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #player_arg, #message_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "command_finished" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let command_id_arg = input.sig.inputs[3].clone();
        let command_topic_arg = input.sig.inputs[4].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::CommandFinishedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #command_id_arg, #command_topic_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_created" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyCreatedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_damaged" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();
        let attacker_id_arg = input.sig.inputs[3].clone();
        let direction_arg = input.sig.inputs[4].clone();
        let weapon_def_id_arg = input.sig.inputs[5].clone();
        let paralyzer_arg = input.sig.inputs[6].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyDamagedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg, #attacker_id_arg, #direction_arg, #weapon_def_id_arg, #paralyzer_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_destroyed" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();
        let attacker_id_arg = input.sig.inputs[3].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyDestroyedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg, #attacker_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_enter_los" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyEnterLOSWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_enter_radar" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyEnterRadarWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_finished" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyFinishedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_leave_los" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyLeaveLOSWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "enemy_leave_radar" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::enemy::EnemyLeaveRadarWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "load" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let enemy_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::LoadWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #enemy_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "lua_message" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let message_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::LuaMessageWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #message_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "null" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::NullWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "player_command" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_ids_arg = input.sig.inputs[2].clone();
        let command_topic_id_arg = input.sig.inputs[3].clone();
        let player_id_arg = input.sig.inputs[4].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::PlayerCommandWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_ids_arg, #command_topic_id_arg, #player_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "save" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let file_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::SaveFuncWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #file_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "seismic_ping" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let position_arg = input.sig.inputs[2].clone();
        let strength_arg = input.sig.inputs[3].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::SeismicPingWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #position_arg, #strength_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_captured" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let old_team_id_arg = input.sig.inputs[3].clone();
        let new_team_id_arg = input.sig.inputs[4].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitCapturedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #old_team_id_arg, #new_team_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_created" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let builder_id_arg = input.sig.inputs[3].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitCreatedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #builder_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_damaged" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let attacker_id_arg = input.sig.inputs[3].clone();
        let damage_arg = input.sig.inputs[4].clone();
        let direction_arg = input.sig.inputs[5].clone();
        let weapon_def_id_arg = input.sig.inputs[6].clone();
        let paralyzer_arg = input.sig.inputs[7].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitDamagedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #attacker_id_arg, #damage_arg, #direction_arg, #weapon_def_id_arg, #paralyzer_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_destroyed" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let attacker_id_arg = input.sig.inputs[3].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitDestroyedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #attacker_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_finished" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitFinishedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_given" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let old_team_id_arg = input.sig.inputs[3].clone();
        let new_team_id_arg = input.sig.inputs[4].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitGivenWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #old_team_id_arg, #new_team_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_idle" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitIdleWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "unit_move_failed" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::unit::UnitMoveFailedWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "update" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let frame_arg = input.sig.inputs[2].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::UpdateWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #frame_arg) -> #output {
                #func_body
            }
        }
    } else if kind == "weapon_fired" {
        let logger_arg = input.sig.inputs[0].clone();
        let interface_arg = input.sig.inputs[1].clone();
        let unit_id_arg = input.sig.inputs[2].clone();
        let weapon_def_id_arg = input.sig.inputs[3].clone();

        let output = if let ReturnType::Type(_, func_return_type) = input.sig.output {
            func_return_type
        } else {
            unreachable!()
        };

        quote! {
            spring_ai_rs::inventory::submit! {spring_ai_rs::event::other::WeaponFiredWrapper::new(#func_name)}

            fn #func_name(#logger_arg, #interface_arg, #unit_id_arg, #weapon_def_id_arg) -> #output {
                #func_body
            }
        }
    } else {
        panic!("Not one of the listed function types");
    };

    TokenStream::from(expanded)
}
