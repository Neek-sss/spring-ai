#include <ExternalAI/Interface/aidefines.h>
#include <ExternalAI/Interface/AISCommands.h>
#include <ExternalAI/Interface/AISEvents.h>
#include <ExternalAI/Interface/ELevelOfSupport.h>
#include <ExternalAI/Interface/SAIInterfaceCallback.h>
#include <ExternalAI/Interface/SAIInterfaceLibrary.h>
#include <ExternalAI/Interface/SSkirmishAICallback.h>
#include <ExternalAI/Interface/SSkirmishAILibrary.h>
