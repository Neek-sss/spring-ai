extern crate bindgen;

use std::{fs::remove_dir_all, path::PathBuf};

use git2::{Repository, ResetType};

fn main() {
    let spring_url = "https://github.com/beyond-all-reason/spring.git";
    let commit_hash = "3d35645";

    {
        let repo = Repository::clone(spring_url, "spring").expect("failed to clone spring repo");

        let commit = repo
            .find_commit_by_prefix(commit_hash)
            .expect("Could not find commit");
        repo.reset(commit.as_object(), ResetType::Hard, None)
            .expect("Could not set repository to commit");
    }

    let bindings = bindgen::Builder::default()
        // The input headers we would like to generate bindings for
        .header("src/wrapper.h")
        .clang_arg("-Ispring/rts")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from("out");
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    remove_dir_all("spring").expect("Could not remove spring directory");
}
