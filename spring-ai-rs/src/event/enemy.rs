use std::error::Error;

use slog::{trace, Logger};
use spring_ai_sys::{
    SEnemyCreatedEvent, SEnemyDamagedEvent, SEnemyDestroyedEvent, SEnemyEnterLOSEvent,
    SEnemyEnterRadarEvent, SEnemyFinishedEvent, SEnemyLeaveLOSEvent, SEnemyLeaveRadarEvent,
};

use crate::{
    ai_interface::{
        callback::{unit::Unit, weapon_def::WeaponDef},
        AIInterface,
    },
    event::void_to_event,
};

pub struct EnemyCreatedWrapper {
    pub enemy_created_func: EnemyCreatedFuncType,
}

impl EnemyCreatedWrapper {
    pub const fn new(enemy_created_func: EnemyCreatedFuncType) -> Self {
        Self { enemy_created_func }
    }
}

inventory::collect!(EnemyCreatedWrapper);

pub type EnemyCreatedFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_created_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_created_func: &EnemyCreatedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_created_data =
        void_to_event::<SEnemyCreatedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_created_data,);

    enemy_created_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_created_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub struct EnemyDamagedWrapper {
    pub enemy_damaged_func: EnemyDamagedFuncType,
}

impl EnemyDamagedWrapper {
    pub const fn new(enemy_damaged_func: EnemyDamagedFuncType) -> Self {
        Self { enemy_damaged_func }
    }
}

inventory::collect!(EnemyDamagedWrapper);

pub type EnemyDamagedFuncType =
    fn(&Logger, AIInterface, Unit, Unit, [f32; 3], WeaponDef, bool) -> Result<(), Box<dyn Error>>;

pub fn enemy_damaged_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_damaged_func: &EnemyDamagedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_damaged_data =
        void_to_event::<SEnemyDamagedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_damaged_data,);
    let direction = unsafe { Vec::from_raw_parts(enemy_damaged_data.dir_posF3, 3, 3) };

    enemy_damaged_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_damaged_data.enemy,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: enemy_damaged_data.attacker,
            ai_id: skirmish_ai_id,
        },
        [direction[0], direction[1], direction[2]],
        WeaponDef {
            weapon_def_id: enemy_damaged_data.weaponDefId,
            ai_id: skirmish_ai_id,
        },
        enemy_damaged_data.paralyzer,
    )
}

pub struct EnemyDestroyedWrapper {
    pub enemy_destroyed_func: EnemyDestroyedFuncType,
}

impl EnemyDestroyedWrapper {
    pub const fn new(enemy_destroyed_func: EnemyDestroyedFuncType) -> Self {
        Self {
            enemy_destroyed_func,
        }
    }
}

inventory::collect!(EnemyDestroyedWrapper);

pub type EnemyDestroyedFuncType =
    fn(&Logger, AIInterface, Unit, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_destroyed_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_destroyed_func: &EnemyDestroyedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_destroyed_data =
        void_to_event::<SEnemyDestroyedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_destroyed_data,);

    enemy_destroyed_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_destroyed_data.enemy,
            ai_id: skirmish_ai_id,
        },
        Unit {
            unit_id: enemy_destroyed_data.attacker,
            ai_id: skirmish_ai_id,
        },
    )
}

pub struct EnemyEnterLOSWrapper {
    pub enemy_enter_los_func: EnemyEnterLOSFuncType,
}

impl EnemyEnterLOSWrapper {
    pub const fn new(enemy_enter_los_func: EnemyEnterLOSFuncType) -> Self {
        Self {
            enemy_enter_los_func,
        }
    }
}

inventory::collect!(EnemyEnterLOSWrapper);

pub type EnemyEnterLOSFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_enter_los_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_enter_los_func: &EnemyEnterLOSFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_enter_los_data =
        void_to_event::<SEnemyEnterLOSEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_enter_los_data,);

    enemy_enter_los_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_enter_los_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub struct EnemyEnterRadarWrapper {
    pub enemy_enter_radar_func: EnemyEnterRadarFuncType,
}

impl EnemyEnterRadarWrapper {
    pub const fn new(enemy_enter_radar_func: EnemyEnterRadarFuncType) -> Self {
        Self {
            enemy_enter_radar_func,
        }
    }
}

inventory::collect!(EnemyEnterRadarWrapper);

pub type EnemyEnterRadarFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_enter_radar_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_enter_radar_func: &EnemyEnterRadarFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_enter_radar_data =
        void_to_event::<SEnemyEnterRadarEvent>(data as *mut libc::c_void).unwrap();

    trace!(
        logger,
        "ID: {}; {:?}",
        skirmish_ai_id,
        enemy_enter_radar_data,
    );

    enemy_enter_radar_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_enter_radar_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub struct EnemyFinishedWrapper {
    pub enemy_finished_func: EnemyFinishedFuncType,
}

impl EnemyFinishedWrapper {
    pub const fn new(enemy_finished_func: EnemyFinishedFuncType) -> Self {
        Self {
            enemy_finished_func,
        }
    }
}

inventory::collect!(EnemyFinishedWrapper);

pub type EnemyFinishedFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_finished_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_finished_func: &EnemyFinishedFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_finished_data =
        void_to_event::<SEnemyFinishedEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_finished_data,);

    enemy_finished_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_finished_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub struct EnemyLeaveLOSWrapper {
    pub enemy_leave_los_func: EnemyLeaveLOSFuncType,
}

impl EnemyLeaveLOSWrapper {
    pub const fn new(enemy_leave_los_func: EnemyLeaveLOSFuncType) -> Self {
        Self {
            enemy_leave_los_func,
        }
    }
}

inventory::collect!(EnemyLeaveLOSWrapper);

pub type EnemyLeaveLOSFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_leave_los_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_leave_los_func: &EnemyLeaveLOSFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_leave_los_data =
        void_to_event::<SEnemyLeaveLOSEvent>(data as *mut libc::c_void).unwrap();

    trace!(logger, "ID: {}; {:?}", skirmish_ai_id, enemy_leave_los_data,);

    enemy_leave_los_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_leave_los_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}

pub struct EnemyLeaveRadarWrapper {
    pub enemy_leave_radar_func: EnemyLeaveRadarFuncType,
}

impl EnemyLeaveRadarWrapper {
    pub const fn new(enemy_leave_radar_func: EnemyLeaveRadarFuncType) -> Self {
        Self {
            enemy_leave_radar_func,
        }
    }
}

inventory::collect!(EnemyLeaveRadarWrapper);

pub type EnemyLeaveRadarFuncType = fn(&Logger, AIInterface, Unit) -> Result<(), Box<dyn Error>>;

pub fn enemy_leave_radar_wrapper(
    logger: &Logger,
    skirmish_ai_id: libc::c_int,
    data: *const libc::c_void,
    enemy_leave_radar_func: &EnemyLeaveRadarFuncType,
) -> Result<(), Box<dyn Error>> {
    let enemy_leave_radar_data =
        void_to_event::<SEnemyLeaveRadarEvent>(data as *mut libc::c_void).unwrap();

    trace!(
        logger,
        "ID: {}; {:?}",
        skirmish_ai_id,
        enemy_leave_radar_data,
    );

    enemy_leave_radar_func(
        logger,
        AIInterface::new(skirmish_ai_id),
        Unit {
            unit_id: enemy_leave_radar_data.enemy,
            ai_id: skirmish_ai_id,
        },
    )
}
