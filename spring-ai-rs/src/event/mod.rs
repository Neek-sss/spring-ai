use std::{
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
};
pub mod enemy;
pub mod event_topic;
pub mod other;
pub mod unit;

#[derive(Copy, Clone, Debug)]
pub struct EventError {}

impl Error for EventError {}

impl Display for EventError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{:?}", self)
    }
}

pub fn void_to_event<'a, T>(data: *mut libc::c_void) -> Result<&'a T, Box<dyn Error>> {
    unsafe { (data as *mut T).as_ref() }.ok_or(Box::new(EventError {}))
}
