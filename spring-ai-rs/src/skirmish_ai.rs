use std::{
    collections::HashMap,
    ptr::NonNull,
    sync::{Arc, RwLock},
};

use lazy_static::lazy_static;
pub use spring_ai_sys::SSkirmishAICallback;

#[derive(Copy, Clone, Debug, Default)]
pub struct SkirmishAI {
    callback: usize,
}

impl SkirmishAI {
    pub fn new(callback: *const SSkirmishAICallback) -> Result<Self, &'static str> {
        if NonNull::new(callback as *mut SSkirmishAICallback).is_some() {
            Ok(Self {
                callback: callback as usize,
            })
        } else {
            Err("Callback is null")
        }
    }

    pub fn get_callback(&self) -> Option<NonNull<SSkirmishAICallback>> {
        NonNull::new(self.callback as *mut SSkirmishAICallback)
    }
}

lazy_static! {
    static ref SKIRMISH_AI: Arc<RwLock<HashMap<i32, SkirmishAI>>> = Default::default();
}

pub fn set_skirmish_ai(ai_id: i32, skirmish_ai: SkirmishAI) {
    SKIRMISH_AI.try_write().unwrap().insert(ai_id, skirmish_ai);
}

pub fn get_skirmish_ai(ai_id: i32) -> Option<SkirmishAI> {
    SKIRMISH_AI.try_read().unwrap().get(&ai_id).copied()
}

pub fn remove_skirmish_ai(ai_id: i32) {
    SKIRMISH_AI.try_write().unwrap().remove(&ai_id);
}
