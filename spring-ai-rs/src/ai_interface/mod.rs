pub mod callback;
mod memory;

#[derive(Debug, Copy, Clone)]
pub struct AIInterface {
    pub ai_id: i32,
}

impl AIInterface {
    pub fn new(ai_id: i32) -> Self {
        Self { ai_id }
    }
}
