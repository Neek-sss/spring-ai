use std::{error::Error, ffi::CStr};

use crate::get_callback;

const MAX_PARAMETERS: usize = 32;

#[derive(Debug, Copy, Clone)]
pub struct GroupSupportedCommand {
    pub ai_id: i32,
    pub group_id: i32,
    pub supported_command_index: i32,
}

#[derive(Debug, Clone)]
pub struct GroupSupportedCommandAll {
    id: i32,
    name: String,
    tooltip: String,
    is_show_unique: bool,
    is_disabled: bool,
    parameters: String,
}

impl GroupSupportedCommand {
    pub fn id(&self) -> Result<i32, Box<dyn Error>> {
        let get_supported_command_id_func =
            get_callback!(self.ai_id, Group_SupportedCommand_getId)?;

        Ok(unsafe {
            get_supported_command_id_func(self.ai_id, self.group_id, self.supported_command_index)
        })
    }

    pub fn name(&self) -> Result<String, Box<dyn Error>> {
        let get_supported_command_name_func =
            get_callback!(self.ai_id, Group_SupportedCommand_getName)?;

        Ok(String::from(
            unsafe {
                CStr::from_ptr(get_supported_command_name_func(
                    self.ai_id,
                    self.group_id,
                    self.supported_command_index,
                ))
            }
            .to_str()?,
        ))
    }

    pub fn tooltip(&self) -> Result<String, Box<dyn Error>> {
        let get_supported_command_tooltip_func =
            get_callback!(self.ai_id, Group_SupportedCommand_getToolTip)?;

        Ok(String::from(
            unsafe {
                CStr::from_ptr(get_supported_command_tooltip_func(
                    self.ai_id,
                    self.group_id,
                    self.supported_command_index,
                ))
            }
            .to_str()?,
        ))
    }

    pub fn is_show_unique(&self) -> Result<bool, Box<dyn Error>> {
        let get_suppored_command_is_show_unique_func =
            get_callback!(self.ai_id, Group_SupportedCommand_isShowUnique)?;

        Ok(unsafe {
            get_suppored_command_is_show_unique_func(
                self.ai_id,
                self.group_id,
                self.supported_command_index,
            )
        })
    }

    pub fn is_disabled(&self) -> Result<bool, Box<dyn Error>> {
        let get_supported_command_is_disabled_func =
            get_callback!(self.ai_id, Group_SupportedCommand_isDisabled)?;

        Ok(unsafe {
            get_supported_command_is_disabled_func(
                self.ai_id,
                self.group_id,
                self.supported_command_index,
            )
        })
    }

    pub fn parameters(&self) -> Result<String, Box<dyn Error>> {
        let get_supported_command_parameters_func =
            get_callback!(self.ai_id, Group_SupportedCommand_getParams)?;

        let s = String::with_capacity(MAX_PARAMETERS);
        let ptr = &mut (s.as_ptr() as *const i8);
        unsafe {
            get_supported_command_parameters_func(
                self.ai_id,
                self.group_id,
                self.supported_command_index,
                ptr as *mut *const i8,
                MAX_PARAMETERS as i32,
            )
        };

        Ok(String::from(
            unsafe { CStr::from_ptr(*ptr) }.to_str().unwrap_or(""),
        ))
    }

    pub fn all(&self) -> Result<GroupSupportedCommandAll, Box<dyn Error>> {
        Ok(GroupSupportedCommandAll {
            id: self.id()?,
            name: self.name()?,
            tooltip: self.tooltip()?,
            is_show_unique: self.is_show_unique()?,
            is_disabled: self.is_disabled()?,
            parameters: self.parameters()?,
        })
    }
}
