use std::{
    collections::{HashMap, HashSet},
    error::Error,
};

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            group::GroupAddUnitCommandData,
            unit::{
                AiSelectUnitCommandData, AttackAreaUnitCommandData, AttackUnitCommandData,
                BuildUnitCommandData, CaptureAreaUnitCommanddData, CaptureUnitCommanddData,
                CloakUnitCommandData, CustomUnitCommanddData, DGunUnitCommandData,
                FightUnitCommandData, GuardUnitCommandData, LoadOntoUnitCommandData,
                LoadUnitsAreaCommandData, LoadUnitsUnitCommandData, MoveUnitCommandData,
                PatrolUnitCommandData, ReclaimAreaUnitCommandData, ReclaimFeatureUnitCommandData,
                ReclaimUnitUnitCommandData, RepairUnitCommandData, RestoreAreaUnitCommandData,
                ResurrectAreaUnitCommandData, ResurrectUnitCommandData, SelfDestroyUnitCommandData,
                SetAutoRepairLevelUnitCommandData, SetBaseUnitCommandData, SetFireUnitCommandData,
                SetIdleModeCommandData, SetMoveStateCommandData, SetUnitOnOffCommandData,
                SetUnitRepeatCommandData, SetUnitStockpileCommandData, SetUnitStopCommandData,
                SetUnitTrajectoryCommandData, SetUnitUnloadAreaCommandData,
                SetUnitUnloadCommandData, SetUnitWaitCommandData, SetUnitWaitGatherCommandData,
                SetUnitWaitSquadCommandData, SetUnitWaitTimeCommandData,
            },
            CommandData,
        },
        command_topic::CommandTopic,
        options::UnitCommandOptions,
    },
    engine::handle_command,
    facing::Facing,
    feature::Feature,
    fire_state::FireState,
    group::{Group, GROUP_UNITS, GROUP_UNIT_DEFS},
    idle_mode::IdleMode,
    move_state::MoveState,
    position::Position,
    trajectory::Trajectory,
    unit::Unit,
    unit_def::UnitDef,
};

// impl AIInterface {
//     pub fn create_group(&self) -> Result<Group, String> {
//         Group::create(self.ai_id)
//     }
// }

impl Group {
    pub fn add_unit(
        &mut self,
        unit: Unit,
        options: Vec<UnitCommandOptions>,
        timeout: Option<i32>,
    ) -> Result<(), Box<dyn Error>> {
        let mut command_c_data = GroupAddUnitCommandData {
            unit_id: unit.unit_id,
            group_id: -1,
            options,
            timeout: timeout.unwrap_or(i32::MAX),
            to_group_id: self.group_id,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitGroupAdd.into(),
            &mut command_c_data,
        )?;

        {
            let mut group_units_lock = GROUP_UNITS.try_write()?;
            let ai_group_units = &mut group_units_lock.get_mut(&self.ai_id).unwrap();
            let group_units = ai_group_units
                .entry(self.group_id)
                .or_insert(HashSet::new());
            group_units.insert(unit.unit_id);
        }

        {
            let mut group_unit_defs_lock = GROUP_UNIT_DEFS.try_write()?;
            let ai_group_unit_defs = &mut group_unit_defs_lock.get_mut(&self.ai_id).unwrap();
            let group_unit_defs = ai_group_unit_defs
                .entry(self.group_id)
                .or_insert(HashMap::new());
            *group_unit_defs.entry(unit.unit_def()?.def_id).or_insert(0) += 1;
        }

        Ok(())
    }

    // fn create(ai_id: i32) -> Result<Group, String> {
    //     let mut command_c_data = CreateGroupCommandData {}.c_data();
    //
    //     handle_command(
    //         ai_id,
    //         COMMAND_TO_ID_ENGINE,
    //         -1,
    //         CommandTopic::GroupCreate.into(),
    //         &mut command_c_data,
    //     )?;
    //
    //     Ok(Group {
    //         ai_id,
    //         group_id: command_c_data.ret_groupId,
    //     })
    // }
    //
    // pub fn erase(self) -> Result<(), Box<dyn Error>> {
    //     let mut command_c_data = EraseGroupCommandData {
    //         group_id: self.group_id,
    //     }
    //     .c_data();
    //
    //     handle_command(
    //         self.ai_id,
    //         COMMAND_TO_ID_ENGINE,
    //         -1,
    //         CommandTopic::GroupErase.into(),
    //         &mut command_c_data,
    //     )?;
    //
    //     {
    //         let mut group_units_lock = GROUP_UNITS.try_write()?;
    //         let ai_group_units = &mut group_units_lock.get_mut(&self.ai_id).unwrap();
    //         ai_group_units.remove(&self.group_id);
    //     }
    //
    //     {
    //         let mut group_unit_defs_lock = GROUP_UNIT_DEFS.try_write()?;
    //         let ai_group_unit_defs = &mut group_unit_defs_lock.get_mut(&self.ai_id).unwrap();
    //         ai_group_unit_defs.remove(&self.group_id);
    //     }
    //
    //     Ok(())
    // }
    //
    // pub fn clear_unit(
    //     &mut self,
    //     unit: Unit,
    //     options: Vec<UnitCommandOptions>,
    //     timeout: Option<i32>,
    // ) -> Result<(), Box<dyn Error>> {
    //     let mut command_c_data = GroupClearUnitCommandData {
    //         unit_id: unit.unit_id,
    //         group_id: -1,
    //         options,
    //         timeout: timeout.unwrap_or(i32::MAX),
    //     }
    //     .c_data();
    //
    //     handle_command(
    //         self.ai_id,
    //         COMMAND_TO_ID_ENGINE,
    //         -1,
    //         CommandTopic::UnitGroupClear.into(),
    //         &mut command_c_data,
    //     )?;
    //
    //     {
    //         let mut group_units_lock = GROUP_UNITS.try_write()?;
    //         let ai_group_units = &mut group_units_lock.get_mut(&self.ai_id).unwrap();
    //         let group_units = ai_group_units
    //             .entry(self.group_id)
    //             .or_insert(HashSet::new())
    //             .remove(&unit.unit_id);
    //     }
    //
    //     {
    //         let mut group_unit_defs_lock = GROUP_UNIT_DEFS.try_write()?;
    //         let ai_group_unit_defs = &mut group_unit_defs_lock.get_mut(&self.ai_id).unwrap();
    //         let group_unit_defs = ai_group_unit_defs
    //             .entry(self.group_id)
    //             .or_insert(HashMap::new());
    //         let group_def_count = group_unit_defs.entry(unit.unit_def()?.def_id).or_insert(1);
    //         if *group_def_count == 1 {
    //             group_unit_defs.remove(&unit.unit_def()?.def_id);
    //         } else {
    //             *group_def_count -= 1;
    //         }
    //     }
    //
    //     Ok(())
    // }

    pub fn select(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = AiSelectUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitAISelect.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn attack(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = AttackUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            target_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitAttack.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn attack_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = AttackAreaUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            attack_position: position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitAttackArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn build<P: Position>(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        to_build_unit_def_id: UnitDef,
        position: P,
        facing: Facing,
    ) -> Result<(), Box<dyn Error>> {
        let mut command_data = BuildUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_build_unit_def_id: to_build_unit_def_id.def_id,
            build_position: position.to_f32_array(self.ai_id)?,
            facing,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitBuild.into(),
            &mut command_data.c_data(),
        )?;

        Ok(())
    }

    pub fn capture(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = CaptureUnitCommanddData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_capture_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitCapture.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn capture_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = CaptureAreaUnitCommanddData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitCaptureArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn cloak(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        cloak: bool,
    ) -> Result<(), &'static str> {
        let mut command_data = CloakUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            cloak,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitCloak.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn custom_command(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        command_id: i32,
        parameters: Vec<f32>,
    ) -> Result<(), &'static str> {
        let mut command_data = CustomUnitCommanddData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            command_id,
            parameters,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitCustom.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn dgun_unit(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = DGunUnitCommandData {
            unitId: -1,
            groupId: self.group_id,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            toAttackUnitId: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitDGun.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn fight(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
    ) -> Result<(), &'static str> {
        let mut command_data = FightUnitCommandData {
            unitId: -1,
            groupId: self.group_id,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            position,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitFight.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn guard(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = GuardUnitCommandData {
            unitId: -1,
            groupId: self.group_id,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            to_guard_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitGuard.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn load_onto(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = LoadOntoUnitCommandData {
            unitId: -1,
            groupId: self.group_id,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            transporter_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitLoadOnto.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn load_units(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        targets: &[Unit],
    ) -> Result<(), &'static str> {
        let mut command_data = LoadUnitsUnitCommandData {
            unitId: -1,
            groupId: self.group_id,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            to_load_unit_ids: targets.iter().map(|t| t.unit_id).collect(),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitLoadUnits.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn load_units_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = LoadUnitsAreaCommandData {
            unitId: -1,
            groupId: self.group_id,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitLoadUnitsArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn move_unit(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        to_pos: [f32; 3],
    ) -> Result<(), &'static str> {
        let mut command_data = MoveUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_pos,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitMove.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn patrol(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        to_pos: [f32; 3],
    ) -> Result<(), &'static str> {
        let mut command_data = PatrolUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_pos,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitPatrol.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn reclaim_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = ReclaimAreaUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitReclaimArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn reclaim_feature(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Feature,
    ) -> Result<(), &'static str> {
        let mut command_data = ReclaimFeatureUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_reclaim_feature_id: target.feature_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitReclaimFeature.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn reclaim_unit(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = ReclaimUnitUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_reclaim_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitReclaimFeature.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn repair_unit(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = RepairUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_repair_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitRepair.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn restore_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = RestoreAreaUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitRestoreArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn resurrect_unit(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = ResurrectUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_resurrect_feature_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitResurrect.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn resurrect_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = ResurrectAreaUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitResurrectArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn self_destruct(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SelfDestroyUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSelfDestroy.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_auto_repair_level(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        auto_repair_level: i32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetAutoRepairLevelUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            auto_repair_level,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetAutoRepairLevel.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_base_command(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        base_position: [f32; 3],
    ) -> Result<(), &'static str> {
        let mut command_data = SetBaseUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            base_position,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetBase.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_fire_state(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        fire_state: FireState,
    ) -> Result<(), &'static str> {
        let mut command_data = SetFireUnitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            fire_state,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetFireState.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_idle_mode(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        idle_mode: IdleMode,
    ) -> Result<(), &'static str> {
        let mut command_data = SetIdleModeCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            idle_mode,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetIdleMode.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_move_state(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        move_state: MoveState,
    ) -> Result<(), &'static str> {
        let mut command_data = SetMoveStateCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            move_state,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetMoveState.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_on_off(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        on: bool,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitOnOffCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            on,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetOnOff.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_repeat(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        repeat: bool,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitRepeatCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            repeat,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetRepeat.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_trajectory(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        trajectory: Trajectory,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitTrajectoryCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            trajectory,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitSetTrajectory.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn stockpile(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitStockpileCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitStockpile.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn stop(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitStopCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitStop.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn unload(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        to_unload_unit: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitUnloadCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            to_unload_unit_id: to_unload_unit.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitUnloadUnit.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn unload_area(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitUnloadAreaCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitUnloadUnitsArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitWait.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_death(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitWaitDeath.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_gather(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitGatherCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitWaitGather.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_squad(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        num_units: i32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitSquadCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            num_units,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitWaitSquad.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_time(
        &self,
        options: &[UnitCommandOptions],
        timeout: Option<i32>,
        time: i32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitTimeCommandData {
            unit_id: -1,
            group_id: self.group_id,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            time,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::UnitWaitTime.into(),
            &mut command_data.c_data(),
        )
    }
}
