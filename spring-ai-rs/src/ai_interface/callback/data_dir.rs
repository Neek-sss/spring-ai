use std::{error::Error, ffi::CStr};

use crate::{ai_interface::AIInterface, get_callback};

impl AIInterface {
    pub fn config_dir(&self) -> Result<String, Box<dyn Error>> {
        let get_config_dir = get_callback!(self.ai_id, DataDirs_getConfigDir)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_config_dir(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn writeable_dir(&self) -> Result<String, Box<dyn Error>> {
        let get_writeable_dir = get_callback!(self.ai_id, DataDirs_getWriteableDir)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_writeable_dir(self.ai_id)) }.to_str()?,
        ))
    }
}
