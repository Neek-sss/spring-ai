use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum MoveState {
    HoldPosition,
    Maneuvre,
    Roam,
}

impl Into<i32> for MoveState {
    fn into(self) -> i32 {
        match self {
            MoveState::HoldPosition => 0,
            MoveState::Maneuvre => 1,
            MoveState::Roam => 2,
        }
    }
}

impl Into<MoveState> for i32 {
    fn into(self) -> MoveState {
        match self {
            0 => MoveState::HoldPosition,
            1 => MoveState::Maneuvre,
            2 => MoveState::Roam,
            _ => unimplemented!(),
        }
    }
}
