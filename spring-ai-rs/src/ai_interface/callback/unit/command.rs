use std::error::Error;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            unit::{
                AiSelectUnitCommandData, AttackAreaUnitCommandData, AttackUnitCommandData,
                BuildUnitCommandData, CaptureAreaUnitCommanddData, CaptureUnitCommanddData,
                CloakUnitCommandData, CustomUnitCommanddData, DGunUnitCommandData,
                FightUnitCommandData, GuardUnitCommandData, LoadOntoUnitCommandData,
                LoadUnitsAreaCommandData, LoadUnitsUnitCommandData, MoveUnitCommandData,
                PatrolUnitCommandData, ReclaimAreaUnitCommandData, ReclaimFeatureUnitCommandData,
                ReclaimUnitUnitCommandData, RepairUnitCommandData, RestoreAreaUnitCommandData,
                ResurrectAreaUnitCommandData, ResurrectUnitCommandData, SelfDestroyUnitCommandData,
                SetAutoRepairLevelUnitCommandData, SetBaseUnitCommandData, SetFireUnitCommandData,
                SetIdleModeCommandData, SetMoveStateCommandData, SetUnitOnOffCommandData,
                SetUnitRepeatCommandData, SetUnitStockpileCommandData, SetUnitStopCommandData,
                SetUnitTrajectoryCommandData, SetUnitUnloadAreaCommandData,
                SetUnitUnloadCommandData, SetUnitWaitCommandData, SetUnitWaitGatherCommandData,
                SetUnitWaitSquadCommandData, SetUnitWaitTimeCommandData,
            },
            CommandData,
        },
        command_topic::CommandTopic,
        options::UnitCommandOptions,
    },
    engine::handle_command,
    facing::Facing,
    feature::Feature,
    fire_state::FireState,
    idle_mode::IdleMode,
    move_state::MoveState,
    position::Position,
    trajectory::Trajectory,
    unit::Unit,
    unit_def::UnitDef,
};

impl Unit {
    pub fn select(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = AiSelectUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitAISelect.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn attack(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = AttackUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            target_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitAttack.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn attack_area(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = AttackAreaUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            attack_position: position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitAttackArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn build<P: Position, D: PartialEq<UnitDef>>(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        to_build_unit_def: D,
        position: P,
        facing: Facing,
    ) -> Result<(), Box<dyn Error>> {
        let mut building_def_opt = None;
        for unit_def in self.unit_def()?.building().build_options()? {
            if to_build_unit_def == unit_def {
                building_def_opt = Some(unit_def);
            }
        }

        if let Some(building_def) = building_def_opt {
            let building_x = building_def.miscellaneous().x_size()?;
            let building_z = building_def.miscellaneous().z_size()?;

            let mut build_position = position.to_f32_array(self.ai_id)?;
            build_position[0] =
                ((build_position[0] + (building_x as f32 / 2.0)) / 16.0).trunc() * 16.0;
            build_position[2] =
                ((build_position[2] + (building_z as f32 / 2.0)) / 16.0).trunc() * 16.0;

            let mut command_data = BuildUnitCommandData {
                unit_id: self.unit_id,
                group_id: -1,
                options: options.to_vec(),
                timeout: timeout.unwrap_or(i32::MAX),
                to_build_unit_def_id: building_def.def_id,
                build_position,
                facing,
            };

            handle_command(
                self.ai_id,
                COMMAND_TO_ID_ENGINE,
                command_id.unwrap_or(-1),
                CommandTopic::UnitBuild.into(),
                &mut command_data.c_data(),
            )?;
        }

        Ok(())
    }

    pub fn capture(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = CaptureUnitCommanddData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_capture_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitCapture.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn capture_area(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = CaptureAreaUnitCommanddData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitCaptureArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn cloak(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        cloak: bool,
    ) -> Result<(), &'static str> {
        let mut command_data = CloakUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            cloak,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitCloak.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn custom_command(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        parameters: Vec<f32>,
    ) -> Result<(), &'static str> {
        let mut command_data = CustomUnitCommanddData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            command_id: command_id.unwrap_or(-1),
            parameters,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitCustom.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn dgun_unit(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = DGunUnitCommandData {
            unitId: self.unit_id,
            groupId: -1,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            toAttackUnitId: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitDGun.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn fight<P: Position>(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: P,
    ) -> Result<(), &'static str> {
        let mut command_data = FightUnitCommandData {
            unitId: self.unit_id,
            groupId: -1,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            position: position.to_f32_array(self.ai_id).unwrap(),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitFight.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn guard(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = GuardUnitCommandData {
            unitId: self.unit_id,
            groupId: -1,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            to_guard_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitGuard.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn load_onto(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = LoadOntoUnitCommandData {
            unitId: self.unit_id,
            groupId: -1,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            transporter_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitLoadOnto.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn load_units(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        targets: &[Unit],
    ) -> Result<(), &'static str> {
        let mut command_data = LoadUnitsUnitCommandData {
            unitId: self.unit_id,
            groupId: -1,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            to_load_unit_ids: targets.iter().map(|t| t.unit_id).collect(),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitLoadUnits.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn load_units_area(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = LoadUnitsAreaCommandData {
            unitId: self.unit_id,
            groupId: -1,
            options: options.to_vec(),
            timeOut: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitLoadUnitsArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn move_unit<P: Position>(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        to_pos: P,
    ) -> Result<(), Box<dyn Error>> {
        let mut command_data = MoveUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_pos: to_pos.to_f32_array(self.ai_id)?,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitMove.into(),
            &mut command_data.c_data(),
        )?;

        Ok(())
    }

    pub fn patrol<P: Position>(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        to_pos: P,
    ) -> Result<(), Box<dyn Error>> {
        let mut command_data = PatrolUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_pos: to_pos.to_f32_array(self.ai_id)?,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitPatrol.into(),
            &mut command_data.c_data(),
        )?;

        Ok(())
    }

    pub fn reclaim_area<P: Position>(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: P,
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = ReclaimAreaUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position: position.to_f32_array(self.ai_id).unwrap(),
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitReclaimArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn reclaim_feature(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Feature,
    ) -> Result<(), &'static str> {
        let mut command_data = ReclaimFeatureUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_reclaim_feature_id: target.feature_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitReclaimFeature.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn reclaim_unit(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = ReclaimUnitUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_reclaim_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitReclaimFeature.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn repair_unit(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = RepairUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_repair_unit_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitRepair.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn restore_area(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = RestoreAreaUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitRestoreArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn resurrect_unit(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        target: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = ResurrectUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            to_resurrect_feature_id: target.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitResurrect.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn resurrect_area(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = ResurrectAreaUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitResurrectArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn self_destruct(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SelfDestroyUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSelfDestroy.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_auto_repair_level(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        auto_repair_level: i32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetAutoRepairLevelUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            auto_repair_level,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetAutoRepairLevel.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_base_command(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        base_position: [f32; 3],
    ) -> Result<(), &'static str> {
        let mut command_data = SetBaseUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            base_position,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetBase.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_fire_state(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        fire_state: FireState,
    ) -> Result<(), &'static str> {
        let mut command_data = SetFireUnitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            fire_state,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetFireState.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_idle_mode(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        idle_mode: IdleMode,
    ) -> Result<(), &'static str> {
        let mut command_data = SetIdleModeCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            idle_mode,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetIdleMode.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_move_state(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        move_state: MoveState,
    ) -> Result<(), &'static str> {
        let mut command_data = SetMoveStateCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            move_state,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetMoveState.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_on_off(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        on: bool,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitOnOffCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            on,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetOnOff.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_repeat(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        repeat: bool,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitRepeatCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            repeat,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetRepeat.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn set_trajectory(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        trajectory: Trajectory,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitTrajectoryCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            trajectory,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitSetTrajectory.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn stockpile(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitStockpileCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitStockpile.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn stop(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitStopCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitStop.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn unload(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        to_unload_unit: Unit,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitUnloadCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            to_unload_unit_id: to_unload_unit.unit_id,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitUnloadUnit.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn unload_area(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        position: [f32; 3],
        radius: f32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitUnloadAreaCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            position,
            radius,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitUnloadUnitsArea.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitWait.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_death(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitWaitDeath.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_gather(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitGatherCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitWaitGather.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_squad(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        num_units: i32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitSquadCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            num_units,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitWaitSquad.into(),
            &mut command_data.c_data(),
        )
    }

    pub fn wait_time(
        &self,
        options: &[UnitCommandOptions],
        command_id: Option<i32>,
        timeout: Option<i32>,
        time: i32,
    ) -> Result<(), &'static str> {
        let mut command_data = SetUnitWaitTimeCommandData {
            unit_id: self.unit_id,
            group_id: -1,
            options: options.to_vec(),
            timeout: timeout.unwrap_or(i32::MAX),
            time,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            command_id.unwrap_or(-1),
            CommandTopic::UnitWaitTime.into(),
            &mut command_data.c_data(),
        )
    }
}
