use std::{error::Error, f32::NAN, ffi::CStr};

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct UnitCurrentCommand {
    pub ai_id: i32,
    pub unit_id: i32,
    pub current_command_index: i32,
}

#[derive(Debug, Clone)]
pub struct UnitCurrentCommandAll {
    id: i32,
    options: i16,
    tag: i32,
    timeout: i32,
    parameters: Vec<f32>,
}

const MAX_PARAMETERS: usize = 32;

impl UnitCurrentCommand {
    pub fn id(&self) -> Result<i32, Box<dyn Error>> {
        let get_current_command_id_func = get_callback!(self.ai_id, Unit_CurrentCommand_getId)?;

        Ok(unsafe {
            get_current_command_id_func(self.ai_id, self.unit_id, self.current_command_index)
        })
    }

    pub fn options(&self) -> Result<i16, Box<dyn Error>> {
        let get_current_command_options_func =
            get_callback!(self.ai_id, Unit_CurrentCommand_getOptions)?;

        Ok(unsafe {
            get_current_command_options_func(self.ai_id, self.unit_id, self.current_command_index)
        })
    }

    pub fn tag(&self) -> Result<i32, Box<dyn Error>> {
        let get_current_command_tag_func = get_callback!(self.ai_id, Unit_CurrentCommand_getTag)?;

        Ok(unsafe {
            get_current_command_tag_func(self.ai_id, self.unit_id, self.current_command_index)
        })
    }

    pub fn timeout(&self) -> Result<i32, Box<dyn Error>> {
        let get_current_command_timeout_func =
            get_callback!(self.ai_id, Unit_CurrentCommand_getTimeOut)?;

        Ok(unsafe {
            get_current_command_timeout_func(self.ai_id, self.unit_id, self.current_command_index)
        })
    }

    pub fn parameters(&self) -> Result<Vec<f32>, Box<dyn Error>> {
        let get_current_command_parameters_func =
            get_callback!(self.ai_id, Unit_CurrentCommand_getParams)?;

        let mut ret = [NAN; MAX_PARAMETERS];
        unsafe {
            get_current_command_parameters_func(
                self.ai_id,
                self.unit_id,
                self.current_command_index,
                ret.as_mut_ptr(),
                MAX_PARAMETERS as i32,
            )
        };

        Ok(ret.iter().cloned().filter(|&f| f.is_finite()).collect())
    }

    pub fn all(&self) -> Result<UnitCurrentCommandAll, Box<dyn Error>> {
        Ok(UnitCurrentCommandAll {
            id: self.id()?,
            options: self.options()?,
            tag: self.tag()?,
            timeout: self.timeout()?,
            parameters: self.parameters()?,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct UnitSupportedCommand {
    pub ai_id: i32,
    pub unit_id: i32,
    pub supported_command_index: i32,
}

#[derive(Debug, Clone)]
pub struct UnitSupportedCommandAll {
    id: i32,
    name: String,
    tooltip: String,
    is_show_unique: bool,
    is_disabled: bool,
    parameters: String,
}

impl UnitSupportedCommand {
    pub fn id(&self) -> Result<i32, Box<dyn Error>> {
        let get_supported_command_id_func = get_callback!(self.ai_id, Unit_SupportedCommand_getId)?;

        Ok(unsafe {
            get_supported_command_id_func(self.ai_id, self.unit_id, self.supported_command_index)
        })
    }

    pub fn name(&self) -> Result<String, Box<dyn Error>> {
        let get_supported_command_name_func =
            get_callback!(self.ai_id, Unit_SupportedCommand_getName)?;

        Ok(String::from(
            unsafe {
                CStr::from_ptr(get_supported_command_name_func(
                    self.ai_id,
                    self.unit_id,
                    self.supported_command_index,
                ))
            }
            .to_str()?,
        ))
    }

    pub fn tooltip(&self) -> Result<String, Box<dyn Error>> {
        let get_supported_command_tooltip_func =
            get_callback!(self.ai_id, Unit_SupportedCommand_getToolTip)?;

        Ok(String::from(
            unsafe {
                CStr::from_ptr(get_supported_command_tooltip_func(
                    self.ai_id,
                    self.unit_id,
                    self.supported_command_index,
                ))
            }
            .to_str()?,
        ))
    }

    pub fn is_show_unique(&self) -> Result<bool, Box<dyn Error>> {
        let get_suppored_command_is_show_unique_func =
            get_callback!(self.ai_id, Unit_SupportedCommand_isShowUnique)?;

        Ok(unsafe {
            get_suppored_command_is_show_unique_func(
                self.ai_id,
                self.unit_id,
                self.supported_command_index,
            )
        })
    }

    pub fn is_disabled(&self) -> Result<bool, Box<dyn Error>> {
        let get_supported_command_is_disabled_func =
            get_callback!(self.ai_id, Unit_SupportedCommand_isDisabled)?;

        Ok(unsafe {
            get_supported_command_is_disabled_func(
                self.ai_id,
                self.unit_id,
                self.supported_command_index,
            )
        })
    }

    pub fn parameters(&self) -> Result<String, Box<dyn Error>> {
        let get_supported_command_parameters_func =
            get_callback!(self.ai_id, Unit_SupportedCommand_getParams)?;

        let s = String::with_capacity(MAX_PARAMETERS);
        let ptr = &mut (s.as_ptr() as *const i8);
        unsafe {
            get_supported_command_parameters_func(
                self.ai_id,
                self.unit_id,
                self.supported_command_index,
                ptr as *mut *const i8,
                MAX_PARAMETERS as i32,
            )
        };

        Ok(String::from(
            unsafe { CStr::from_ptr(*ptr) }.to_str().unwrap_or(""),
        ))
    }

    pub fn all(&self) -> Result<UnitSupportedCommandAll, Box<dyn Error>> {
        Ok(UnitSupportedCommandAll {
            id: self.id()?,
            name: self.name()?,
            tooltip: self.tooltip()?,
            is_show_unique: self.is_show_unique()?,
            is_disabled: self.is_disabled()?,
            parameters: self.parameters()?,
        })
    }
}
