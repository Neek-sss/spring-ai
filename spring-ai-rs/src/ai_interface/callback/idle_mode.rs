use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum IdleMode {
    Fly,
    Land,
}

impl Into<i32> for IdleMode {
    fn into(self) -> i32 {
        match self {
            IdleMode::Fly => 0,
            IdleMode::Land => 1,
        }
    }
}

impl Into<IdleMode> for i32 {
    fn into(self) -> IdleMode {
        match self {
            0 => IdleMode::Fly,
            1 => IdleMode::Land,
            _ => unimplemented!(),
        }
    }
}
