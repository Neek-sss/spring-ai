pub mod cheat;
pub mod command;
pub mod data_dir;
pub mod engine;
pub mod feature;
pub mod feature_def;
pub mod game;
pub mod game_mod;
pub mod group;
// TODO: Was removed by BAR
// pub mod gui;
pub mod drawer;
pub mod facing;
pub mod fire_state;
pub mod idle_mode;
pub mod lua;
pub mod map;
pub mod move_state;
pub mod other;
pub mod path;
pub mod position;
pub mod resource;
pub mod send;
pub mod skirmish_ai;
pub mod teams;
pub mod trace;
pub mod trajectory;
pub mod unit;
pub mod unit_def;
pub mod weapon_def;

#[macro_export]
macro_rules! get_callback {
    ($ai_id:expr, $command_name:ident) => {
        unsafe {
            $crate::skirmish_ai::get_skirmish_ai($ai_id)
                .ok_or("Cannot get skirmish ai")
                .and_then(|skirmish_ai| {
                    skirmish_ai
                        .get_callback()
                        .ok_or("Cannot get callback")
                        .and_then(|callback| {
                            callback
                                .as_ref()
                                .$command_name
                                .ok_or(stringify!("No callback function exists:" $command_name))
                        })
                })
        }
    };
}
