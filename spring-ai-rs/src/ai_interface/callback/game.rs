use std::{error::Error, ffi::CStr};

use crate::{ai_interface::AIInterface, get_callback};

#[derive(Debug, Copy, Clone)]
pub struct Game {
    ai_id: i32,
}

#[derive(Debug, Clone)]
pub struct GameAll {
    ai_interface_version: i32,
    debug_mode_enabled: bool,
    //    mode: i32,
    paused: bool,
    speed_factor: f32,
    setup_script: String,
}

impl AIInterface {
    pub fn game(&self) -> Game {
        Game { ai_id: self.ai_id }
    }
}

impl Game {
    pub fn ai_interface_version(&self) -> Result<i32, Box<dyn Error>> {
        let interface_version = get_callback!(self.ai_id, Game_getAiInterfaceVersion)?;
        Ok(unsafe { interface_version(self.ai_id) })
    }

    pub fn debug_mode_enabled(&self) -> Result<bool, Box<dyn Error>> {
        let is_debug_enabled_func = get_callback!(self.ai_id, Game_isDebugModeEnabled)?;
        Ok(unsafe { is_debug_enabled_func(self.ai_id) })
    }

    pub fn mode(&self) -> Result<i32, Box<dyn Error>> {
        let get_mode_func = get_callback!(self.ai_id, Game_getMode)?;
        Ok(unsafe { get_mode_func(self.ai_id) })
    }

    pub fn paused(&self) -> Result<bool, Box<dyn Error>> {
        let is_paused_func = get_callback!(self.ai_id, Game_isPaused)?;
        Ok(unsafe { is_paused_func(self.ai_id) })
    }

    pub fn speed_factor(&self) -> Result<f32, Box<dyn Error>> {
        let get_speed_factor_func = get_callback!(self.ai_id, Game_getSpeedFactor)?;
        Ok(unsafe { get_speed_factor_func(self.ai_id) })
    }

    pub fn setup_script(&self) -> Result<String, Box<dyn Error>> {
        let get_setup_script_func = get_callback!(self.ai_id, Game_getSetupScript)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_setup_script_func(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn all(&self) -> Result<GameAll, Box<dyn Error>> {
        Ok(GameAll {
            ai_interface_version: self.ai_interface_version()?,
            debug_mode_enabled: self.debug_mode_enabled()?,
            // TODO:           mode: self.mode()?,
            paused: self.paused()?,
            speed_factor: self.speed_factor()?,
            setup_script: self.setup_script()?,
        })
    }

    // TODO: Game_getCategoryFlag
    // TODO: Game_getCategoriesFlag
    // TODO: Game_getCategoryName
}
