use std::{
    error::Error,
    ops::{Index, IndexMut},
};

use crate::ai_interface::callback::map::Map;

pub trait Position {
    fn to_f32_array(&self, ai_id: i32) -> Result<[f32; 3], Box<dyn Error>>;
}

#[derive(Copy, Clone, Debug)]
pub struct AirPosition(pub f32, pub f32);

impl Index<usize> for AirPosition {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            _ => unimplemented!(),
        }
    }
}

impl IndexMut<usize> for AirPosition {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            _ => unimplemented!(),
        }
    }
}

impl Position for AirPosition {
    fn to_f32_array(&self, ai_id: i32) -> Result<[f32; 3], Box<dyn Error>> {
        Ok([self[0], Map { ai_id }.max_height()?, self[1]])
    }
}

#[derive(Copy, Clone, Debug)]
pub struct GroundPosition(pub f32, pub f32);

impl Index<usize> for GroundPosition {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            _ => unimplemented!(),
        }
    }
}

impl IndexMut<usize> for GroundPosition {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            _ => unimplemented!(),
        }
    }
}

impl Position for GroundPosition {
    fn to_f32_array(&self, ai_id: i32) -> Result<[f32; 3], Box<dyn Error>> {
        Ok([
            self[0],
            // TODO: (Takes too long; I think it's the copy of the whole map) Map { ai_id }.center_height_map()?[(self[0] as usize / 8, self[1] as usize / 8)],
            0.0, self[1],
        ])
    }
}

#[derive(Copy, Clone, Debug)]
pub struct WaterPosition(pub f32, pub f32);

impl Index<usize> for WaterPosition {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            _ => unimplemented!(),
        }
    }
}

impl IndexMut<usize> for WaterPosition {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            _ => unimplemented!(),
        }
    }
}

impl Position for WaterPosition {
    fn to_f32_array(&self, _: i32) -> Result<[f32; 3], Box<dyn Error>> {
        Ok([self[0], 0.0, self[1]])
    }
}

impl Position for [f32; 3] {
    fn to_f32_array(&self, _: i32) -> Result<[f32; 3], Box<dyn Error>> {
        Ok([self[0], 0.0, self[1]])
    }
}
