use std::error::Error;

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct WeaponBounce {
    pub ai_id: i32,
    pub weapon_def_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct WeaponBounceAll {
    water: bool,
    ground: bool,
    rebound: f32,
    slip: f32,
    number_of_bounces: i32,
}

impl WeaponBounce {
    pub fn water(&self) -> Result<bool, Box<dyn Error>> {
        let get_water_bounce = get_callback!(self.ai_id, WeaponDef_isWaterBounce)?;
        Ok(unsafe { get_water_bounce(self.ai_id, self.weapon_def_id) })
    }

    pub fn ground(&self) -> Result<bool, Box<dyn Error>> {
        let get_ground_bounce = get_callback!(self.ai_id, WeaponDef_isGroundBounce)?;
        Ok(unsafe { get_ground_bounce(self.ai_id, self.weapon_def_id) })
    }

    pub fn rebound(&self) -> Result<f32, Box<dyn Error>> {
        let get_bounce_rebound = get_callback!(self.ai_id, WeaponDef_getBounceRebound)?;
        Ok(unsafe { get_bounce_rebound(self.ai_id, self.weapon_def_id) })
    }

    pub fn slip(&self) -> Result<f32, Box<dyn Error>> {
        let get_bounce_slip = get_callback!(self.ai_id, WeaponDef_getBounceSlip)?;
        Ok(unsafe { get_bounce_slip(self.ai_id, self.weapon_def_id) })
    }

    pub fn number_of_bounces(&self) -> Result<i32, Box<dyn Error>> {
        let get_number_of_bounces = get_callback!(self.ai_id, WeaponDef_getNumBounce)?;
        Ok(unsafe { get_number_of_bounces(self.ai_id, self.weapon_def_id) })
    }

    pub fn all(&self) -> Result<WeaponBounceAll, Box<dyn Error>> {
        Ok(WeaponBounceAll {
            water: self.water()?,
            ground: self.ground()?,
            rebound: self.rebound()?,
            slip: self.slip()?,
            number_of_bounces: self.number_of_bounces()?,
        })
    }
}
