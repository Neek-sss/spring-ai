use std::{collections::HashMap, error::Error};

use crate::{
    ai_interface::{callback::resource::Resource, AIInterface},
    get_callback,
};

#[derive(Debug, Copy, Clone)]
pub struct WeaponShield {
    pub ai_id: i32,
    pub weapon_def_id: i32,
}

#[derive(Debug, Clone)]
pub struct WeaponShieldAll {
    repulser: bool,
    smart: bool,
    exterior: bool,
    resource_use: HashMap<Resource, f32>,
    radius: f32,
    force: f32,
    max_speed: f32,
    power: f32,
    power_regen: f32,
    power_regen_resource: HashMap<Resource, f32>,
    starting_power: f32,
    recharge_delay: i32,
}

impl WeaponShield {
    pub fn repulser(&self) -> Result<bool, Box<dyn Error>> {
        let get_shield_repulser = get_callback!(self.ai_id, WeaponDef_isShieldRepulser)?;
        Ok(unsafe { get_shield_repulser(self.ai_id, self.weapon_def_id) })
    }

    pub fn smart(&self) -> Result<bool, Box<dyn Error>> {
        let get_shield_smart = get_callback!(self.ai_id, WeaponDef_isSmartShield)?;
        Ok(unsafe { get_shield_smart(self.ai_id, self.weapon_def_id) })
    }

    pub fn exterior(&self) -> Result<bool, Box<dyn Error>> {
        let get_shield_exterior = get_callback!(self.ai_id, WeaponDef_isExteriorShield)?;
        Ok(unsafe { get_shield_exterior(self.ai_id, self.weapon_def_id) })
    }

    pub fn resource_use(&self, resource: Resource) -> Result<f32, Box<dyn Error>> {
        let get_shield_resource_use = get_callback!(self.ai_id, WeaponDef_Shield_getResourceUse)?;
        Ok(
            unsafe {
                get_shield_resource_use(self.ai_id, self.weapon_def_id, resource.resource_id)
            },
        )
    }

    pub fn radius(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_radius = get_callback!(self.ai_id, WeaponDef_Shield_getRadius)?;
        Ok(unsafe { get_shield_radius(self.ai_id, self.weapon_def_id) })
    }

    pub fn force(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_force = get_callback!(self.ai_id, WeaponDef_Shield_getForce)?;
        Ok(unsafe { get_shield_force(self.ai_id, self.weapon_def_id) })
    }

    pub fn max_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_max_speed = get_callback!(self.ai_id, WeaponDef_Shield_getMaxSpeed)?;
        Ok(unsafe { get_shield_max_speed(self.ai_id, self.weapon_def_id) })
    }

    pub fn power(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_power = get_callback!(self.ai_id, WeaponDef_Shield_getPower)?;
        Ok(unsafe { get_shield_power(self.ai_id, self.weapon_def_id) })
    }

    pub fn power_regen(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_power_regen = get_callback!(self.ai_id, WeaponDef_Shield_getPowerRegen)?;
        Ok(unsafe { get_shield_power_regen(self.ai_id, self.weapon_def_id) })
    }

    pub fn power_regen_resource(&self, resource: Resource) -> Result<f32, Box<dyn Error>> {
        let get_shield_power_regen_resource =
            get_callback!(self.ai_id, WeaponDef_Shield_getPowerRegenResource)?;
        Ok(unsafe {
            get_shield_power_regen_resource(self.ai_id, self.weapon_def_id, resource.resource_id)
        })
    }

    pub fn starting_power(&self) -> Result<f32, Box<dyn Error>> {
        let get_shield_starting_power =
            get_callback!(self.ai_id, WeaponDef_Shield_getStartingPower)?;
        Ok(unsafe { get_shield_starting_power(self.ai_id, self.weapon_def_id) })
    }

    pub fn recharge_delay(&self) -> Result<i32, Box<dyn Error>> {
        let get_shield_recharge_delay =
            get_callback!(self.ai_id, WeaponDef_Shield_getRechargeDelay)?;
        Ok(unsafe { get_shield_recharge_delay(self.ai_id, self.weapon_def_id) })
    }

    // TODO: InterceptType

    pub fn all(&self) -> Result<WeaponShieldAll, Box<dyn Error>> {
        Ok(WeaponShieldAll {
            repulser: self.repulser()?,
            smart: self.smart()?,
            exterior: self.exterior()?,
            resource_use: AIInterface::new(self.ai_id)
                .resource_interface()
                .get_resources()?
                .into_iter()
                .filter_map(|resource| {
                    if let Ok(res) = self.resource_use(resource) {
                        Some((resource, res))
                    } else {
                        None
                    }
                })
                .collect(),
            radius: self.radius()?,
            force: self.force()?,
            max_speed: self.max_speed()?,
            power: self.power()?,
            power_regen: self.power_regen()?,
            power_regen_resource: AIInterface::new(self.ai_id)
                .resource_interface()
                .get_resources()?
                .into_iter()
                .filter_map(|resource| {
                    if let Ok(res) = self.power_regen_resource(resource) {
                        Some((resource, res))
                    } else {
                        None
                    }
                })
                .collect(),
            starting_power: self.starting_power()?,
            recharge_delay: self.recharge_delay()?,
        })
    }
}
