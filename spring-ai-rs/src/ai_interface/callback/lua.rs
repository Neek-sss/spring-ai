use std::ffi::CString;

use spring_ai_sys::{COMMAND_TO_ID_ENGINE, MAX_RESPONSE_SIZE};

use crate::ai_interface::{
    callback::{
        command::{
            command_data::{
                lua::{CallLuaRulesCommandData, CallLuaUICommandData},
                CommandData,
            },
            command_topic::CommandTopic,
        },
        engine::handle_command,
    },
    AIInterface,
};

pub struct Lua {
    pub ai_id: i32,
}

impl AIInterface {
    pub fn lua(&self) -> Lua {
        Lua { ai_id: self.ai_id }
    }
}

impl Lua {
    pub fn call_rules(&self, in_data: &str) -> Result<String, String> {
        let in_data_cstring = CString::new(in_data).map_err(|e| format!("{e}"))?;

        let mut command_data = CallLuaRulesCommandData {
            in_data: in_data_cstring,
            in_size: in_data.len() as i32,
            out_data: CString::new(String::with_capacity(MAX_RESPONSE_SIZE as usize))
                .map_err(|e| format!("{e}"))?,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::CallLuaRules.into(),
            &mut command_data.c_data(),
        )?;

        Ok(command_data.out_data.to_string_lossy().to_string())
    }
    pub fn call_ui(&self, in_data: &str) -> Result<String, String> {
        let in_data_cstring = CString::new(in_data).map_err(|e| format!("{e}"))?;

        let mut command_data = CallLuaUICommandData {
            in_data: in_data_cstring,
            in_size: in_data.len() as i32,
            out_data: CString::new(String::with_capacity(MAX_RESPONSE_SIZE as usize))
                .map_err(|e| format!("{e}"))?,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::CallLuaUI.into(),
            &mut command_data.c_data(),
        )?;

        Ok(command_data.out_data.to_string_lossy().to_string())
    }
}
