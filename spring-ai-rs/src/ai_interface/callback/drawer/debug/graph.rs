use std::ffi::CString;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            debug_drawer::{
                AddPointLineGraphDrawerDebugCommandData,
                DeletePointsLineGraphDrawerDebugCommandData,
                SetColorLineGraphDrawerDebugCommandData, SetLabelLineGraphDrawerDebugCommandData,
                SetPositionGraphDrawerDebugCommandData, SetSizeGraphDrawerDebugCommandData,
            },
            CommandData,
        },
        command_topic::CommandTopic,
    },
    engine::handle_command,
};

pub struct GraphLine {
    pub ai_id: i32,
    pub line_id: i32,
}

impl GraphLine {
    pub fn add_point(&self, x: f32, y: f32) -> Result<(), String> {
        let mut command_c_data = AddPointLineGraphDrawerDebugCommandData {
            line_id: self.line_id,
            x,
            y,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerGraphLineAddPoint.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn delete_points(&self, num_points: i32) -> Result<(), String> {
        let mut command_c_data = DeletePointsLineGraphDrawerDebugCommandData {
            line_id: self.line_id,
            num_points,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerGraphLineDeletePoints.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_color(&self, color: [i16; 3]) -> Result<(), String> {
        let mut command_c_data = SetColorLineGraphDrawerDebugCommandData {
            line_id: self.line_id,
            color,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerGraphLineSetColor.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_label(&self, label: &str) -> Result<(), String> {
        let mut command_c_data = SetLabelLineGraphDrawerDebugCommandData {
            line_id: self.line_id,
            label: CString::new(label).map_err(|e| e.to_string())?,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerGraphLineSetLabel.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}

pub struct Graph {
    pub ai_id: i32,
}

impl Graph {
    pub fn set_position(&self, x: f32, y: f32) -> Result<(), String> {
        let mut command_c_data = SetPositionGraphDrawerDebugCommandData { x, y }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerGraphSetPOS.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_size(&self, w: f32, h: f32) -> Result<(), String> {
        let mut command_c_data = SetSizeGraphDrawerDebugCommandData { w, h }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DebugDrawerGraphSetSize.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
