use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            drawer::{
                CreateLineFigureDrawerCommandData, CreateSplineFigureDrawerCommandData,
                DeleteFigureDrawerCommandData, SetColorFigureDrawerCommandData,
            },
            CommandData,
        },
        command_topic::CommandTopic,
    },
    engine::handle_command,
};

pub struct Figure {
    pub ai_id: i32,
}

impl Figure {
    pub fn create_line(
        &self,
        position_1: [f32; 3],
        position_2: [f32; 3],
        width: f32,
        arrow: bool,
        lifetime: i32,
        figure_group_id: i32,
    ) -> Result<(), String> {
        let mut command_c_data = CreateLineFigureDrawerCommandData {
            position_1,
            position_2,
            width,
            arrow,
            lifetime,
            figure_group_id,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerFigureCreateLine.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn create_spline(
        &self,
        position_1: [f32; 3],
        position_2: [f32; 3],
        position_3: [f32; 3],
        position_4: [f32; 3],
        width: f32,
        arrow: bool,
        lifetime: i32,
        figure_group_id: i32,
    ) -> Result<(), String> {
        let mut command_c_data = CreateSplineFigureDrawerCommandData {
            position_1,
            position_2,
            position_3,
            position_4,
            width,
            arrow,
            lifetime,
            figure_group_id,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerFigureCreateSpline.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn delete(&self, figure_group_id: i32) -> Result<(), String> {
        let mut command_c_data = DeleteFigureDrawerCommandData { figure_group_id }.c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerFigureDelete.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
    pub fn set_color(
        &self,
        figure_group_id: i32,
        color: [i16; 3],
        alpha: i16,
    ) -> Result<(), String> {
        let mut command_c_data = SetColorFigureDrawerCommandData {
            figure_group_id,
            color,
            alpha,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerFigureSetColor.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
