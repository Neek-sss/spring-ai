use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::callback::{
    command::{
        command_data::{
            drawer::{AddNotificationDrawerCommandData, DrawUnitDrawerCommandData},
            CommandData,
        },
        command_topic::CommandTopic,
    },
    engine::handle_command,
    facing::Facing,
};

pub mod debug;
pub mod figure;
pub mod line;
pub mod path;
pub mod point;

pub struct Drawer {
    pub ai_id: i32,
}

impl Drawer {
    pub fn add_notification(
        &self,
        position: [f32; 3],
        color: [i16; 3],
        alpha: i16,
    ) -> Result<(), String> {
        let mut command_c_data = AddNotificationDrawerCommandData {
            position,
            color,
            alpha,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerAddNotification.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }

    pub fn draw_unit(
        &self,
        draw_unit_def_id: i32,
        position: [f32; 3],
        rotation: f32,
        lifetime: i32,
        team_id: i32,
        transparent: bool,
        draw_border: bool,
        facing: Facing,
    ) -> Result<(), String> {
        let mut command_c_data = DrawUnitDrawerCommandData {
            draw_unit_def_id,
            position,
            rotation,
            lifetime,
            team_id,
            transparent,
            draw_border,
            facing,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::DrawerDrawUnit.into(),
            &mut command_c_data,
        )?;

        Ok(())
    }
}
