use std::error::Error;

use serde::{Deserialize, Serialize};
use slog::Logger;

use crate::{
    ai_interface::{callback::resource::Resource, AIInterface},
    get_callback,
};

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct Team {
    pub ai_id: i32,
    pub team_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct TeamAll {
    color: [i16; 3],
    allied: bool,
}

#[derive(Debug, Copy, Clone)]
pub struct TeamInterface {
    pub ai_id: i32,
}

const MAX_TEAM: usize = 32;

impl AIInterface {
    pub fn team_interface(&self) -> TeamInterface {
        TeamInterface { ai_id: self.ai_id }
    }
}

impl TeamInterface {
    pub fn get_teams(&self) -> Result<Vec<Team>, Box<dyn Error>> {
        let get_teams = get_callback!(self.ai_id, Game_getTeams)?;

        let mut ret = Vec::new();
        for i in 0..unsafe { get_teams(self.ai_id) } {
            ret.push(Team {
                ai_id: self.ai_id,
                team_id: i,
            })
        }

        Ok(ret)
    }

    pub fn enemy_teams(&self) -> Result<Vec<Team>, Box<dyn Error>> {
        let get_enemy_team_funcs = get_callback!(self.ai_id, getEnemyTeams)?;

        let mut team_ids = [-1_i32; MAX_TEAM];
        unsafe { get_enemy_team_funcs(self.ai_id, team_ids.as_mut_ptr(), MAX_TEAM as i32) };
        Ok(team_ids
            .iter()
            .cloned()
            .filter_map(|team_id| {
                if team_id == -1 {
                    None
                } else {
                    Some(Team {
                        ai_id: self.ai_id,
                        team_id,
                    })
                }
            })
            .collect())
    }

    pub fn ally_teams(&self) -> Result<Vec<Team>, Box<dyn Error>> {
        let get_ally_team_funcs = get_callback!(self.ai_id, getAlliedTeams)?;

        let mut team_ids = [-1_i32; MAX_TEAM];
        unsafe { get_ally_team_funcs(self.ai_id, team_ids.as_mut_ptr(), MAX_TEAM as i32) };
        Ok(team_ids
            .iter()
            .cloned()
            .filter_map(|team_id| {
                if team_id == -1 {
                    None
                } else {
                    Some(Team {
                        ai_id: self.ai_id,
                        team_id,
                    })
                }
            })
            .collect())
    }

    pub fn current_team(&self) -> Result<Team, Box<dyn Error>> {
        let current_team_func = get_callback!(self.ai_id, Game_getMyTeam)?;

        Ok(Team {
            ai_id: self.ai_id,
            team_id: unsafe { current_team_func(self.ai_id) },
        })
    }
}

impl Team {
    pub fn color(&self) -> Result<[i16; 3], Box<dyn Error>> {
        let team_color_func = get_callback!(self.ai_id, Game_getTeamColor)?;

        let mut color_ret = [0_i16; 3];
        unsafe { team_color_func(self.ai_id, self.team_id, color_ret.as_mut_ptr()) };

        Ok([color_ret[0], color_ret[1], color_ret[2]])
    }

    pub fn allied(&self, logger: &Logger) -> Result<bool, Box<dyn Error>> {
        let is_allied = get_callback!(self.ai_id, Game_isAllied)?;
        let current_team_id = AIInterface::new(self.ai_id)
            .team_interface()
            .current_team()?
            .team_id;
        let ret = Ok(unsafe { is_allied(self.ai_id, self.team_id, current_team_id) });
        ret
    }

    pub fn all(&self, logger: &Logger) -> Result<TeamAll, Box<dyn Error>> {
        Ok(TeamAll {
            color: self.color()?,
            allied: self.allied(logger)?,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct TeamResource {
    ai_id: i32,
    other_team_id: i32,
    resource_id: i32,
}

#[derive(Debug, Clone)]
pub struct TeamResourceAll {
    name: String,
    income_multiplier: f32,
    current: f32,
    income: f32,
    usage: f32,
    storage: f32,
    pull: f32,
    share: f32,
    sent: f32,
    received: f32,
    excess: f32,
}

impl TeamResource {
    pub fn new(team: &Team, resource: &Resource) -> Self {
        Self {
            ai_id: team.ai_id,
            other_team_id: team.team_id,
            resource_id: resource.resource_id,
        }
    }

    pub fn name(&self) -> Result<String, Box<dyn Error>> {
        Resource {
            ai_id: self.ai_id,
            resource_id: self.resource_id,
        }
        .name()
    }

    pub fn income_multiplier(&self) -> Result<f32, Box<dyn Error>> {
        let income_multiplier_func = get_callback!(self.ai_id, Game_getTeamIncomeMultiplier)?;
        Ok(unsafe { income_multiplier_func(self.ai_id, self.other_team_id) })
    }

    pub fn current(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_current = get_callback!(self.ai_id, Game_getTeamResourceCurrent)?;
        Ok(unsafe { team_resource_current(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn income(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_income = get_callback!(self.ai_id, Game_getTeamResourceIncome)?;
        Ok(unsafe { team_resource_income(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn usage(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_usage = get_callback!(self.ai_id, Game_getTeamResourceUsage)?;
        Ok(unsafe { team_resource_usage(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn storage(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_storage = get_callback!(self.ai_id, Game_getTeamResourceStorage)?;
        Ok(unsafe { team_resource_storage(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn pull(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_pull = get_callback!(self.ai_id, Game_getTeamResourcePull)?;
        Ok(unsafe { team_resource_pull(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn share(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_share = get_callback!(self.ai_id, Game_getTeamResourceShare)?;
        Ok(unsafe { team_resource_share(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn sent(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_sent = get_callback!(self.ai_id, Game_getTeamResourceSent)?;
        Ok(unsafe { team_resource_sent(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn received(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_received = get_callback!(self.ai_id, Game_getTeamResourceReceived)?;
        Ok(unsafe { team_resource_received(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn excess(&self) -> Result<f32, Box<dyn Error>> {
        let team_resource_excess = get_callback!(self.ai_id, Game_getTeamResourceExcess)?;
        Ok(unsafe { team_resource_excess(self.ai_id, self.other_team_id, self.resource_id) })
    }

    pub fn all(&self) -> Result<TeamResourceAll, Box<dyn Error>> {
        Ok(TeamResourceAll {
            name: self.name()?,
            income_multiplier: self.income_multiplier()?,
            current: self.current()?,
            income: self.income()?,
            usage: self.usage()?,
            storage: self.storage()?,
            pull: self.pull()?,
            share: self.share()?,
            sent: self.sent()?,
            received: self.received()?,
            excess: self.excess()?,
        })
    }
}
