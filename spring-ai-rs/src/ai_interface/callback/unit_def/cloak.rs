use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitCloak {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitCloakAll {
    pub able_to_cloak: bool,
    pub start_cloaked: bool,
    pub cloak_cost: f32,
    pub cloak_cost_moving: f32,
    pub decloak_distance: f32,
    pub decloak_spherical: bool,
    pub decloak_on_fire: bool,
}

impl UnitCloak {
    pub fn able_to_cloak(&self) -> Result<bool, Box<dyn Error>> {
        let get_able_to_cloak_func = get_callback!(self.ai_id, UnitDef_isAbleToCloak)?;
        Ok(unsafe { get_able_to_cloak_func(self.ai_id, self.def_id) })
    }

    pub fn start_cloaked(&self) -> Result<bool, Box<dyn Error>> {
        let get_start_cloaked_func = get_callback!(self.ai_id, UnitDef_isStartCloaked)?;
        Ok(unsafe { get_start_cloaked_func(self.ai_id, self.def_id) })
    }

    pub fn cloak_cost(&self) -> Result<f32, Box<dyn Error>> {
        let get_cloak_cost_func = get_callback!(self.ai_id, UnitDef_getCloakCost)?;
        Ok(unsafe { get_cloak_cost_func(self.ai_id, self.def_id) })
    }

    pub fn cloak_cost_moving(&self) -> Result<f32, Box<dyn Error>> {
        let get_cloak_cost_moving_func = get_callback!(self.ai_id, UnitDef_getCloakCostMoving)?;
        Ok(unsafe { get_cloak_cost_moving_func(self.ai_id, self.def_id) })
    }

    pub fn decloak_distance(&self) -> Result<f32, Box<dyn Error>> {
        let get_decloak_distance_func = get_callback!(self.ai_id, UnitDef_getDecloakDistance)?;
        Ok(unsafe { get_decloak_distance_func(self.ai_id, self.def_id) })
    }

    pub fn decloak_spherical(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_decloak_spherical_func = get_callback!(self.ai_id, UnitDef_isDecloakSpherical)?;
        Ok(unsafe { get_is_decloak_spherical_func(self.ai_id, self.def_id) })
    }

    pub fn decloak_on_fire(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_decloak_on_fire_func = get_callback!(self.ai_id, UnitDef_isDecloakOnFire)?;
        Ok(unsafe { get_is_decloak_on_fire_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitCloakAll, Box<dyn Error>> {
        Ok(UnitCloakAll {
            able_to_cloak: self.able_to_cloak()?,
            start_cloaked: self.start_cloaked()?,
            cloak_cost: self.cloak_cost()?,
            cloak_cost_moving: self.cloak_cost_moving()?,
            decloak_distance: self.decloak_distance()?,
            decloak_spherical: self.decloak_spherical()?,
            decloak_on_fire: self.decloak_on_fire()?,
        })
    }
}
