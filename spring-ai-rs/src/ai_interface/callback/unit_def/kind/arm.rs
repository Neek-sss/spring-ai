use crate::ai_interface::callback::unit_def::{kind::base::UnitDefKind, UnitDef};

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum Arm {
    // Commander
    Commander,
    WindTurbine,
    Solar,
    MetalStorage,
    EnergyStorage,
    MetalExtractor,
    EnergyConverter,
    BotLab,
    VehiclePlant,
    AirPlant,
    HovercraftPlatform,
    Camera,
    Radar,
    Teeth,
    LightLaserTower,
    LightAntiAir,
    FloatingLightAntiAir,
    TorpedoLauncher,
    UnderwaterMetalStorage,
    UnderwaterEnergyStorage,
    NavalEnergyConverter,
    Shipyard,
    TidalGenerator,
    FloatingTeeth,
    FloatingRadar,
    FloatingHovercraftPlatform,
    OffshoreTorpedoLauncher,
    // T1 Air Plant
    T1AirConstructor,
    T1AirScout,
    T1Fighter,
    T1Bomber,
    LightParalyzerDrone,
    T1AirTransport,
    // T1 Air Constructor
    AdvancedSolar,
    GeothermalPowerplant,
    ArmedMetalExtractor,
    AircraftRepairPad,
    UnderwaterAdvancedGeothermalPowerplant,
    AdvancedAircraftPlant,
    ConstructionTurret,
    AreaControlLaserTower,
    LightningTurret,
    MediumRangeAntiAir,
    LaserTurret,
    AreaControlPlasmaArtillery,
    PopUpAntiAirMissileBattery,
    RadarJammer,
    AntiRadar,
    // T2 Air Plant
    T2AirConstructor,
    RadarSonarAircraft,
    StealthFighter,
    Gunship,
    AtomicBomber,
    StrategicBomber,
    TorpedoBomber,
    HeavyTransport,
    RapidAssaultGunship,
    EMPBomber,
    // T2 Air Constructor
    FusionReactor,
    AdvancedFusionReactor,
    CloakedFusionReactor,
    AdvancedGeothermalPowerplant,
    GeothermalWeapon,
    Unknown0,
    AdvancedMetalExtractor,
    AdvancedArmoredMetalExtractor,
    WaterAircraftRepairPad,
    HardenedEnergyStorage,
    HardenedMetalStorage,
    AdvancedRadar,
    LongRangeJammer,
    FortificationWall,
    AdvancedEnergyConverter,
    IntrusionDetector,
    PlasmaDeflector,
    EnergyWeapon,
    RadarTargeting,
    PopUpBattery,
    PopUpPlasmaArtillery,
    AntiAirFlak,
    LongRangeAntiAir,
    SeaplanePlatform,
    EMPMissileLauncher,
    TacticalMissileLauncher,
    NuclearSilo,
    AntiNukeLauncher,
    DecoyFusionReactor,
    LongRangePlasmaCannon,
    RapidFireLongRangePlasmaCannon,
    ExperimentalGantry,
    TachyonAccelerator,
    // T1 Bot Lab
    T1BotConstructor,
    T1RessurectionBot,
    FastInfantryBot,
    RocketBot,
    LightPlasmaBot,
    AmphibiousAntiAirBot,
    // T1 Bot Constructor
    AdvancedBotLab,
    // T2 Bot Lab
    T2BotConstructor,
    FastAssaultBot,
    AmphibiousBot,
    HeavilyArmoredAssaultBot,
    ArmoredAssaultBot,
    SpiderBot,
    MortarBot,
    CrawlingBomb,
    HeavyRocketBot,
    AdvancedCrawlingBomb,
    DecoyCommander,
    RadarBot,
    SpyBot,
    StealthBot,
    CombatEngineer,
    JammerBot,
    HeavyAntiAir,
    // Stealth Bot
    Unknown1,
    // T2 Bot Constructor
    // T1 Vehicle Plant
    T1VehicleConstructor,
    AmphibiousConstructor,
    Minelayer,
    LightScoutVehicle,
    LightTank,
    LightAmphibiousTank,
    MediumAssaultTank,
    LightMobileArtillery,
    MissileTruck,
    AntiSwarmTank,
    // T1 Minelayer
    LightMine,
    MediumMine,
    HeavyMine,
    // T1 Vehicle Constructor
    AdvancedVehiclePlant,
    // T2 Vehicle Plant
    T2VehicleConstructor,
    RadarJammerVehicle,
    HeavyAssaultTank,
    StealthyRocketLauncher,
    HeavyMissileTank,
    MobileArtilleryTank,
    HeavyArtilleryTank,
    VeryHeavyAssaultTank,
    MediumAmphibiousTank,
    VeryHeavyAmphibiousTank,
    AntiAirFlakTank,
    AntiNukeTank,
    RadarVehicle,
    // Experimental Gantry
    MobileHeavyTurretExperimental,
    AllTerrainAssaultExperimental,
    AssaultBotExperimental,
    AmphibiousSiegeExperimental,
    HeavyRocketExperimental,
    HeavyLaserHovertankExperimental,
    // T1 Hovercraft Plant
    T1HovercraftConstructor,
    FastAttackHovertank,
    Hovertank,
    AssaultHovertank,
    RocketHovertank,
    AntiAirHovertank,
    // T1 Hovercraft Constructor
    NavalConstructionTurret,
    AmphibiousComplex,
    FloatingHeavyLaserTower,
    AdvancedShipyard,
    UnderwaterGeothermalPowerplant,
    // T1 Amphibious Constructor
    PopUpTorpedoLauncher,
    // T1 Shipyard
    T1ShipConstructor,
    RessurectionSub,
    LightGunBoat,
    MissileCorvette,
    AssaultFrigate,
    Destroyer,
    Sub,
    // T1 Ship Constructor
    // T2 Shipyard
    T2ShipConstructor,
    NavalEngineer,
    Cruiser,
    FastAssaultSubmarine,
    LongRangeBattleSubmarine,
    AntiAirShip,
    RadarJammerShip,
    AircraftCarrier,
    Battleship,
    CruiseMissileShip,
    Flagship,
    // Naval Engineer
    FloatingHeavyMine,
    // T2 Ship Constructor
    UnderwaterExperimentalGantry,
    UnderwaterFusionReactor,
    UnderwaterMetalConverter,
    UnderwaterEnergyConverter,
    AdvancedSonar,
    NavalAdvancedRadarTargeting,
    AdvancedTorpedoLauncher,
    NavalAntiAirGun,
    FloatingMultiweaponPlatform,
    // T1 Seaplane Plant
    SeaplaneConstructor,
    SeaplaneGunship,
    SeaplaneBomber,
    SeaplaneTorpedoGunship,
    SeaplaneFighter,
    SeaplaneRadarSonar,
    // T1 Seaplane Constructor
    Unknown,
}

impl PartialEq<Arm> for UnitDef {
    fn eq(&self, other: &Arm) -> bool {
        *other == self.kind().unwrap_or(UnitDefKind::Unknown)
    }
}

impl PartialEq<UnitDef> for Arm {
    fn eq(&self, other: &UnitDef) -> bool {
        other.kind().unwrap_or(UnitDefKind::Unknown) == *self
    }
}

impl PartialEq<Arm> for UnitDefKind {
    fn eq(&self, other: &Arm) -> bool {
        other == self
    }
}

impl PartialEq<UnitDefKind> for Arm {
    fn eq(&self, other: &UnitDefKind) -> bool {
        match other {
            UnitDefKind::ArmCommander => *self == Self::Commander,
            UnitDefKind::ArmWindTurbine => *self == Self::WindTurbine,
            UnitDefKind::ArmSolar => *self == Self::Solar,
            UnitDefKind::ArmMetalStorage => *self == Self::MetalStorage,
            UnitDefKind::ArmEnergyStorage => *self == Self::EnergyStorage,
            UnitDefKind::ArmMetalExtractor => *self == Self::MetalExtractor,
            UnitDefKind::ArmEnergyConverter => *self == Self::EnergyConverter,
            UnitDefKind::ArmBotLab => *self == Self::BotLab,
            UnitDefKind::ArmVehiclePlant => *self == Self::VehiclePlant,
            UnitDefKind::ArmAirPlant => *self == Self::AirPlant,
            UnitDefKind::ArmHovercraftPlatform => *self == Self::HovercraftPlatform,
            UnitDefKind::ArmCamera => *self == Self::Camera,
            UnitDefKind::ArmRadar => *self == Self::Radar,
            UnitDefKind::ArmTeeth => *self == Self::Teeth,
            UnitDefKind::ArmLightLaserTower => *self == Self::LightLaserTower,
            UnitDefKind::ArmLightAntiAir => *self == Self::LightAntiAir,
            UnitDefKind::ArmFloatingLightAntiAir => *self == Self::FloatingLightAntiAir,
            UnitDefKind::ArmTorpedoLauncher => *self == Self::TorpedoLauncher,
            UnitDefKind::ArmUnderwaterMetalStorage => *self == Self::UnderwaterMetalStorage,
            UnitDefKind::ArmUnderwaterEnergyStorage => *self == Self::UnderwaterEnergyStorage,
            UnitDefKind::ArmNavalEnergyConverter => *self == Self::NavalEnergyConverter,
            UnitDefKind::ArmShipyard => *self == Self::Shipyard,
            UnitDefKind::ArmTidalGenerator => *self == Self::TidalGenerator,
            UnitDefKind::ArmFloatingTeeth => *self == Self::FloatingTeeth,
            UnitDefKind::ArmFloatingRadar => *self == Self::FloatingRadar,
            UnitDefKind::ArmFloatingHovercraftPlatform => *self == Self::FloatingHovercraftPlatform,
            UnitDefKind::ArmOffshoreTorpedoLauncher => *self == Self::OffshoreTorpedoLauncher,
            UnitDefKind::ArmT1AirConstructor => *self == Self::T1AirConstructor,
            UnitDefKind::ArmT1AirScout => *self == Self::T1AirScout,
            UnitDefKind::ArmT1Fighter => *self == Self::T1Fighter,
            UnitDefKind::ArmT1Bomber => *self == Self::T1Bomber,
            UnitDefKind::ArmLightParalyzerDrone => *self == Self::LightParalyzerDrone,
            UnitDefKind::ArmT1AirTransport => *self == Self::T1AirTransport,
            UnitDefKind::ArmAdvancedSolar => *self == Self::AdvancedSolar,
            UnitDefKind::ArmGeothermalPowerplant => *self == Self::GeothermalPowerplant,
            UnitDefKind::ArmArmedMetalExtractor => *self == Self::ArmedMetalExtractor,
            UnitDefKind::ArmAircraftRepairPad => *self == Self::AircraftRepairPad,
            UnitDefKind::ArmUnderwaterAdvancedGeothermalPowerplant => {
                *self == Self::UnderwaterAdvancedGeothermalPowerplant
            }
            UnitDefKind::ArmAdvancedAircraftPlant => *self == Self::AdvancedAircraftPlant,
            UnitDefKind::ArmConstructionTurret => *self == Self::ConstructionTurret,
            UnitDefKind::ArmAreaControlLaserTower => *self == Self::AreaControlLaserTower,
            UnitDefKind::ArmLightningTurret => *self == Self::LightningTurret,
            UnitDefKind::ArmMediumRangeAntiAir => *self == Self::MediumRangeAntiAir,
            UnitDefKind::ArmLaserTurret => *self == Self::LaserTurret,
            UnitDefKind::ArmAreaControlPlasmaArtillery => *self == Self::AreaControlPlasmaArtillery,
            UnitDefKind::ArmPopUpAntiAirMissileBattery => *self == Self::PopUpAntiAirMissileBattery,
            UnitDefKind::ArmRadarJammer => *self == Self::RadarJammer,
            UnitDefKind::ArmAntiRadar => *self == Self::AntiRadar,
            UnitDefKind::ArmT2AirConstructor => *self == Self::T2AirConstructor,
            UnitDefKind::ArmRadarSonarAircraft => *self == Self::RadarSonarAircraft,
            UnitDefKind::ArmStealthFighter => *self == Self::StealthFighter,
            UnitDefKind::ArmGunship => *self == Self::Gunship,
            UnitDefKind::ArmAtomicBomber => *self == Self::AtomicBomber,
            UnitDefKind::ArmStrategicBomber => *self == Self::StrategicBomber,
            UnitDefKind::ArmTorpedoBomber => *self == Self::TorpedoBomber,
            UnitDefKind::ArmRapidAssaultGunship => *self == Self::RapidAssaultGunship,
            UnitDefKind::ArmEMPBomber => *self == Self::EMPBomber,
            UnitDefKind::ArmHeavyTransport => *self == Self::HeavyTransport,
            UnitDefKind::ArmFusionReactor => *self == Self::FusionReactor,
            UnitDefKind::ArmAdvancedFusionReactor => *self == Self::AdvancedFusionReactor,
            UnitDefKind::ArmCloakedFusionReactor => *self == Self::CloakedFusionReactor,
            UnitDefKind::ArmAdvancedGeothermalPowerplant => {
                *self == Self::AdvancedGeothermalPowerplant
            }
            UnitDefKind::ArmGeothermalWeapon => *self == Self::GeothermalWeapon,
            UnitDefKind::ArmAdvancedMetalExtractor => *self == Self::AdvancedMetalExtractor,
            UnitDefKind::ArmAdvancedArmoredMetalExtractor => {
                *self == Self::AdvancedArmoredMetalExtractor
            }
            UnitDefKind::ArmWaterAircraftRepairPad => *self == Self::WaterAircraftRepairPad,
            UnitDefKind::ArmHardenedEnergyStorage => *self == Self::HardenedEnergyStorage,
            UnitDefKind::ArmHardenedMetalStorage => *self == Self::HardenedMetalStorage,
            UnitDefKind::ArmAdvancedRadar => *self == Self::AdvancedRadar,
            UnitDefKind::ArmLongRangeJammer => *self == Self::LongRangeJammer,
            UnitDefKind::ArmFortificationWall => *self == Self::FortificationWall,
            UnitDefKind::ArmAdvancedEnergyConverter => *self == Self::AdvancedEnergyConverter,
            UnitDefKind::ArmIntrusionDetector => *self == Self::IntrusionDetector,
            UnitDefKind::ArmPlasmaDeflector => *self == Self::PlasmaDeflector,
            UnitDefKind::ArmEnergyWeapon => *self == Self::EnergyWeapon,
            UnitDefKind::ArmRadarTargeting => *self == Self::RadarTargeting,
            UnitDefKind::ArmPopUpBattery => *self == Self::PopUpBattery,
            UnitDefKind::ArmPopUpPlasmaArtillery => *self == Self::PopUpPlasmaArtillery,
            UnitDefKind::ArmAntiAirFlak => *self == Self::AntiAirFlak,
            UnitDefKind::ArmLongRangeAntiAir => *self == Self::LongRangeAntiAir,
            UnitDefKind::ArmSeaplanePlatform => *self == Self::SeaplanePlatform,
            UnitDefKind::ArmTacticalMissileLauncher => *self == Self::TacticalMissileLauncher,
            UnitDefKind::ArmNuclearSilo => *self == Self::NuclearSilo,
            UnitDefKind::ArmAntiNukeLauncher => *self == Self::AntiNukeLauncher,
            UnitDefKind::ArmDecoyFusionReactor => *self == Self::DecoyFusionReactor,
            UnitDefKind::ArmLongRangePlasmaCannon => *self == Self::LongRangePlasmaCannon,
            UnitDefKind::ArmRapidFireLongRangePlasmaCannon => {
                *self == Self::RapidFireLongRangePlasmaCannon
            }
            UnitDefKind::ArmExperimentalGantry => *self == Self::ExperimentalGantry,
            UnitDefKind::ArmTachyonAccelerator => *self == Self::TachyonAccelerator,
            UnitDefKind::ArmEMPMissileLauncher => *self == Self::EMPMissileLauncher,
            UnitDefKind::ArmT1BotConstructor => *self == Self::T1BotConstructor,
            UnitDefKind::ArmT1RessurectionBot => *self == Self::T1RessurectionBot,
            UnitDefKind::ArmFastInfantryBot => *self == Self::FastInfantryBot,
            UnitDefKind::ArmRocketBot => *self == Self::RocketBot,
            UnitDefKind::ArmLightPlasmaBot => *self == Self::LightPlasmaBot,
            UnitDefKind::ArmAmphibiousAntiAirBot => *self == Self::AmphibiousAntiAirBot,
            UnitDefKind::ArmAdvancedBotLab => *self == Self::AdvancedBotLab,
            UnitDefKind::ArmT2BotConstructor => *self == Self::T2BotConstructor,
            UnitDefKind::ArmFastAssaultBot => *self == Self::FastAssaultBot,
            UnitDefKind::ArmAmphibiousBot => *self == Self::AmphibiousBot,
            UnitDefKind::ArmHeavilyArmoredAssaultBot => *self == Self::HeavilyArmoredAssaultBot,
            UnitDefKind::ArmArmoredAssaultBot => *self == Self::ArmoredAssaultBot,
            UnitDefKind::ArmSpiderBot => *self == Self::SpiderBot,
            UnitDefKind::ArmMortarBot => *self == Self::MortarBot,
            UnitDefKind::ArmCrawlingBomb => *self == Self::CrawlingBomb,
            UnitDefKind::ArmHeavyRocketBot => *self == Self::HeavyRocketBot,
            UnitDefKind::ArmAdvancedCrawlingBomb => *self == Self::AdvancedCrawlingBomb,
            UnitDefKind::ArmDecoyCommander => *self == Self::DecoyCommander,
            UnitDefKind::ArmRadarBot => *self == Self::RadarBot,
            UnitDefKind::ArmSpyBot => *self == Self::SpyBot,
            UnitDefKind::ArmStealthBot => *self == Self::StealthBot,
            UnitDefKind::ArmCombatEngineer => *self == Self::CombatEngineer,
            UnitDefKind::ArmJammerBot => *self == Self::JammerBot,
            UnitDefKind::ArmHeavyAntiAir => *self == Self::HeavyAntiAir,
            UnitDefKind::ArmT1VehicleConstructor => *self == Self::T1VehicleConstructor,
            UnitDefKind::ArmAmphibiousConstructor => *self == Self::AmphibiousConstructor,
            UnitDefKind::ArmMinelayer => *self == Self::Minelayer,
            UnitDefKind::ArmLightScoutVehicle => *self == Self::LightScoutVehicle,
            UnitDefKind::ArmLightTank => *self == Self::LightTank,
            UnitDefKind::ArmLightAmphibiousTank => *self == Self::LightAmphibiousTank,
            UnitDefKind::ArmMediumAssaultTank => *self == Self::MediumAssaultTank,
            UnitDefKind::ArmLightMobileArtillery => *self == Self::LightMobileArtillery,
            UnitDefKind::ArmMissileTruck => *self == Self::MissileTruck,
            UnitDefKind::ArmAntiSwarmTank => *self == Self::AntiSwarmTank,
            UnitDefKind::ArmLightMine => *self == Self::LightMine,
            UnitDefKind::ArmMediumMine => *self == Self::MediumMine,
            UnitDefKind::ArmHeavyMine => *self == Self::HeavyMine,
            UnitDefKind::ArmAdvancedVehiclePlant => *self == Self::AdvancedVehiclePlant,
            UnitDefKind::ArmT2VehicleConstructor => *self == Self::T2VehicleConstructor,
            UnitDefKind::ArmRadarJammerVehicle => *self == Self::RadarJammerVehicle,
            UnitDefKind::ArmHeavyAssaultTank => *self == Self::HeavyAssaultTank,
            UnitDefKind::ArmStealthyRocketLauncher => *self == Self::StealthyRocketLauncher,
            UnitDefKind::ArmHeavyMissileTank => *self == Self::HeavyMissileTank,
            UnitDefKind::ArmMobileArtilleryTank => *self == Self::MobileArtilleryTank,
            UnitDefKind::ArmHeavyArtilleryTank => *self == Self::HeavyArtilleryTank,
            UnitDefKind::ArmVeryHeavyAssaultTank => *self == Self::VeryHeavyAssaultTank,
            UnitDefKind::ArmMediumAmphibiousTank => *self == Self::MediumAmphibiousTank,
            UnitDefKind::ArmVeryHeavyAmphibiousTank => *self == Self::VeryHeavyAmphibiousTank,
            UnitDefKind::ArmAntiAirFlakTank => *self == Self::AntiAirFlakTank,
            UnitDefKind::ArmAntiNukeTank => *self == Self::AntiNukeTank,
            UnitDefKind::ArmRadarVehicle => *self == Self::RadarVehicle,
            UnitDefKind::ArmMobileHeavyTurretExperimental => {
                *self == Self::MobileHeavyTurretExperimental
            }
            UnitDefKind::ArmAllTerrainAssaultExperimental => {
                *self == Self::AllTerrainAssaultExperimental
            }
            UnitDefKind::ArmAssaultBotExperimental => *self == Self::AssaultBotExperimental,
            UnitDefKind::ArmAmphibiousSiegeExperimental => {
                *self == Self::AmphibiousSiegeExperimental
            }
            UnitDefKind::ArmHeavyRocketExperimental => *self == Self::HeavyRocketExperimental,
            UnitDefKind::ArmHeavyLaserHovertankExperimental => {
                *self == Self::HeavyLaserHovertankExperimental
            }
            UnitDefKind::ArmT1HovercraftConstructor => *self == Self::T1HovercraftConstructor,
            UnitDefKind::ArmFastAttackHovertank => *self == Self::FastAttackHovertank,
            UnitDefKind::ArmHovertank => *self == Self::Hovertank,
            UnitDefKind::ArmAssaultHovertank => *self == Self::AssaultHovertank,
            UnitDefKind::ArmRocketHovertank => *self == Self::RocketHovertank,
            UnitDefKind::ArmAntiAirHovertank => *self == Self::AntiAirHovertank,
            UnitDefKind::ArmNavalConstructionTurret => *self == Self::NavalConstructionTurret,
            UnitDefKind::ArmAmphibiousComplex => *self == Self::AmphibiousComplex,
            UnitDefKind::ArmFloatingHeavyLaserTower => *self == Self::FloatingHeavyLaserTower,
            UnitDefKind::ArmAdvancedShipyard => *self == Self::AdvancedShipyard,
            UnitDefKind::ArmUnderwaterGeothermalPowerplant => {
                *self == Self::UnderwaterGeothermalPowerplant
            }
            UnitDefKind::ArmPopUpTorpedoLauncher => *self == Self::PopUpTorpedoLauncher,
            UnitDefKind::ArmT1ShipConstructor => *self == Self::T1ShipConstructor,
            UnitDefKind::ArmRessurectionSub => *self == Self::RessurectionSub,
            UnitDefKind::ArmLightGunBoat => *self == Self::LightGunBoat,
            UnitDefKind::ArmMissileCorvette => *self == Self::MissileCorvette,
            UnitDefKind::ArmAssaultFrigate => *self == Self::AssaultFrigate,
            UnitDefKind::ArmDestroyer => *self == Self::Destroyer,
            UnitDefKind::ArmSub => *self == Self::Sub,
            UnitDefKind::ArmT2ShipConstructor => *self == Self::T2ShipConstructor,
            UnitDefKind::ArmNavalEngineer => *self == Self::NavalEngineer,
            UnitDefKind::ArmCruiser => *self == Self::Cruiser,
            UnitDefKind::ArmFastAssaultSubmarine => *self == Self::FastAssaultSubmarine,
            UnitDefKind::ArmLongRangeBattleSubmarine => *self == Self::LongRangeBattleSubmarine,
            UnitDefKind::ArmAntiAirShip => *self == Self::AntiAirShip,
            UnitDefKind::ArmRadarJammerShip => *self == Self::RadarJammerShip,
            UnitDefKind::ArmAircraftCarrier => *self == Self::AircraftCarrier,
            UnitDefKind::ArmBattleship => *self == Self::Battleship,
            UnitDefKind::ArmCruiseMissileShip => *self == Self::CruiseMissileShip,
            UnitDefKind::ArmFlagship => *self == Self::Flagship,
            UnitDefKind::ArmFloatingHeavyMine => *self == Self::FloatingHeavyMine,
            UnitDefKind::ArmUnderwaterExperimentalGantry => {
                *self == Self::UnderwaterExperimentalGantry
            }
            UnitDefKind::ArmUnderwaterFusionReactor => *self == Self::UnderwaterFusionReactor,
            UnitDefKind::ArmUnderwaterMetalConverter => *self == Self::UnderwaterMetalConverter,
            UnitDefKind::ArmUnderwaterEnergyConverter => *self == Self::UnderwaterEnergyConverter,
            UnitDefKind::ArmAdvancedSonar => *self == Self::AdvancedSonar,
            UnitDefKind::ArmNavalAdvancedRadarTargeting => {
                *self == Self::NavalAdvancedRadarTargeting
            }
            UnitDefKind::ArmAdvancedTorpedoLauncher => *self == Self::AdvancedTorpedoLauncher,
            UnitDefKind::ArmNavalAntiAirGun => *self == Self::NavalAntiAirGun,
            UnitDefKind::ArmFloatingMultiweaponPlatform => {
                *self == Self::FloatingMultiweaponPlatform
            }
            UnitDefKind::ArmSeaplaneConstructor => *self == Self::SeaplaneConstructor,
            UnitDefKind::ArmSeaplaneGunship => *self == Self::SeaplaneGunship,
            UnitDefKind::ArmSeaplaneBomber => *self == Self::SeaplaneBomber,
            UnitDefKind::ArmSeaplaneTorpedoGunship => *self == Self::SeaplaneTorpedoGunship,
            UnitDefKind::ArmSeaplaneFighter => *self == Self::SeaplaneFighter,
            UnitDefKind::ArmSeaplaneRadarSonar => *self == Self::SeaplaneRadarSonar,
            UnitDefKind::Unknown => *self == Self::Unknown,
            _ => false,
        }
    }
}
