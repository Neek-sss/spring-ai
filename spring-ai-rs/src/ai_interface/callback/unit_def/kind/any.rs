use crate::ai_interface::callback::unit_def::{kind::base::UnitDefKind, UnitDef};

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum Any {
    // Commander
    Commander,
    WindTurbine,
    Solar,
    MetalStorage,
    EnergyStorage,
    MetalExtractor,
    EnergyConverter,
    BotLab,
    VehiclePlant,
    AirPlant,
    HovercraftPlatform,
    Camera,
    Radar,
    Teeth,
    LightLaserTower,
    LightAntiAir,
    FloatingLightAntiAir,
    TorpedoLauncher,
    UnderwaterMetalStorage,
    UnderwaterEnergyStorage,
    NavalEnergyConverter,
    Shipyard,
    TidalGenerator,
    FloatingTeeth,
    FloatingRadar,
    FloatingHovercraftPlatform,
    OffshoreTorpedoLauncher,
    // T1 Air Plant
    T1AirConstructor,
    T1AirScout,
    T1Fighter,
    T1Bomber,
    LightParalyzerDrone,
    T1AirTransport,
    // T1 Air Constructor
    AdvancedSolar,
    GeothermalPowerplant,
    ArmedMetalExtractor,
    AircraftRepairPad,
    UnderwaterAdvancedGeothermalPowerplant,
    AdvancedAircraftPlant,
    ConstructionTurret,
    AreaControlLaserTower,
    ElementalTurret,
    LaserTower,
    Artillery,
    AntiAirMissle,
    RadarJammer,
    AntiRadar,
    // T2 Air Plant
    T2AirConstructor,
    RadarSonarAircraft,
    StealthFighter,
    Gunship,
    StrategicBomber,
    TorpedoBomber,
    HeavyTransport,
    // T2 Air Constructor
    FusionReactor,
    AdvancedFusionReactor,
    AdvancedGeothermalPowerplant,
    GeothermalWeapon,
    Unknown0,
    AdvancedMetalExtractor,
    AdvancedArmoredMetalExtractor,
    WaterAircraftRepairPad,
    HardenedEnergyStorage,
    HardenedMetalStorage,
    AdvancedRadar,
    LongRangeJammer,
    FortificationWall,
    AdvancedEnergyConverter,
    IntrusionDetector,
    PlasmaDeflector,
    EnergyWeapon,
    RadarTargeting,
    PopUpBattery,
    PopUpPlasmaArtillery,
    AntiAirFlak,
    LongRangeAntiAir,
    SeaplanePlatform,
    TacticalMissileLauncher,
    NuclearSilo,
    AntiNukeLauncher,
    LongRangePlasmaCannon,
    RapidFireLongRangePlasmaCannon,
    ExperimentalGantry,
    // T1 Bot Lab
    T1BotConstructor,
    T1RessurectionBot,
    FastInfantryBot,
    RocketBot,
    LightPlasmaBot,
    AmphibiousAntiAirBot,
    // T1 Bot Constructor
    AdvancedBotLab,
    // T2 Bot Lab
    T2BotConstructor,
    FastAssaultBot,
    AmphibiousBot,
    HeavilyArmoredAssaultBot,
    ArmoredAssaultBot,
    SpiderBot,
    MortarBot,
    CrawlingBomb,
    HeavyRocketBot,
    AdvancedCrawlingBomb,
    DecoyCommander,
    RadarBot,
    SpyBot,
    StealthBot,
    CombatEngineer,
    JammerBot,
    HeavyAntiAir,
    // Stealth Bot
    Unknown1,
    // T2 Bot Constructor
    // T1 Vehicle Plant
    T1VehicleConstructor,
    AmphibiousConstructor,
    Minelayer,
    LightScoutVehicle,
    LightTank,
    LightAmphibiousTank,
    MediumAssaultTank,
    LightMobileArtillery,
    MissileTruck,
    AntiSwarmTank,
    // T1 Minelayer
    LightMine,
    MediumMine,
    HeavyMine,
    // T1 Vehicle Constructor
    AdvancedVehiclePlant,
    // T2 Vehicle Plant
    T2VehicleConstructor,
    RadarJammerVehicle,
    HeavyAssaultTank,
    StealthyRocketLauncher,
    HeavyMissileTank,
    MobileArtilleryTank,
    HeavyArtilleryTank,
    VeryHeavyAssaultTank,
    MediumAmphibiousTank,
    HeavyAmphibiousTank,
    VeryHeavyAmphibiousTank,
    AntiAirFlakTank,
    AntiNukeTank,
    RadarVehicle,
    // Experimental Gantry
    MobileHeavyTurretExperimental,
    AllTerrainAssaultExperimental,
    AssaultBotExperimental,
    AmphibiousSiegeExperimental,
    HeavyRocketExperimental,
    HeavyLaserHovertankExperimental,
    // T1 Hovercraft Plant
    T1HovercraftConstructor,
    FastAttackHovertank,
    Hovertank,
    AssaultHovertank,
    RocketHovertank,
    AntiAirHovertank,
    // T1 Hovercraft Constructor
    NavalConstructionTurret,
    AmphibiousComplex,
    FloatingHeavyLaserTower,
    AdvancedShipyard,
    UnderwaterGeothermalPowerplant,
    // T1 Amphibious Constructor
    PopUpTorpedoLauncher,
    // T1 Shipyard
    T1ShipConstructor,
    RessurectionSub,
    LightGunBoat,
    MissileCorvette,
    AssaultFrigate,
    Destroyer,
    Sub,
    // T1 Ship Constructor
    // T2 Shipyard
    T2ShipConstructor,
    NavalEngineer,
    Cruiser,
    FastAssaultSubmarine,
    LongRangeBattleSubmarine,
    AntiAirShip,
    RadarJammerShip,
    AircraftCarrier,
    Battleship,
    CruiseMissileShip,
    Flagship,
    // Naval Engineer
    FloatingHeavyMine,
    // T2 Ship Constructor
    UnderwaterExperimentalGantry,
    UnderwaterFusionReactor,
    UnderwaterMetalConverter,
    UnderwaterEnergyConverter,
    AdvancedSonar,
    NavalAdvancedRadarTargeting,
    AdvancedTorpedoLauncher,
    NavalAntiAirGun,
    FloatingMultiweaponPlatform,
    // T1 Seaplane Plant
    SeaplaneConstructor,
    SeaplaneGunship,
    SeaplaneBomber,
    SeaplaneTorpedoGunship,
    SeaplaneFighter,
    SeaplaneRadarSonar,
    // T1 Seaplane Constructor
    Unknown,
}

impl PartialEq<Any> for UnitDef {
    fn eq(&self, other: &Any) -> bool {
        *other == self.kind().unwrap_or(UnitDefKind::Unknown)
    }
}

impl PartialEq<UnitDef> for Any {
    fn eq(&self, other: &UnitDef) -> bool {
        other.kind().unwrap_or(UnitDefKind::Unknown) == *self
    }
}

impl PartialEq<Any> for UnitDefKind {
    fn eq(&self, other: &Any) -> bool {
        other == self
    }
}

impl PartialEq<UnitDefKind> for Any {
    fn eq(&self, other: &UnitDefKind) -> bool {
        match other {
            UnitDefKind::CoreCommander | UnitDefKind::ArmCommander => *self == Self::Commander,
            UnitDefKind::CoreWindTurbine | UnitDefKind::ArmWindTurbine => {
                *self == Self::WindTurbine
            }
            UnitDefKind::CoreSolar | UnitDefKind::ArmSolar => *self == Self::Solar,
            UnitDefKind::CoreMetalStorage | UnitDefKind::ArmMetalStorage => {
                *self == Self::MetalStorage
            }
            UnitDefKind::CoreEnergyStorage | UnitDefKind::ArmEnergyStorage => {
                *self == Self::EnergyStorage
            }
            UnitDefKind::CoreMetalExtractor | UnitDefKind::ArmMetalExtractor => {
                *self == Self::MetalExtractor
            }
            UnitDefKind::CoreEnergyConverter | UnitDefKind::ArmEnergyConverter => {
                *self == Self::EnergyConverter
            }
            UnitDefKind::CoreBotLab | UnitDefKind::ArmBotLab => *self == Self::BotLab,
            UnitDefKind::CoreVehiclePlant | UnitDefKind::ArmVehiclePlant => {
                *self == Self::VehiclePlant
            }
            UnitDefKind::CoreAirPlant | UnitDefKind::ArmAirPlant => *self == Self::AirPlant,
            UnitDefKind::CoreHovercraftPlatform | UnitDefKind::ArmHovercraftPlatform => {
                *self == Self::HovercraftPlatform
            }
            UnitDefKind::CoreCamera | UnitDefKind::ArmCamera => *self == Self::Camera,
            UnitDefKind::CoreRadar | UnitDefKind::ArmRadar => *self == Self::Radar,
            UnitDefKind::CoreTeeth | UnitDefKind::ArmTeeth => *self == Self::Teeth,
            UnitDefKind::CoreLightLaserTower | UnitDefKind::ArmLightLaserTower => {
                *self == Self::LightLaserTower
            }
            UnitDefKind::CoreLightAntiAir | UnitDefKind::ArmLightAntiAir => {
                *self == Self::LightAntiAir
            }
            UnitDefKind::CoreFloatingLightAntiAir | UnitDefKind::ArmFloatingLightAntiAir => {
                *self == Self::FloatingLightAntiAir
            }
            UnitDefKind::CoreTorpedoLauncher | UnitDefKind::ArmTorpedoLauncher => {
                *self == Self::TorpedoLauncher
            }
            UnitDefKind::CoreUnderwaterMetalStorage | UnitDefKind::ArmUnderwaterMetalStorage => {
                *self == Self::UnderwaterMetalStorage
            }
            UnitDefKind::CoreUnderwaterEnergyStorage | UnitDefKind::ArmUnderwaterEnergyStorage => {
                *self == Self::UnderwaterEnergyStorage
            }
            UnitDefKind::CoreNavalEnergyConverter | UnitDefKind::ArmNavalEnergyConverter => {
                *self == Self::NavalEnergyConverter
            }
            UnitDefKind::CoreShipyard | UnitDefKind::ArmShipyard => *self == Self::Shipyard,
            UnitDefKind::CoreTidalGenerator | UnitDefKind::ArmTidalGenerator => {
                *self == Self::TidalGenerator
            }
            UnitDefKind::CoreFloatingTeeth | UnitDefKind::ArmFloatingTeeth => {
                *self == Self::FloatingTeeth
            }
            UnitDefKind::CoreFloatingRadar | UnitDefKind::ArmFloatingRadar => {
                *self == Self::FloatingRadar
            }
            UnitDefKind::CoreFloatingHovercraftPlatform
            | UnitDefKind::ArmFloatingHovercraftPlatform => {
                *self == Self::FloatingHovercraftPlatform
            }
            UnitDefKind::CoreOffshoreTorpedoLauncher | UnitDefKind::ArmOffshoreTorpedoLauncher => {
                *self == Self::OffshoreTorpedoLauncher
            }
            UnitDefKind::CoreT1AirConstructor | UnitDefKind::ArmT1AirConstructor => {
                *self == Self::T1AirConstructor
            }
            UnitDefKind::CoreT1AirScout | UnitDefKind::ArmT1AirScout => *self == Self::T1AirScout,
            UnitDefKind::CoreT1Fighter | UnitDefKind::ArmT1Fighter => *self == Self::T1Fighter,
            UnitDefKind::CoreT1Bomber | UnitDefKind::ArmT1Bomber => *self == Self::T1Bomber,
            UnitDefKind::CoreLightParalyzerDrone | UnitDefKind::ArmLightParalyzerDrone => {
                *self == Self::LightParalyzerDrone
            }
            UnitDefKind::CoreT1AirTransport | UnitDefKind::ArmT1AirTransport => {
                *self == Self::T1AirTransport
            }
            UnitDefKind::CoreAdvancedSolar | UnitDefKind::ArmAdvancedSolar => {
                *self == Self::AdvancedSolar
            }
            UnitDefKind::CoreGeothermalPowerplant | UnitDefKind::ArmGeothermalPowerplant => {
                *self == Self::GeothermalPowerplant
            }
            UnitDefKind::CoreArmedMetalExtractor | UnitDefKind::ArmArmedMetalExtractor => {
                *self == Self::ArmedMetalExtractor
            }
            UnitDefKind::CoreAircraftRepairPad | UnitDefKind::ArmAircraftRepairPad => {
                *self == Self::AircraftRepairPad
            }
            UnitDefKind::CoreUnderwaterAdvancedGeothermalPowerplant
            | UnitDefKind::ArmUnderwaterAdvancedGeothermalPowerplant => {
                *self == Self::UnderwaterAdvancedGeothermalPowerplant
            }
            UnitDefKind::CoreAdvancedAircraftPlant | UnitDefKind::ArmAdvancedAircraftPlant => {
                *self == Self::AdvancedAircraftPlant
            }
            UnitDefKind::CoreConstructionTurret | UnitDefKind::ArmConstructionTurret => {
                *self == Self::ConstructionTurret
            }
            UnitDefKind::CoreAreaControlLaserTower | UnitDefKind::ArmAreaControlLaserTower => {
                *self == Self::AreaControlLaserTower
            }
            UnitDefKind::CoreFlamethrowerTurret | UnitDefKind::ArmLightningTurret => {
                *self == Self::ElementalTurret
            }
            UnitDefKind::CoreAntiSwarmLaserTower | UnitDefKind::ArmLaserTurret => {
                *self == Self::LaserTower
            }
            UnitDefKind::CoreArtillery | UnitDefKind::ArmAreaControlPlasmaArtillery => {
                *self == Self::Artillery
            }
            UnitDefKind::CoreAntiAirMissle | UnitDefKind::ArmPopUpAntiAirMissileBattery => {
                *self == Self::AntiAirMissle
            }
            UnitDefKind::CoreRadarJammer | UnitDefKind::ArmRadarJammer => {
                *self == Self::RadarJammer
            }
            UnitDefKind::CoreAntiRadar | UnitDefKind::ArmAntiRadar => *self == Self::AntiRadar,
            UnitDefKind::CoreT2AirConstructor | UnitDefKind::ArmT2AirConstructor => {
                *self == Self::T2AirConstructor
            }
            UnitDefKind::CoreRadarSonarAircraft | UnitDefKind::ArmRadarSonarAircraft => {
                *self == Self::RadarSonarAircraft
            }
            UnitDefKind::CoreStealthFighter | UnitDefKind::ArmStealthFighter => {
                *self == Self::StealthFighter
            }
            UnitDefKind::CoreGunship | UnitDefKind::ArmGunship => *self == Self::Gunship,
            UnitDefKind::CoreStrategicBomber | UnitDefKind::ArmStrategicBomber => {
                *self == Self::StrategicBomber
            }
            UnitDefKind::CoreTorpedoBomber | UnitDefKind::ArmTorpedoBomber => {
                *self == Self::TorpedoBomber
            }
            UnitDefKind::CoreHeavyTransport | UnitDefKind::ArmHeavyTransport => {
                *self == Self::HeavyTransport
            }
            UnitDefKind::CoreFusionReactor | UnitDefKind::ArmFusionReactor => {
                *self == Self::FusionReactor
            }
            UnitDefKind::CoreAdvancedFusionReactor | UnitDefKind::ArmAdvancedFusionReactor => {
                *self == Self::AdvancedFusionReactor
            }
            UnitDefKind::CoreAdvancedGeothermalPowerplant
            | UnitDefKind::ArmAdvancedGeothermalPowerplant => {
                *self == Self::AdvancedGeothermalPowerplant
            }
            UnitDefKind::CoreGeothermalWeapon | UnitDefKind::ArmGeothermalWeapon => {
                *self == Self::GeothermalWeapon
            }
            UnitDefKind::CoreAdvancedMetalExtractor | UnitDefKind::ArmAdvancedMetalExtractor => {
                *self == Self::AdvancedMetalExtractor
            }
            UnitDefKind::CoreAdvancedArmoredMetalExtractor
            | UnitDefKind::ArmAdvancedArmoredMetalExtractor => {
                *self == Self::AdvancedArmoredMetalExtractor
            }
            UnitDefKind::CoreWaterAircraftRepairPad | UnitDefKind::ArmWaterAircraftRepairPad => {
                *self == Self::WaterAircraftRepairPad
            }
            UnitDefKind::CoreHardenedEnergyStorage | UnitDefKind::ArmHardenedEnergyStorage => {
                *self == Self::HardenedEnergyStorage
            }
            UnitDefKind::CoreHardenedMetalStorage | UnitDefKind::ArmHardenedMetalStorage => {
                *self == Self::HardenedMetalStorage
            }
            UnitDefKind::CoreAdvancedRadar | UnitDefKind::ArmAdvancedRadar => {
                *self == Self::AdvancedRadar
            }
            UnitDefKind::CoreLongRangeJammer | UnitDefKind::ArmLongRangeJammer => {
                *self == Self::LongRangeJammer
            }
            UnitDefKind::CoreFortificationWall | UnitDefKind::ArmFortificationWall => {
                *self == Self::FortificationWall
            }
            UnitDefKind::CoreAdvancedEnergyConverter | UnitDefKind::ArmAdvancedEnergyConverter => {
                *self == Self::AdvancedEnergyConverter
            }
            UnitDefKind::CoreIntrusionDetector | UnitDefKind::ArmIntrusionDetector => {
                *self == Self::IntrusionDetector
            }
            UnitDefKind::CorePlasmaDeflector | UnitDefKind::ArmPlasmaDeflector => {
                *self == Self::PlasmaDeflector
            }
            UnitDefKind::CoreEnergyWeapon | UnitDefKind::ArmEnergyWeapon => {
                *self == Self::EnergyWeapon
            }
            UnitDefKind::CoreRadarTargeting | UnitDefKind::ArmRadarTargeting => {
                *self == Self::RadarTargeting
            }
            UnitDefKind::CorePopUpBattery | UnitDefKind::ArmPopUpBattery => {
                *self == Self::PopUpBattery
            }
            UnitDefKind::CorePopUpPlasmaArtillery | UnitDefKind::ArmPopUpPlasmaArtillery => {
                *self == Self::PopUpPlasmaArtillery
            }
            UnitDefKind::CoreAntiAirFlak | UnitDefKind::ArmAntiAirFlak => {
                *self == Self::AntiAirFlak
            }
            UnitDefKind::CoreLongRangeAntiAir | UnitDefKind::ArmLongRangeAntiAir => {
                *self == Self::LongRangeAntiAir
            }
            UnitDefKind::CoreSeaplanePlatform | UnitDefKind::ArmSeaplanePlatform => {
                *self == Self::SeaplanePlatform
            }
            UnitDefKind::CoreTacticalMissileLauncher | UnitDefKind::ArmTacticalMissileLauncher => {
                *self == Self::TacticalMissileLauncher
            }
            UnitDefKind::CoreNuclearSilo | UnitDefKind::ArmNuclearSilo => {
                *self == Self::NuclearSilo
            }
            UnitDefKind::CoreAntiNukeLauncher | UnitDefKind::ArmAntiNukeLauncher => {
                *self == Self::AntiNukeLauncher
            }
            UnitDefKind::CoreLongRangePlasmaCannon | UnitDefKind::ArmLongRangePlasmaCannon => {
                *self == Self::LongRangePlasmaCannon
            }
            UnitDefKind::CoreRapidFireLongRangePlasmaCannon
            | UnitDefKind::ArmRapidFireLongRangePlasmaCannon => {
                *self == Self::RapidFireLongRangePlasmaCannon
            }
            UnitDefKind::CoreExperimentalGantry | UnitDefKind::ArmExperimentalGantry => {
                *self == Self::ExperimentalGantry
            }
            UnitDefKind::CoreT1BotConstructor | UnitDefKind::ArmT1BotConstructor => {
                *self == Self::T1BotConstructor
            }
            UnitDefKind::CoreT1RessurectionBot | UnitDefKind::ArmT1RessurectionBot => {
                *self == Self::T1RessurectionBot
            }
            UnitDefKind::CoreFastInfantryBot | UnitDefKind::ArmFastInfantryBot => {
                *self == Self::FastInfantryBot
            }
            UnitDefKind::CoreRocketBot | UnitDefKind::ArmRocketBot => *self == Self::RocketBot,
            UnitDefKind::CoreLightPlasmaBot | UnitDefKind::ArmLightPlasmaBot => {
                *self == Self::LightPlasmaBot
            }
            UnitDefKind::CoreAmphibiousAntiAirBot | UnitDefKind::ArmAmphibiousAntiAirBot => {
                *self == Self::AmphibiousAntiAirBot
            }
            UnitDefKind::CoreAdvancedBotLab | UnitDefKind::ArmAdvancedBotLab => {
                *self == Self::AdvancedBotLab
            }
            UnitDefKind::CoreT2BotConstructor | UnitDefKind::ArmT2BotConstructor => {
                *self == Self::T2BotConstructor
            }
            UnitDefKind::CoreFastAssaultBot | UnitDefKind::ArmFastAssaultBot => {
                *self == Self::FastAssaultBot
            }
            UnitDefKind::CoreAmphibiousBot | UnitDefKind::ArmAmphibiousBot => {
                *self == Self::AmphibiousBot
            }
            UnitDefKind::CoreHeavilyArmoredAssaultBot
            | UnitDefKind::ArmHeavilyArmoredAssaultBot => *self == Self::HeavilyArmoredAssaultBot,
            UnitDefKind::CoreArmoredAssaultBot | UnitDefKind::ArmArmoredAssaultBot => {
                *self == Self::MetalStorage
            }
            UnitDefKind::CoreSpiderBot | UnitDefKind::ArmSpiderBot => *self == Self::SpiderBot,
            UnitDefKind::CoreMortarBot | UnitDefKind::ArmMortarBot => *self == Self::MortarBot,
            UnitDefKind::CoreCrawlingBomb | UnitDefKind::ArmCrawlingBomb => {
                *self == Self::CrawlingBomb
            }
            UnitDefKind::CoreHeavyRocketBot | UnitDefKind::ArmHeavyRocketBot => {
                *self == Self::HeavyRocketBot
            }
            UnitDefKind::CoreAdvancedCrawlingBomb | UnitDefKind::ArmAdvancedCrawlingBomb => {
                *self == Self::AdvancedCrawlingBomb
            }
            UnitDefKind::CoreDecoyCommander | UnitDefKind::ArmDecoyCommander => {
                *self == Self::DecoyCommander
            }
            UnitDefKind::CoreRadarBot | UnitDefKind::ArmRadarBot => *self == Self::RadarBot,
            UnitDefKind::CoreSpyBot | UnitDefKind::ArmSpyBot => *self == Self::SpyBot,
            UnitDefKind::CoreStealthBot | UnitDefKind::ArmStealthBot => *self == Self::StealthBot,
            UnitDefKind::CoreCombatEngineer | UnitDefKind::ArmCombatEngineer => {
                *self == Self::CombatEngineer
            }
            UnitDefKind::CoreJammerBot | UnitDefKind::ArmJammerBot => *self == Self::JammerBot,
            UnitDefKind::CoreHeavyAntiAir | UnitDefKind::ArmHeavyAntiAir => {
                *self == Self::HeavyAntiAir
            }
            UnitDefKind::CoreT1VehicleConstructor | UnitDefKind::ArmT1VehicleConstructor => {
                *self == Self::T1VehicleConstructor
            }
            UnitDefKind::CoreAmphibiousConstructor | UnitDefKind::ArmAmphibiousConstructor => {
                *self == Self::AmphibiousConstructor
            }
            UnitDefKind::CoreMinelayer | UnitDefKind::ArmMinelayer => *self == Self::Minelayer,
            UnitDefKind::CoreLightScoutVehicle | UnitDefKind::ArmLightScoutVehicle => {
                *self == Self::LightScoutVehicle
            }
            UnitDefKind::CoreLightTank | UnitDefKind::ArmLightTank => *self == Self::LightTank,
            UnitDefKind::CoreLightAmphibiousTank | UnitDefKind::ArmLightAmphibiousTank => {
                *self == Self::LightAmphibiousTank
            }
            UnitDefKind::CoreMediumAssaultTank | UnitDefKind::ArmMediumAssaultTank => {
                *self == Self::MediumAssaultTank
            }
            UnitDefKind::CoreLightMobileArtillery | UnitDefKind::ArmLightMobileArtillery => {
                *self == Self::LightMobileArtillery
            }
            UnitDefKind::CoreMissileTruck | UnitDefKind::ArmMissileTruck => {
                *self == Self::MissileTruck
            }
            UnitDefKind::CoreAntiSwarmTank | UnitDefKind::ArmAntiSwarmTank => {
                *self == Self::AntiSwarmTank
            }
            UnitDefKind::CoreLightMine | UnitDefKind::ArmLightMine => *self == Self::LightMine,
            UnitDefKind::CoreMediumMine | UnitDefKind::ArmMediumMine => *self == Self::MediumMine,
            UnitDefKind::CoreHeavyMine | UnitDefKind::ArmHeavyMine => *self == Self::HeavyMine,
            UnitDefKind::CoreAdvancedVehiclePlant | UnitDefKind::ArmAdvancedVehiclePlant => {
                *self == Self::AdvancedVehiclePlant
            }
            UnitDefKind::CoreT2VehicleConstructor | UnitDefKind::ArmT2VehicleConstructor => {
                *self == Self::MetalStorage
            }
            UnitDefKind::CoreRadarJammerVehicle | UnitDefKind::ArmRadarJammerVehicle => {
                *self == Self::RadarJammerVehicle
            }
            UnitDefKind::CoreHeavyAssaultTank | UnitDefKind::ArmHeavyAssaultTank => {
                *self == Self::HeavyAssaultTank
            }
            UnitDefKind::CoreStealthyRocketLauncher | UnitDefKind::ArmStealthyRocketLauncher => {
                *self == Self::StealthyRocketLauncher
            }
            UnitDefKind::CoreHeavyMissileTank | UnitDefKind::ArmHeavyMissileTank => {
                *self == Self::HeavyMissileTank
            }
            UnitDefKind::CoreMobileArtilleryTank | UnitDefKind::ArmMobileArtilleryTank => {
                *self == Self::MobileArtilleryTank
            }
            UnitDefKind::CoreHeavyArtilleryTank | UnitDefKind::ArmHeavyArtilleryTank => {
                *self == Self::HeavyArtilleryTank
            }
            UnitDefKind::CoreVeryHeavyAssaultTank | UnitDefKind::ArmVeryHeavyAssaultTank => {
                *self == Self::VeryHeavyAssaultTank
            }
            UnitDefKind::CoreMediumAmphibiousTank | UnitDefKind::ArmMediumAmphibiousTank => {
                *self == Self::MediumAmphibiousTank
            }
            UnitDefKind::ArmHeavyAmphibiousTank => {
                *self == Self::HeavyAmphibiousTank
            }
            UnitDefKind::CoreVeryHeavyAmphibiousTank | UnitDefKind::ArmVeryHeavyAmphibiousTank => {
                *self == Self::VeryHeavyAmphibiousTank
            }
            UnitDefKind::CoreAntiAirFlakTank | UnitDefKind::ArmAntiAirFlakTank => {
                *self == Self::AntiAirFlakTank
            }
            UnitDefKind::CoreAntiNukeTank | UnitDefKind::ArmAntiNukeTank => {
                *self == Self::AntiNukeTank
            }
            UnitDefKind::CoreRadarVehicle | UnitDefKind::ArmRadarVehicle => {
                *self == Self::RadarVehicle
            }
            UnitDefKind::CoreMobileHeavyTurretExperimental
            | UnitDefKind::ArmMobileHeavyTurretExperimental => {
                *self == Self::MobileHeavyTurretExperimental
            }
            UnitDefKind::CoreAllTerrainAssaultExperimental
            | UnitDefKind::ArmAllTerrainAssaultExperimental => {
                *self == Self::AllTerrainAssaultExperimental
            }
            UnitDefKind::CoreAssaultBotExperimental | UnitDefKind::ArmAssaultBotExperimental => {
                *self == Self::AssaultBotExperimental
            }
            UnitDefKind::CoreAmphibiousSiegeExperimental
            | UnitDefKind::ArmAmphibiousSiegeExperimental => {
                *self == Self::AmphibiousSiegeExperimental
            }
            UnitDefKind::CoreHeavyRocketExperimental | UnitDefKind::ArmHeavyRocketExperimental => {
                *self == Self::HeavyRocketExperimental
            }
            UnitDefKind::CoreHeavyLaserHovertankExperimental
            | UnitDefKind::ArmHeavyLaserHovertankExperimental => {
                *self == Self::HeavyLaserHovertankExperimental
            }
            UnitDefKind::CoreT1HovercraftConstructor | UnitDefKind::ArmT1HovercraftConstructor => {
                *self == Self::T1HovercraftConstructor
            }
            UnitDefKind::CoreFastAttackHovertank | UnitDefKind::ArmFastAttackHovertank => {
                *self == Self::FastAttackHovertank
            }
            UnitDefKind::CoreHovertank | UnitDefKind::ArmHovertank => *self == Self::Hovertank,
            UnitDefKind::CoreAssaultHovertank | UnitDefKind::ArmAssaultHovertank => {
                *self == Self::AssaultHovertank
            }
            UnitDefKind::CoreRocketHovertank | UnitDefKind::ArmRocketHovertank => {
                *self == Self::RocketHovertank
            }
            UnitDefKind::CoreAntiAirHovertank | UnitDefKind::ArmAntiAirHovertank => {
                *self == Self::AntiAirHovertank
            }
            UnitDefKind::CoreNavalConstructionTurret | UnitDefKind::ArmNavalConstructionTurret => {
                *self == Self::NavalConstructionTurret
            }
            UnitDefKind::CoreAmphibiousComplex | UnitDefKind::ArmAmphibiousComplex => {
                *self == Self::AmphibiousComplex
            }
            UnitDefKind::CoreFloatingHeavyLaserTower | UnitDefKind::ArmFloatingHeavyLaserTower => {
                *self == Self::FloatingHeavyLaserTower
            }
            UnitDefKind::CoreAdvancedShipyard | UnitDefKind::ArmAdvancedShipyard => {
                *self == Self::AdvancedShipyard
            }
            UnitDefKind::CoreUnderwaterGeothermalPowerplant
            | UnitDefKind::ArmUnderwaterGeothermalPowerplant => {
                *self == Self::UnderwaterGeothermalPowerplant
            }
            UnitDefKind::CorePopUpTorpedoLauncher | UnitDefKind::ArmPopUpTorpedoLauncher => {
                *self == Self::PopUpTorpedoLauncher
            }
            UnitDefKind::CoreT1ShipConstructor | UnitDefKind::ArmT1ShipConstructor => {
                *self == Self::T1ShipConstructor
            }
            UnitDefKind::CoreRessurectionSub | UnitDefKind::ArmRessurectionSub => {
                *self == Self::RessurectionSub
            }
            UnitDefKind::CoreLightGunBoat | UnitDefKind::ArmLightGunBoat => {
                *self == Self::LightGunBoat
            }
            UnitDefKind::CoreMissileCorvette | UnitDefKind::ArmMissileCorvette => {
                *self == Self::MissileCorvette
            }
            UnitDefKind::CoreAssaultFrigate | UnitDefKind::ArmAssaultFrigate => {
                *self == Self::AssaultFrigate
            }
            UnitDefKind::CoreDestroyer | UnitDefKind::ArmDestroyer => *self == Self::Destroyer,
            UnitDefKind::CoreSub | UnitDefKind::ArmSub => *self == Self::Sub,
            UnitDefKind::CoreT2ShipConstructor | UnitDefKind::ArmT2ShipConstructor => {
                *self == Self::T2ShipConstructor
            }
            UnitDefKind::CoreNavalEngineer | UnitDefKind::ArmNavalEngineer => {
                *self == Self::NavalEngineer
            }
            UnitDefKind::CoreCruiser | UnitDefKind::ArmCruiser => *self == Self::Cruiser,
            UnitDefKind::CoreFastAssaultSubmarine | UnitDefKind::ArmFastAssaultSubmarine => {
                *self == Self::FastAssaultSubmarine
            }
            UnitDefKind::CoreLongRangeBattleSubmarine
            | UnitDefKind::ArmLongRangeBattleSubmarine => *self == Self::LongRangeBattleSubmarine,
            UnitDefKind::CoreAntiAirShip | UnitDefKind::ArmAntiAirShip => {
                *self == Self::AntiAirShip
            }
            UnitDefKind::CoreRadarJammerShip | UnitDefKind::ArmRadarJammerShip => {
                *self == Self::RadarJammerShip
            }
            UnitDefKind::CoreAircraftCarrier | UnitDefKind::ArmAircraftCarrier => {
                *self == Self::AircraftCarrier
            }
            UnitDefKind::CoreBattleship | UnitDefKind::ArmBattleship => *self == Self::Battleship,
            UnitDefKind::CoreCruiseMissileShip | UnitDefKind::ArmCruiseMissileShip => {
                *self == Self::CruiseMissileShip
            }
            UnitDefKind::CoreFlagship | UnitDefKind::ArmFlagship => *self == Self::Flagship,
            UnitDefKind::CoreFloatingHeavyMine | UnitDefKind::ArmFloatingHeavyMine => {
                *self == Self::FloatingHeavyMine
            }
            UnitDefKind::CoreUnderwaterExperimentalGantry
            | UnitDefKind::ArmUnderwaterExperimentalGantry => {
                *self == Self::UnderwaterExperimentalGantry
            }
            UnitDefKind::CoreUnderwaterFusionReactor | UnitDefKind::ArmUnderwaterFusionReactor => {
                *self == Self::MetalStorage
            }
            UnitDefKind::CoreUnderwaterMetalConverter
            | UnitDefKind::ArmUnderwaterMetalConverter => *self == Self::UnderwaterMetalConverter,
            UnitDefKind::CoreUnderwaterEnergyConverter
            | UnitDefKind::ArmUnderwaterEnergyConverter => *self == Self::UnderwaterEnergyConverter,
            UnitDefKind::CoreAdvancedSonar | UnitDefKind::ArmAdvancedSonar => {
                *self == Self::AdvancedSonar
            }
            UnitDefKind::CoreNavalAdvancedRadarTargeting
            | UnitDefKind::ArmNavalAdvancedRadarTargeting => {
                *self == Self::NavalAdvancedRadarTargeting
            }
            UnitDefKind::CoreAdvancedTorpedoLauncher | UnitDefKind::ArmAdvancedTorpedoLauncher => {
                *self == Self::AdvancedTorpedoLauncher
            }
            UnitDefKind::CoreNavalAntiAirGun | UnitDefKind::ArmNavalAntiAirGun => {
                *self == Self::NavalAntiAirGun
            }
            UnitDefKind::CoreFloatingMultiweaponPlatform
            | UnitDefKind::ArmFloatingMultiweaponPlatform => {
                *self == Self::FloatingMultiweaponPlatform
            }
            UnitDefKind::CoreSeaplaneConstructor | UnitDefKind::ArmSeaplaneConstructor => {
                *self == Self::SeaplaneConstructor
            }
            UnitDefKind::CoreSeaplaneGunship | UnitDefKind::ArmSeaplaneGunship => {
                *self == Self::SeaplaneGunship
            }
            UnitDefKind::CoreSeaplaneBomber | UnitDefKind::ArmSeaplaneBomber => {
                *self == Self::SeaplaneBomber
            }
            UnitDefKind::CoreSeaplaneTorpedoGunship | UnitDefKind::ArmSeaplaneTorpedoGunship => {
                *self == Self::SeaplaneTorpedoGunship
            }
            UnitDefKind::CoreSeaplaneFighter | UnitDefKind::ArmSeaplaneFighter => {
                *self == Self::SeaplaneFighter
            }
            UnitDefKind::CoreSeaplaneRadarSonar | UnitDefKind::ArmSeaplaneRadarSonar => {
                *self == Self::SeaplaneRadarSonar
            }
            UnitDefKind::CoreFlyingFortress
            | UnitDefKind::ArmEMPBomber
            | UnitDefKind::ArmAtomicBomber
            | UnitDefKind::ArmRapidAssaultGunship
            | UnitDefKind::ArmMediumRangeAntiAir
            | UnitDefKind::ArmCloakedFusionReactor
            | UnitDefKind::ArmTachyonAccelerator
            | UnitDefKind::ArmEMPMissileLauncher
            | UnitDefKind::ArmDecoyFusionReactor
            | UnitDefKind::Unknown
            | UnitDefKind::UnknownCurrent
            | UnitDefKind::ArmAssaultCorvette
            | UnitDefKind::ArmT2SupportShip => *self == Self::Unknown,
        }
    }
}
