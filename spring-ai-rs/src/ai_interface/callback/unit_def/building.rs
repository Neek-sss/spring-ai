use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::{ai_interface::callback::unit_def::UnitDef, get_callback};

const MAX_BUILD_OPTIONS: usize = 128;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitBuilding {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitBuildingAll {
    pub builder: bool,
    pub is_build_range_3d: bool,
    pub build_distance: f32,
    pub build_speed: f32,
    pub reclaim_speed: f32,
    pub repair_speed: f32,
    pub max_repair_speed: f32,
    pub resurrect_speed: f32,
    pub capture_speed: f32,
    pub up_direction_smoothing: f32,
    pub terraform_speed: f32,
    pub able_to_restore: bool,
    pub able_to_repair: bool,
    pub able_to_self_repair: bool,
    pub able_to_reclaim: bool,
    pub assistable: bool,
    pub able_to_repeat: bool,
    pub able_to_capture: bool,
    pub able_to_resurrect: bool,
    pub build_options: Vec<UnitDef>,
}

impl UnitBuilding {
    pub fn builder(&self) -> Result<bool, Box<dyn Error>> {
        let builder_func = get_callback!(self.ai_id, UnitDef_isBuilder)?;
        Ok(unsafe { builder_func(self.ai_id, self.def_id) })
    }

    pub fn is_build_range_3d(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_build_range_3d_func = get_callback!(self.ai_id, UnitDef_isBuildRange3D)?;
        Ok(unsafe { get_is_build_range_3d_func(self.ai_id, self.def_id) })
    }

    pub fn build_distance(&self) -> Result<f32, Box<dyn Error>> {
        let get_build_distance_func = get_callback!(self.ai_id, UnitDef_getBuildDistance)?;
        Ok(unsafe { get_build_distance_func(self.ai_id, self.def_id) })
    }

    pub fn build_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_build_speed_func = get_callback!(self.ai_id, UnitDef_getBuildSpeed)?;
        Ok(unsafe { get_build_speed_func(self.ai_id, self.def_id) })
    }

    pub fn reclaim_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_reclaim_speed_func = get_callback!(self.ai_id, UnitDef_getReclaimSpeed)?;
        Ok(unsafe { get_reclaim_speed_func(self.ai_id, self.def_id) })
    }

    pub fn repair_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_repair_speed_func = get_callback!(self.ai_id, UnitDef_getRepairSpeed)?;
        Ok(unsafe { get_repair_speed_func(self.ai_id, self.def_id) })
    }

    pub fn max_repair_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_repair_speed_func = get_callback!(self.ai_id, UnitDef_getMaxRepairSpeed)?;
        Ok(unsafe { get_max_repair_speed_func(self.ai_id, self.def_id) })
    }

    pub fn resurrect_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_ressurect_speed_func = get_callback!(self.ai_id, UnitDef_getResurrectSpeed)?;
        Ok(unsafe { get_ressurect_speed_func(self.ai_id, self.def_id) })
    }

    pub fn capture_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_capture_speed_func = get_callback!(self.ai_id, UnitDef_getCaptureSpeed)?;
        Ok(unsafe { get_capture_speed_func(self.ai_id, self.def_id) })
    }

    pub fn up_direction_smoothing(&self) -> Result<f32, Box<dyn Error>> {
        let get_up_dir_smoothing = get_callback!(self.ai_id, UnitDef_getUpDirSmoothing)?;
        Ok(unsafe { get_up_dir_smoothing(self.ai_id, self.def_id) })
    }

    pub fn terraform_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_terraform_speed_func = get_callback!(self.ai_id, UnitDef_getTerraformSpeed)?;
        Ok(unsafe { get_terraform_speed_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_restore(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_restore_func = get_callback!(self.ai_id, UnitDef_isAbleToRestore)?;
        Ok(unsafe { able_to_restore_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_repair(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_repair_func = get_callback!(self.ai_id, UnitDef_isAbleToRepair)?;
        Ok(unsafe { able_to_repair_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_self_repair(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_self_repair_func = get_callback!(self.ai_id, UnitDef_isAbleToSelfRepair)?;
        Ok(unsafe { able_to_self_repair_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_reclaim(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_reclaim_func = get_callback!(self.ai_id, UnitDef_isAbleToReclaim)?;
        Ok(unsafe { able_to_reclaim_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_assist(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_assist_func = get_callback!(self.ai_id, UnitDef_isAbleToAssist)?;
        Ok(unsafe { able_to_assist_func(self.ai_id, self.def_id) })
    }

    pub fn assistable(&self) -> Result<bool, Box<dyn Error>> {
        let assistable_func = get_callback!(self.ai_id, UnitDef_isAssistable)?;
        Ok(unsafe { assistable_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_repeat(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_repeat_func = get_callback!(self.ai_id, UnitDef_isAbleToRepeat)?;
        Ok(unsafe { able_to_repeat_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_capture(&self) -> Result<bool, Box<dyn Error>> {
        let get_able_to_capture_func = get_callback!(self.ai_id, UnitDef_isAbleToCapture)?;
        Ok(unsafe { get_able_to_capture_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_resurrect(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_able_to_resurrect_func = get_callback!(self.ai_id, UnitDef_isAbleToResurrect)?;
        Ok(unsafe { get_is_able_to_resurrect_func(self.ai_id, self.def_id) })
    }

    pub fn build_options(&self) -> Result<Vec<UnitDef>, Box<dyn Error>> {
        let get_build_options_func = get_callback!(self.ai_id, UnitDef_getBuildOptions)?;

        let mut ret = [-1_i32; MAX_BUILD_OPTIONS];
        unsafe {
            get_build_options_func(
                self.ai_id,
                self.def_id,
                ret.as_mut_ptr(),
                MAX_BUILD_OPTIONS as i32,
            )
        };

        Ok(ret
            .iter()
            .filter_map(|&def_id| {
                if def_id == -1 {
                    None
                } else {
                    Some(UnitDef {
                        ai_id: self.ai_id,
                        def_id,
                    })
                }
            })
            .collect())
    }

    pub fn all(&self) -> Result<UnitBuildingAll, Box<dyn Error>> {
        Ok(UnitBuildingAll {
            builder: self.builder()?,
            is_build_range_3d: self.is_build_range_3d()?,
            build_distance: self.build_distance()?,
            build_speed: self.build_speed()?,
            reclaim_speed: self.reclaim_speed()?,
            repair_speed: self.repair_speed()?,
            max_repair_speed: self.max_repair_speed()?,
            resurrect_speed: self.resurrect_speed()?,
            capture_speed: self.capture_speed()?,
            terraform_speed: self.terraform_speed()?,
            up_direction_smoothing: self.up_direction_smoothing()?,
            able_to_restore: self.able_to_restore()?,
            able_to_repair: self.able_to_repair()?,
            able_to_self_repair: self.able_to_self_repair()?,
            able_to_reclaim: self.able_to_reclaim()?,
            assistable: self.assistable()?,
            able_to_repeat: self.able_to_repeat()?,
            able_to_capture: self.able_to_capture()?,
            able_to_resurrect: self.able_to_resurrect()?,
            build_options: self.build_options()?,
        })
    }
}
