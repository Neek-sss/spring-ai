use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitMovement {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum MoveType {
    Ground,
    Hover,
    Ship,
}

impl From<i32> for MoveType {
    fn from(i: i32) -> Self {
        match i {
            1 => MoveType::Hover,
            2 => MoveType::Ship,
            _ => MoveType::Ground,
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum SpeedModClass {
    Tank,
    KBot,
    Hover,
    Ship,
}

impl From<i32> for SpeedModClass {
    fn from(i: i32) -> Self {
        match i {
            1 => SpeedModClass::KBot,
            2 => SpeedModClass::Hover,
            3 => SpeedModClass::Ship,
            _ => SpeedModClass::Tank,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitMovementAll {
    pub speed: f32,
    pub turn_rate: f32,
    pub can_turn_in_place: bool,
    pub turn_in_place_distance: f32,
    pub turn_in_place_speed_limit: f32,
    pub able_to_submerge: bool,
    pub able_to_fly: bool,
    pub able_to_move: bool,
    pub able_to_hover: bool,
    pub floater: bool,
    // TODO:
    pub move_state: i32,
    //    pub x_size: i32,
    //    pub z_size: i32,
    //    pub depth: f32,
    //    pub max_slope: f32,
    //    pub slope_mod: f32,
    //    pub path_type: i32,
    //    pub crush_strength: f32,
    //    pub move_type: MoveType,
    //    pub speed_mod_class: SpeedModClass,
    //    pub terrain_class: i32,
    //    pub follow_ground: bool,
    //    pub sub_marine: bool,
}

impl UnitMovement {
    pub fn speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_speed_func = get_callback!(self.ai_id, UnitDef_getSpeed)?;
        Ok(unsafe { get_speed_func(self.ai_id, self.def_id) })
    }

    pub fn turn_rate(&self) -> Result<f32, Box<dyn Error>> {
        let get_turn_rate_func = get_callback!(self.ai_id, UnitDef_getTurnRate)?;
        Ok(unsafe { get_turn_rate_func(self.ai_id, self.def_id) })
    }

    pub fn can_turn_in_place(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_turn_in_place_func = get_callback!(self.ai_id, UnitDef_isTurnInPlace)?;
        Ok(unsafe { get_is_turn_in_place_func(self.ai_id, self.def_id) })
    }

    pub fn turn_in_place_distance(&self) -> Result<f32, Box<dyn Error>> {
        let get_turn_in_place_distance_func =
            get_callback!(self.ai_id, UnitDef_getTurnInPlaceDistance)?;
        Ok(unsafe { get_turn_in_place_distance_func(self.ai_id, self.def_id) })
    }

    pub fn turn_in_place_speed_limit(&self) -> Result<f32, Box<dyn Error>> {
        let get_turn_in_place_speed_limit_func =
            get_callback!(self.ai_id, UnitDef_getTurnInPlaceSpeedLimit)?;
        Ok(unsafe { get_turn_in_place_speed_limit_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_submerge(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_submerge_func = get_callback!(self.ai_id, UnitDef_isAbleToSubmerge)?;
        Ok(unsafe { able_to_submerge_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_fly(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_fly_func = get_callback!(self.ai_id, UnitDef_isAbleToFly)?;
        Ok(unsafe { able_to_fly_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_move(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_move_func = get_callback!(self.ai_id, UnitDef_isAbleToMove)?;
        Ok(unsafe { able_to_move_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_hover(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_hover_func = get_callback!(self.ai_id, UnitDef_isAbleToHover)?;
        Ok(unsafe { able_to_hover_func(self.ai_id, self.def_id) })
    }

    pub fn floater(&self) -> Result<bool, Box<dyn Error>> {
        let floater_func = get_callback!(self.ai_id, UnitDef_isFloater)?;
        Ok(unsafe { floater_func(self.ai_id, self.def_id) })
    }

    pub fn move_state(&self) -> Result<i32, Box<dyn Error>> {
        let get_move_state_func = get_callback!(self.ai_id, UnitDef_getMoveState)?;
        Ok(unsafe { get_move_state_func(self.ai_id, self.def_id) })
    }

    //    pub fn x_size(&self) -> Result<i32, Box<dyn Error>> {
    //        let get_x_size_func = get_callback!(self.ai_id, UnitDef_MoveData_getXSize)?;
    //        Ok(unsafe { get_x_size_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn z_size(&self) -> Result<i32, Box<dyn Error>> {
    //        let get_z_size_func = get_callback!(self.ai_id, UnitDef_MoveData_getZSize)?;
    //        Ok(unsafe { get_z_size_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn depth(&self) -> Result<f32, Box<dyn Error>> {
    //        let get_depth_func = get_callback!(self.ai_id, UnitDef_MoveData_getDepth)?;
    //        Ok(unsafe { get_depth_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn max_slope(&self) -> Result<f32, Box<dyn Error>> {
    //        let get_max_slope_func = get_callback!(self.ai_id, UnitDef_MoveData_getMaxSlope)?;
    //        Ok(unsafe { get_max_slope_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn slope_mod(&self) -> Result<f32, Box<dyn Error>> {
    //        let get_slope_mod_func = get_callback!(self.ai_id, UnitDef_MoveData_getSlopeMod)?;
    //        Ok(unsafe { get_slope_mod_func(self.ai_id, self.def_id) })
    //    }

    //    pub fn depth_mod(&self, height: f32) -> Result<f32, Box<dyn Error>> {
    //        let get_depth_mod_func = get_callback!(self.ai_id, UnitDef_MoveData_getDepthMod)?;
    //        Ok(unsafe { get_depth_mod_func(self.ai_id, self.def_id, height) })
    //    }

    //    pub fn path_type(&self) -> Result<i32, Box<dyn Error>> {
    //        let get_path_type_func = get_callback!(self.ai_id, UnitDef_MoveData_getPathType)?;
    //        Ok(unsafe { get_path_type_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn crush_strength(&self) -> Result<f32, Box<dyn Error>> {
    //        let get_crush_strength_func = get_callback!(self.ai_id, UnitDef_MoveData_getCrushStrength)?;
    //        Ok(unsafe { get_crush_strength_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn move_type(&self) -> Result<MoveType, Box<dyn Error>> {
    //        let get_move_type_func = get_callback!(self.ai_id, UnitDef_MoveData_getMoveType)?;
    //        Ok(MoveType::from(unsafe {
    //            get_move_type_func(self.ai_id, self.def_id)
    //        }))
    //    }
    //
    //    pub fn speed_mod_class(&self) -> Result<SpeedModClass, Box<dyn Error>> {
    //        let get_speed_mod_class_func =
    //            get_callback!(self.ai_id, UnitDef_MoveData_getSpeedModClass)?;
    //        Ok(SpeedModClass::from(unsafe {
    //            get_speed_mod_class_func(self.ai_id, self.def_id)
    //        }))
    //    }
    //
    //    pub fn terrain_class(&self) -> Result<i32, Box<dyn Error>> {
    //        let get_terrain_class_func = get_callback!(self.ai_id, UnitDef_MoveData_getTerrainClass)?;
    //        Ok(unsafe { get_terrain_class_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn follow_ground(&self) -> Result<bool, Box<dyn Error>> {
    //        let get_follow_ground_func = get_callback!(self.ai_id, UnitDef_MoveData_getFollowGround)?;
    //        Ok(unsafe { get_follow_ground_func(self.ai_id, self.def_id) })
    //    }
    //
    //    pub fn sub_marine(&self) -> Result<bool, Box<dyn Error>> {
    //        let get_sub_marine_func = get_callback!(self.ai_id, UnitDef_MoveData_isSubMarine)?;
    //        Ok(unsafe { get_sub_marine_func(self.ai_id, self.def_id) })
    //    }

    //    pub fn name(&self) -> Result<(), Box<dyn Error>> {
    //        let get_name_func = get_callback!(self.ai_id, UnitDef_MoveData_getName)?;
    //        Ok(unsafe { get_name_func(self.ai_id, self.def_id) })
    //    }

    pub fn all(&self) -> Result<UnitMovementAll, Box<dyn Error>> {
        Ok(UnitMovementAll {
            speed: self.speed()?,
            turn_rate: self.turn_rate()?,
            can_turn_in_place: self.can_turn_in_place()?,
            turn_in_place_distance: self.turn_in_place_distance()?,
            turn_in_place_speed_limit: self.turn_in_place_speed_limit()?,
            able_to_submerge: self.able_to_submerge()?,
            able_to_fly: self.able_to_fly()?,
            able_to_move: self.able_to_move()?,
            able_to_hover: self.able_to_hover()?,
            floater: self.floater()?,
            move_state: self.move_state()?,
            //            x_size: self.x_size()?,
            //            z_size: self.z_size()?,
            //            depth: self.depth()?,
            //            max_slope: self.max_slope()?,
            //            slope_mod: self.slope_mod()?,
            //            path_type: self.path_type()?,
            //            crush_strength: self.crush_strength()?,
            //            move_type: self.move_type()?,
            //            speed_mod_class: self.speed_mod_class()?,
            //            terrain_class: self.terrain_class()?,
            //            follow_ground: self.follow_ground()?,
            //            sub_marine: self.sub_marine()?,
        })
    }
}
