use std::{collections::HashMap, error::Error};

use serde::{Deserialize, Serialize};

use crate::{
    ai_interface::{callback::resource::Resource, AIInterface},
    get_callback,
};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitConstruction {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitConstructionAll {
    pub resource: HashMap<Resource, f32>,
    pub build_time: f32,
    pub upkeep: HashMap<Resource, f32>,
    pub max_height_difference: f32,
    pub min_water_depth: f32,
    pub max_water_depth: f32,
}

impl UnitConstruction {
    pub fn resource(&self, resource: Resource) -> Result<f32, Box<dyn Error>> {
        let get_cost_func = get_callback!(self.ai_id, UnitDef_getCost)?;
        Ok(unsafe { get_cost_func(self.ai_id, self.def_id, resource.resource_id) })
    }

    pub fn build_time(&self) -> Result<f32, Box<dyn Error>> {
        let get_build_time_func = get_callback!(self.ai_id, UnitDef_getBuildTime)?;
        Ok(unsafe { get_build_time_func(self.ai_id, self.def_id) })
    }

    pub fn upkeep(&self, resource: Resource) -> Result<f32, Box<dyn Error>> {
        let get_upkeep_func = get_callback!(self.ai_id, UnitDef_getUpkeep)?;
        Ok(unsafe { get_upkeep_func(self.ai_id, self.def_id, resource.resource_id) })
    }

    pub fn max_height_difference(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_height_dif_func = get_callback!(self.ai_id, UnitDef_getMaxHeightDif)?;
        Ok(unsafe { get_max_height_dif_func(self.ai_id, self.def_id) })
    }

    pub fn min_water_depth(&self) -> Result<f32, Box<dyn Error>> {
        let get_min_water_depth_func = get_callback!(self.ai_id, UnitDef_getMinWaterDepth)?;
        Ok(unsafe { get_min_water_depth_func(self.ai_id, self.def_id) })
    }

    pub fn max_water_depth(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_water_depth_func = get_callback!(self.ai_id, UnitDef_getMaxWaterDepth)?;
        Ok(unsafe { get_max_water_depth_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitConstructionAll, Box<dyn Error>> {
        Ok(UnitConstructionAll {
            resource: AIInterface::new(self.ai_id)
                .resource_interface()
                .get_resources()?
                .into_iter()
                .filter_map(|resource| {
                    if let Ok(res) = self.resource(resource) {
                        Some((resource, res))
                    } else {
                        None
                    }
                })
                .collect(),
            build_time: self.build_time()?,
            upkeep: AIInterface::new(self.ai_id)
                .resource_interface()
                .get_resources()?
                .into_iter()
                .filter_map(|resource| {
                    if let Ok(upk) = self.upkeep(resource) {
                        Some((resource, upk))
                    } else {
                        None
                    }
                })
                .collect(),
            max_height_difference: self.max_height_difference()?,
            min_water_depth: self.min_water_depth()?,
            max_water_depth: self.min_water_depth()?,
        })
    }
}
