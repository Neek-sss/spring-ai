use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitDetection {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitDetectionAll {
    pub jammar_radius: i32,
    pub sonar_jam_radius: i32,
    pub seismic_radius: i32,
    pub seismic_signature: f32,
    pub is_stealth: bool,
    pub is_sonar_stealth: bool,
}

impl UnitDetection {
    pub fn jammer_radius(&self) -> Result<i32, Box<dyn Error>> {
        let get_jammer_radius_func = get_callback!(self.ai_id, UnitDef_getJammerRadius)?;
        Ok(unsafe { get_jammer_radius_func(self.ai_id, self.def_id) })
    }

    pub fn sonar_jam_radius(&self) -> Result<i32, Box<dyn Error>> {
        let get_sonar_jam_radius_func = get_callback!(self.ai_id, UnitDef_getSonarJamRadius)?;
        Ok(unsafe { get_sonar_jam_radius_func(self.ai_id, self.def_id) })
    }

    pub fn seismic_radius(&self) -> Result<i32, Box<dyn Error>> {
        let get_seismic_radius_func = get_callback!(self.ai_id, UnitDef_getSeismicRadius)?;
        Ok(unsafe { get_seismic_radius_func(self.ai_id, self.def_id) })
    }

    pub fn seismic_signature(&self) -> Result<f32, Box<dyn Error>> {
        let get_seismic_signature_func = get_callback!(self.ai_id, UnitDef_getSeismicSignature)?;
        Ok(unsafe { get_seismic_signature_func(self.ai_id, self.def_id) })
    }

    pub fn is_stealth(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_stealth_func = get_callback!(self.ai_id, UnitDef_isStealth)?;
        Ok(unsafe { get_is_stealth_func(self.ai_id, self.def_id) })
    }

    pub fn is_sonar_stealth(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_sonar_stealth_func = get_callback!(self.ai_id, UnitDef_isSonarStealth)?;
        Ok(unsafe { get_is_sonar_stealth_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitDetectionAll, Box<dyn Error>> {
        Ok(UnitDetectionAll {
            jammar_radius: self.jammer_radius()?,
            sonar_jam_radius: self.sonar_jam_radius()?,
            seismic_radius: self.seismic_radius()?,
            seismic_signature: self.seismic_signature()?,
            is_stealth: self.is_stealth()?,
            is_sonar_stealth: self.is_sonar_stealth()?,
        })
    }
}
