use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitAutoHeal {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitAutoHealAll {
    pub normal: f32,
    pub idle: f32,
}

impl UnitAutoHeal {
    pub fn normal(&self) -> Result<f32, Box<dyn Error>> {
        let get_auto_heal_func = get_callback!(self.ai_id, UnitDef_getAutoHeal)?;
        Ok(unsafe { get_auto_heal_func(self.ai_id, self.def_id) })
    }

    pub fn idle(&self) -> Result<f32, Box<dyn Error>> {
        let get_get_idle_auto_heal_func = get_callback!(self.ai_id, UnitDef_getIdleAutoHeal)?;
        Ok(unsafe { get_get_idle_auto_heal_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitAutoHealAll, Box<dyn Error>> {
        Ok(UnitAutoHealAll {
            normal: self.normal()?,
            idle: self.idle()?,
        })
    }
}
