use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitFlanking {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum FlankingMode {
    NoBonus,
    GlobalCoordsMobile,
    UnitCoordsMobile,
    UnitCoordsLocked,
}

impl From<i32> for FlankingMode {
    fn from(i: i32) -> Self {
        match i {
            1 => FlankingMode::GlobalCoordsMobile,
            2 => FlankingMode::UnitCoordsMobile,
            3 => FlankingMode::UnitCoordsLocked,
            _ => FlankingMode::NoBonus,
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitFlankingAll {
    pub mode: FlankingMode,
    pub direction: [f32; 3],
    pub max: f32,
    pub min: f32,
    pub mobility_add: f32,
}

impl UnitFlanking {
    pub fn mode(&self) -> Result<FlankingMode, Box<dyn Error>> {
        let get_flanking_bonus_mode_func =
            get_callback!(self.ai_id, UnitDef_FlankingBonus_getMode)?;
        Ok(FlankingMode::from(unsafe {
            get_flanking_bonus_mode_func(self.ai_id, self.def_id)
        }))
    }

    pub fn direction(&self) -> Result<[f32; 3], Box<dyn Error>> {
        let get_flanking_bonus_dir_func = get_callback!(self.ai_id, UnitDef_FlankingBonus_getDir)?;

        let mut temp = [0.0_f32; 3];
        unsafe { get_flanking_bonus_dir_func(self.ai_id, self.def_id, temp.as_mut_ptr()) };

        Ok(temp)
    }

    pub fn max(&self) -> Result<f32, Box<dyn Error>> {
        let get_flanking_bonus_max_func = get_callback!(self.ai_id, UnitDef_FlankingBonus_getMax)?;
        Ok(unsafe { get_flanking_bonus_max_func(self.ai_id, self.def_id) })
    }

    pub fn min(&self) -> Result<f32, Box<dyn Error>> {
        let get_flanking_bonus_min_func = get_callback!(self.ai_id, UnitDef_FlankingBonus_getMin)?;
        Ok(unsafe { get_flanking_bonus_min_func(self.ai_id, self.def_id) })
    }

    pub fn mobility_add(&self) -> Result<f32, Box<dyn Error>> {
        let get_flanking_bonus_mobility_add_func =
            get_callback!(self.ai_id, UnitDef_FlankingBonus_getMobilityAdd)?;
        Ok(unsafe { get_flanking_bonus_mobility_add_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitFlankingAll, Box<dyn Error>> {
        Ok(UnitFlankingAll {
            mode: self.mode()?,
            direction: self.direction()?,
            max: self.max()?,
            min: self.min()?,
            mobility_add: self.mobility_add()?,
        })
    }
}
