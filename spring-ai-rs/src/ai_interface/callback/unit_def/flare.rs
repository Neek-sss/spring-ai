use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitFlare {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitFlareAll {
    pub able_to_drop: bool,
    pub reload_time: f32,
    pub efficiency: f32,
    pub delay: f32,
    pub drop_vector: [f32; 3],
    pub time: i32,
    pub salvo_size: i32,
    pub salvo_delay: i32,
}

impl UnitFlare {
    pub fn able_to_drop(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_able_to_drop_flare_func = get_callback!(self.ai_id, UnitDef_isAbleToDropFlare)?;
        Ok(unsafe { get_is_able_to_drop_flare_func(self.ai_id, self.def_id) })
    }

    pub fn reload_time(&self) -> Result<f32, Box<dyn Error>> {
        let get_flare_reload_time_func = get_callback!(self.ai_id, UnitDef_getFlareReloadTime)?;
        Ok(unsafe { get_flare_reload_time_func(self.ai_id, self.def_id) })
    }

    pub fn efficiency(&self) -> Result<f32, Box<dyn Error>> {
        let get_flare_efficiency_func = get_callback!(self.ai_id, UnitDef_getFlareEfficiency)?;
        Ok(unsafe { get_flare_efficiency_func(self.ai_id, self.def_id) })
    }

    pub fn delay(&self) -> Result<f32, Box<dyn Error>> {
        let get_flare_delay_func = get_callback!(self.ai_id, UnitDef_getFlareDelay)?;
        Ok(unsafe { get_flare_delay_func(self.ai_id, self.def_id) })
    }

    pub fn drop_vector(&self) -> Result<[f32; 3], Box<dyn Error>> {
        let get_flare_drop_vector_func = get_callback!(self.ai_id, UnitDef_getFlareDropVector)?;

        let mut ret = [0.0; 3];
        unsafe { get_flare_drop_vector_func(self.ai_id, self.def_id, ret.as_mut_ptr()) };

        Ok(ret)
    }

    pub fn time(&self) -> Result<i32, Box<dyn Error>> {
        let get_flare_time_func = get_callback!(self.ai_id, UnitDef_getFlareTime)?;
        Ok(unsafe { get_flare_time_func(self.ai_id, self.def_id) })
    }

    pub fn salvo_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_flare_salvo_size_func = get_callback!(self.ai_id, UnitDef_getFlareSalvoSize)?;
        Ok(unsafe { get_flare_salvo_size_func(self.ai_id, self.def_id) })
    }

    pub fn salvo_delay(&self) -> Result<i32, Box<dyn Error>> {
        let get_flare_salvo_delay_func = get_callback!(self.ai_id, UnitDef_getFlareSalvoDelay)?;
        Ok(unsafe { get_flare_salvo_delay_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitFlareAll, Box<dyn Error>> {
        Ok(UnitFlareAll {
            able_to_drop: self.able_to_drop()?,
            reload_time: self.reload_time()?,
            efficiency: self.efficiency()?,
            delay: self.delay()?,
            drop_vector: self.drop_vector()?,
            time: self.time()?,
            salvo_size: self.salvo_size()?,
            salvo_delay: self.salvo_delay()?,
        })
    }
}
