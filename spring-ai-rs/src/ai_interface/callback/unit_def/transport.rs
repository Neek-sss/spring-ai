use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitTransport {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum TransportMethod {
    LandUnload,
    FlyOverDrop,
    LandFlood,
}

impl From<i32> for TransportMethod {
    fn from(i: i32) -> Self {
        match i {
            0 => TransportMethod::LandUnload,
            1 => TransportMethod::FlyOverDrop,
            _ => TransportMethod::LandFlood,
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitTransportAll {
    pub loading_radius: f32,
    pub unload_spread: f32,
    pub transport_capacity: i32,
    pub transport_size: i32,
    pub min_transport_size: i32,
    pub air_base: bool,
    pub fire_platform: bool,
    pub transport_mass: f32,
    pub min_transport_mass: f32,
    pub hold_steady: bool,
    pub release_held: bool,
    pub not_transportable: bool,
    pub transport_by_enemy: bool,
    pub transport_unload_method: TransportMethod,
    pub fall_speed: f32,
    pub unit_fall_speed: f32,
}

impl UnitTransport {
    pub fn loading_radius(&self) -> Result<f32, Box<dyn Error>> {
        let get_loading_radius_func = get_callback!(self.ai_id, UnitDef_getLoadingRadius)?;
        Ok(unsafe { get_loading_radius_func(self.ai_id, self.def_id) })
    }

    pub fn unload_spread(&self) -> Result<f32, Box<dyn Error>> {
        let get_unload_spread_func = get_callback!(self.ai_id, UnitDef_getUnloadSpread)?;
        Ok(unsafe { get_unload_spread_func(self.ai_id, self.def_id) })
    }

    pub fn transport_capacity(&self) -> Result<i32, Box<dyn Error>> {
        let get_transport_capacity_func = get_callback!(self.ai_id, UnitDef_getTransportCapacity)?;
        Ok(unsafe { get_transport_capacity_func(self.ai_id, self.def_id) })
    }

    pub fn transport_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_transport_size_func = get_callback!(self.ai_id, UnitDef_getTransportSize)?;
        Ok(unsafe { get_transport_size_func(self.ai_id, self.def_id) })
    }

    pub fn min_transport_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_min_transport_size_func = get_callback!(self.ai_id, UnitDef_getMinTransportSize)?;
        Ok(unsafe { get_min_transport_size_func(self.ai_id, self.def_id) })
    }

    pub fn air_base(&self) -> Result<bool, Box<dyn Error>> {
        let get_air_base_func = get_callback!(self.ai_id, UnitDef_isAirBase)?;
        Ok(unsafe { get_air_base_func(self.ai_id, self.def_id) })
    }

    pub fn fire_platform(&self) -> Result<bool, Box<dyn Error>> {
        let get_fire_platform_func = get_callback!(self.ai_id, UnitDef_isFirePlatform)?;
        Ok(unsafe { get_fire_platform_func(self.ai_id, self.def_id) })
    }

    pub fn transport_mass(&self) -> Result<f32, Box<dyn Error>> {
        let get_transport_mass_func = get_callback!(self.ai_id, UnitDef_getTransportMass)?;
        Ok(unsafe { get_transport_mass_func(self.ai_id, self.def_id) })
    }

    pub fn min_transport_mass(&self) -> Result<f32, Box<dyn Error>> {
        let get_min_transport_mass_func = get_callback!(self.ai_id, UnitDef_getMinTransportMass)?;
        Ok(unsafe { get_min_transport_mass_func(self.ai_id, self.def_id) })
    }

    pub fn hold_steady(&self) -> Result<bool, Box<dyn Error>> {
        let get_hold_steady_func = get_callback!(self.ai_id, UnitDef_isHoldSteady)?;
        Ok(unsafe { get_hold_steady_func(self.ai_id, self.def_id) })
    }

    pub fn release_held(&self) -> Result<bool, Box<dyn Error>> {
        let get_release_held_func = get_callback!(self.ai_id, UnitDef_isReleaseHeld)?;
        Ok(unsafe { get_release_held_func(self.ai_id, self.def_id) })
    }

    pub fn not_transportable(&self) -> Result<bool, Box<dyn Error>> {
        let get_not_transportable_func = get_callback!(self.ai_id, UnitDef_isNotTransportable)?;
        Ok(unsafe { get_not_transportable_func(self.ai_id, self.def_id) })
    }

    pub fn transport_by_enemy(&self) -> Result<bool, Box<dyn Error>> {
        let get_transport_by_enemy_func = get_callback!(self.ai_id, UnitDef_isTransportByEnemy)?;
        Ok(unsafe { get_transport_by_enemy_func(self.ai_id, self.def_id) })
    }

    pub fn transport_unload_method(&self) -> Result<TransportMethod, Box<dyn Error>> {
        let get_transport_unload_method_func =
            get_callback!(self.ai_id, UnitDef_getTransportUnloadMethod)?;
        Ok(TransportMethod::from(unsafe {
            get_transport_unload_method_func(self.ai_id, self.def_id)
        }))
    }

    pub fn fall_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_fall_speed_func = get_callback!(self.ai_id, UnitDef_getFallSpeed)?;
        Ok(unsafe { get_fall_speed_func(self.ai_id, self.def_id) })
    }

    pub fn unit_fall_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_unit_fall_speed_func = get_callback!(self.ai_id, UnitDef_getUnitFallSpeed)?;
        Ok(unsafe { get_unit_fall_speed_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitTransportAll, Box<dyn Error>> {
        Ok(UnitTransportAll {
            loading_radius: self.loading_radius()?,
            unload_spread: self.unload_spread()?,
            transport_capacity: self.transport_capacity()?,
            transport_size: self.transport_size()?,
            min_transport_size: self.min_transport_size()?,
            air_base: self.air_base()?,
            fire_platform: self.fire_platform()?,
            transport_mass: self.transport_mass()?,
            min_transport_mass: self.min_transport_mass()?,
            hold_steady: self.hold_steady()?,
            release_held: self.release_held()?,
            not_transportable: self.not_transportable()?,
            transport_by_enemy: self.transport_by_enemy()?,
            transport_unload_method: self.transport_unload_method()?,
            fall_speed: self.fall_speed()?,
            unit_fall_speed: self.unit_fall_speed()?,
        })
    }
}
