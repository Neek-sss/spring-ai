use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitModel {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitModelAll {
    pub height: f32,
    pub radius: f32,
}

impl UnitModel {
    pub fn height(&self) -> Result<f32, Box<dyn Error>> {
        let get_model_height = get_callback!(self.ai_id, UnitDef_getHeight)?;
        Ok(unsafe { get_model_height(self.ai_id, self.def_id) })
    }

    pub fn radius(&self) -> Result<f32, Box<dyn Error>> {
        let get_model_radius = get_callback!(self.ai_id, UnitDef_getRadius)?;
        Ok(unsafe { get_model_radius(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitModelAll, Box<dyn Error>> {
        Ok(UnitModelAll {
            height: self.height()?,
            radius: self.radius()?,
        })
    }
}
