use crate::get_callback;
use std::error::Error;

#[derive(Debug, Copy, Clone)]
pub struct UnitTrack {
    pub ai_id: i32,
    pub def_id: i32,
}

impl UnitTrack {
    pub fn leave_tracks(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_leave_tracks_func = get_callback!(self.ai_id, UnitDef_isLeaveTracks)?;
        Ok(unsafe { get_is_leave_tracks_func(self.ai_id, self.def_id) })
    }

    pub fn width(&self) -> Result<bool, Box<dyn Error>> {
        let get_track_width_func = get_callback!(self.ai_id, UnitDef_getTrackWidth)?;
        Ok(unsafe { get_track_width_func(self.ai_id, self.def_id) })
    }

    pub fn offset(&self) -> Result<bool, Box<dyn Error>> {
        let get_track_offset_func = get_callback!(self.ai_id, UnitDef_getTrackOffset)?;
        Ok(unsafe { get_track_offset_func(self.ai_id, self.def_id) })
    }

    pub fn strength(&self) -> Result<bool, Box<dyn Error>> {
        let get_track_strength_func = get_callback!(self.ai_id, UnitDef_getTrackStrength)?;
        Ok(unsafe { get_track_strength_func(self.ai_id, self.def_id) })
    }

    pub fn stretch(&self) -> Result<bool, Box<dyn Error>> {
        let get_track_stretch_func = get_callback!(self.ai_id, UnitDef_getTrackStretch)?;
        Ok(unsafe { get_track_stretch_func(self.ai_id, self.def_id) })
    }

    pub fn track_type(&self) -> Result<bool, Box<dyn Error>> {
        let get_track_type_func = get_callback!(self.ai_id, UnitDef_getTrackType)?;
        Ok(unsafe { get_track_type_func(self.ai_id, self.def_id) })
    }
}