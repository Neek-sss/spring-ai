use std::{error::Error, ffi::CStr};

use serde::{Deserialize, Serialize};

use crate::{ai_interface::callback::unit_def::UnitDef, get_callback};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitWeaponMount {
    pub ai_id: i32,
    pub def_id: i32,
    mount_id: i32,
    weapon_def_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitWeaponMountAll {
    pub mount_name: String,
    pub slaved_to: UnitWeaponMount,
    pub main_direction: [f32; 3],
    pub max_angle: f32,
    pub target_blacklist: i32,
    pub target_whitelist: i32,
}

impl UnitDef {
    pub fn weapon_mounts(&self) -> Result<Vec<UnitWeaponMount>, Box<dyn Error>> {
        let get_weapon_mounts_func = get_callback!(self.ai_id, UnitDef_getWeaponMounts)?;

        let number_of_mounts = unsafe { get_weapon_mounts_func(self.ai_id, self.def_id) };
        Ok((0..number_of_mounts)
            .filter_map(|mount_id| {
                UnitWeaponMount::get_weapon_def(self.ai_id, self.def_id, mount_id).ok()
            })
            .collect())
    }
}

impl UnitWeaponMount {
    pub fn get_weapon_def(ai_id: i32, def_id: i32, mount_id: i32) -> Result<Self, Box<dyn Error>> {
        let get_weapon_def_func = get_callback!(ai_id, UnitDef_WeaponMount_getWeaponDef)?;

        Ok(Self {
            ai_id,
            def_id,
            mount_id,
            weapon_def_id: unsafe { get_weapon_def_func(ai_id, def_id, mount_id) },
        })
    }

    pub fn mount_name(&self) -> Result<String, Box<dyn Error>> {
        let get_mount_name_func = get_callback!(self.ai_id, UnitDef_WeaponMount_getName)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_mount_name_func(self.ai_id, self.def_id, self.mount_id)) }
                .to_str()?,
        ))
    }

    pub fn slaved_to(&self) -> Result<Self, Box<dyn Error>> {
        let get_slaved_to_func = get_callback!(self.ai_id, UnitDef_WeaponMount_getSlavedTo)?;
        let slave_id = unsafe { get_slaved_to_func(self.ai_id, self.def_id, self.mount_id) };
        Self::get_weapon_def(self.ai_id, self.def_id, slave_id)
    }

    pub fn main_direction(&self) -> Result<[f32; 3], Box<dyn Error>> {
        let get_main_direction_func = get_callback!(self.ai_id, UnitDef_WeaponMount_getMainDir)?;

        let mut ret = [0.0_f32; 3];
        unsafe {
            get_main_direction_func(self.ai_id, self.def_id, self.mount_id, ret.as_mut_ptr())
        };

        Ok(ret)
    }

    pub fn max_angle(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_angle_func = get_callback!(self.ai_id, UnitDef_WeaponMount_getMaxAngleDif)?;
        Ok(unsafe { get_max_angle_func(self.ai_id, self.def_id, self.mount_id) })
    }

    pub fn target_blacklist(&self) -> Result<i32, Box<dyn Error>> {
        let get_target_category_func =
            get_callback!(self.ai_id, UnitDef_WeaponMount_getBadTargetCategory)?;
        Ok(unsafe { get_target_category_func(self.ai_id, self.def_id, self.mount_id) })
    }

    pub fn target_whitelist(&self) -> Result<i32, Box<dyn Error>> {
        let get_only_target_category_func =
            get_callback!(self.ai_id, UnitDef_WeaponMount_getOnlyTargetCategory)?;
        Ok(unsafe { get_only_target_category_func(self.ai_id, self.def_id, self.mount_id) })
    }

    pub fn all(&self) -> Result<UnitWeaponMountAll, Box<dyn Error>> {
        Ok(UnitWeaponMountAll {
            mount_name: self.mount_name()?,
            slaved_to: self.slaved_to()?,
            main_direction: self.main_direction()?,
            max_angle: self.max_angle()?,
            target_blacklist: self.target_blacklist()?,
            target_whitelist: self.target_whitelist()?,
        })
    }
}
