use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitCombat {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitCombatAll {
    pub strafe_to_attack: bool,
    pub max_weapon_range: f32,
    pub armoured_multiple: f32,
    pub armour_type: i32,
    pub able_to_attack: bool,
    pub able_to_patrol: bool,
    pub able_to_fight: bool,
    pub able_to_guard: bool,
    pub able_to_fire_control: bool,
    pub fire_state: i32,
    pub hover_attack: bool,
    pub air_strafe: bool,
    pub targeting_facility: bool,
    pub manual_fire: bool,
    pub able_to_kamikaze: bool,
    pub kamikaze_dist: f32,
}

impl UnitCombat {
    pub fn strafe_to_attack(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_strafe_to_attack_func = get_callback!(self.ai_id, UnitDef_isStrafeToAttack)?;
        Ok(unsafe { get_is_strafe_to_attack_func(self.ai_id, self.def_id) })
    }

    pub fn max_weapon_range(&self) -> Result<f32, Box<dyn Error>> {
        let get_max_weapon_range_func = get_callback!(self.ai_id, UnitDef_getMaxWeaponRange)?;
        Ok(unsafe { get_max_weapon_range_func(self.ai_id, self.def_id) })
    }

    pub fn armoured_multiple(&self) -> Result<f32, Box<dyn Error>> {
        let get_armored_multiple_func = get_callback!(self.ai_id, UnitDef_getArmoredMultiple)?;
        Ok(unsafe { get_armored_multiple_func(self.ai_id, self.def_id) })
    }

    pub fn armour_type(&self) -> Result<i32, Box<dyn Error>> {
        let get_armor_type_func = get_callback!(self.ai_id, UnitDef_getArmorType)?;
        Ok(unsafe { get_armor_type_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_attack(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_attack_func = get_callback!(self.ai_id, UnitDef_isAbleToAttack)?;
        Ok(unsafe { able_to_attack_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_patrol(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_patrol_func = get_callback!(self.ai_id, UnitDef_isAbleToPatrol)?;
        Ok(unsafe { able_to_patrol_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_fight(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_fight_func = get_callback!(self.ai_id, UnitDef_isAbleToFight)?;
        Ok(unsafe { able_to_fight_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_guard(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_guard_func = get_callback!(self.ai_id, UnitDef_isAbleToGuard)?;
        Ok(unsafe { able_to_guard_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_fire_control(&self) -> Result<bool, Box<dyn Error>> {
        let able_to_fire_control_func = get_callback!(self.ai_id, UnitDef_isAbleToFireControl)?;
        Ok(unsafe { able_to_fire_control_func(self.ai_id, self.def_id) })
    }

    pub fn fire_state(&self) -> Result<i32, Box<dyn Error>> {
        let get_fire_state_func = get_callback!(self.ai_id, UnitDef_getFireState)?;
        Ok(unsafe { get_fire_state_func(self.ai_id, self.def_id) })
    }

    pub fn hover_attack(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_able_to_self_d_func = get_callback!(self.ai_id, UnitDef_isHoverAttack)?;
        Ok(unsafe { get_is_able_to_self_d_func(self.ai_id, self.def_id) })
    }

    pub fn air_strafe(&self) -> Result<bool, Box<dyn Error>> {
        let get_air_strafe_func = get_callback!(self.ai_id, UnitDef_isAirStrafe)?;
        Ok(unsafe { get_air_strafe_func(self.ai_id, self.def_id) })
    }

    pub fn targeting_facility(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_targeting_facility_func =
            get_callback!(self.ai_id, UnitDef_isTargetingFacility)?;
        Ok(unsafe { get_is_targeting_facility_func(self.ai_id, self.def_id) })
    }

    pub fn manual_fire(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_manual_fire_func = get_callback!(self.ai_id, UnitDef_canManualFire)?;
        Ok(unsafe { get_is_manual_fire_func(self.ai_id, self.def_id) })
    }

    pub fn able_to_kamikaze(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_able_to_kamikaze_func = get_callback!(self.ai_id, UnitDef_isAbleToKamikaze)?;
        Ok(unsafe { get_is_able_to_kamikaze_func(self.ai_id, self.def_id) })
    }

    pub fn kamikaze_distance(&self) -> Result<f32, Box<dyn Error>> {
        let get_kamikaze_dist_func = get_callback!(self.ai_id, UnitDef_getKamikazeDist)?;
        Ok(unsafe { get_kamikaze_dist_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitCombatAll, Box<dyn Error>> {
        Ok(UnitCombatAll {
            strafe_to_attack: self.strafe_to_attack()?,
            max_weapon_range: self.max_weapon_range()?,
            armoured_multiple: self.armoured_multiple()?,
            armour_type: self.armour_type()?,
            able_to_attack: self.able_to_attack()?,
            able_to_patrol: self.able_to_patrol()?,
            able_to_fight: self.able_to_fight()?,
            able_to_guard: self.able_to_guard()?,
            able_to_fire_control: self.able_to_fire_control()?,
            fire_state: self.fire_state()?,
            hover_attack: self.hover_attack()?,
            air_strafe: self.air_strafe()?,
            targeting_facility: self.targeting_facility()?,
            manual_fire: self.manual_fire()?,
            able_to_kamikaze: self.able_to_kamikaze()?,
            kamikaze_dist: self.kamikaze_distance()?,
        })
    }
}
