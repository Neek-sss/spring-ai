use std::{error::Error, ffi::CStr};

use serde::{Deserialize, Serialize};

use crate::get_callback;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct UnitMiscellaneous {
    pub ai_id: i32,
    pub def_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnitMiscellaneousAll {
    pub idle_time: i32,
    pub power: f32,
    pub health: f32,
    pub category_id: i32,
    pub category_string: String,
    pub mass: f32,
    pub upright: bool,
    pub collide: bool,
    pub push_resistant: bool,
    pub min_collision_speed: f32,
    pub slide_tolerance: f32,
    pub waterline: f32,
    pub tooltip: String,
    pub active_when_built: bool,
    pub on_offable: bool,
    pub full_health_factory: bool,
    pub factory_heading_take_off: bool,
    pub reclaimable: bool,
    pub auto_reclaimable: bool,
    pub capturable: bool,
    pub x_size: i32,
    pub z_size: i32,
    pub max_this_unit: i32,
}

impl UnitMiscellaneous {
    pub fn idle_time(&self) -> Result<i32, Box<dyn Error>> {
        let get_idle_time_func = get_callback!(self.ai_id, UnitDef_getIdleTime)?;
        Ok(unsafe { get_idle_time_func(self.ai_id, self.def_id) })
    }

    pub fn power(&self) -> Result<f32, Box<dyn Error>> {
        let get_power_func = get_callback!(self.ai_id, UnitDef_getPower)?;
        Ok(unsafe { get_power_func(self.ai_id, self.def_id) })
    }

    pub fn health(&self) -> Result<f32, Box<dyn Error>> {
        let get_health_func = get_callback!(self.ai_id, UnitDef_getHealth)?;
        Ok(unsafe { get_health_func(self.ai_id, self.def_id) })
    }

    pub fn category_id(&self) -> Result<i32, Box<dyn Error>> {
        let get_category_func = get_callback!(self.ai_id, UnitDef_getCategory)?;
        Ok(unsafe { get_category_func(self.ai_id, self.def_id) })
    }

    pub fn category_string(&self) -> Result<String, Box<dyn Error>> {
        let get_category_string_func = get_callback!(self.ai_id, UnitDef_getCategoryString)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_category_string_func(self.ai_id, self.def_id)) }
                .to_str()?,
        ))
    }

    pub fn mass(&self) -> Result<f32, Box<dyn Error>> {
        let get_mass_func = get_callback!(self.ai_id, UnitDef_getMass)?;
        Ok(unsafe { get_mass_func(self.ai_id, self.def_id) })
    }

    pub fn upright(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_upright_func = get_callback!(self.ai_id, UnitDef_isUpright)?;
        Ok(unsafe { get_is_upright_func(self.ai_id, self.def_id) })
    }

    pub fn collide(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_collide_func = get_callback!(self.ai_id, UnitDef_isCollide)?;
        Ok(unsafe { get_is_collide_func(self.ai_id, self.def_id) })
    }

    pub fn push_resistant(&self) -> Result<bool, Box<dyn Error>> {
        let get_is_push_resistant_func = get_callback!(self.ai_id, UnitDef_isPushResistant)?;
        Ok(unsafe { get_is_push_resistant_func(self.ai_id, self.def_id) })
    }

    pub fn min_collision_speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_min_collision_speed_func = get_callback!(self.ai_id, UnitDef_getMinCollisionSpeed)?;
        Ok(unsafe { get_min_collision_speed_func(self.ai_id, self.def_id) })
    }

    pub fn slide_tolerance(&self) -> Result<f32, Box<dyn Error>> {
        let get_slide_tolerance_func = get_callback!(self.ai_id, UnitDef_getSlideTolerance)?;
        Ok(unsafe { get_slide_tolerance_func(self.ai_id, self.def_id) })
    }

    pub fn waterline(&self) -> Result<f32, Box<dyn Error>> {
        let get_water_line_func = get_callback!(self.ai_id, UnitDef_getWaterline)?;
        Ok(unsafe { get_water_line_func(self.ai_id, self.def_id) })
    }

    pub fn tooltip(&self) -> Result<String, Box<dyn Error>> {
        let get_tooltip_func = get_callback!(self.ai_id, UnitDef_getTooltip)?;
        Ok(String::from(
            unsafe { CStr::from_ptr(get_tooltip_func(self.ai_id, self.def_id)) }.to_str()?,
        ))
    }

    pub fn active_when_built(&self) -> Result<bool, Box<dyn Error>> {
        let active_when_built_func = get_callback!(self.ai_id, UnitDef_isActivateWhenBuilt)?;
        Ok(unsafe { active_when_built_func(self.ai_id, self.def_id) })
    }

    pub fn on_offable(&self) -> Result<bool, Box<dyn Error>> {
        let on_offable_func = get_callback!(self.ai_id, UnitDef_isOnOffable)?;
        Ok(unsafe { on_offable_func(self.ai_id, self.def_id) })
    }

    pub fn full_health_factory(&self) -> Result<bool, Box<dyn Error>> {
        let full_health_factory_func = get_callback!(self.ai_id, UnitDef_isFullHealthFactory)?;
        Ok(unsafe { full_health_factory_func(self.ai_id, self.def_id) })
    }

    pub fn factory_heading_take_off(&self) -> Result<bool, Box<dyn Error>> {
        let factory_heading_take_off_func =
            get_callback!(self.ai_id, UnitDef_isFactoryHeadingTakeoff)?;
        Ok(unsafe { factory_heading_take_off_func(self.ai_id, self.def_id) })
    }

    pub fn reclaimable(&self) -> Result<bool, Box<dyn Error>> {
        let reclaimable_func = get_callback!(self.ai_id, UnitDef_isReclaimable)?;
        Ok(unsafe { reclaimable_func(self.ai_id, self.def_id) })
    }

    pub fn auto_reclaimable(&self) -> Result<bool, Box<dyn Error>> {
        let auto_reclaimable_func = get_callback!(self.ai_id, FeatureDef_isAutoreclaimable)?;
        Ok(unsafe { auto_reclaimable_func(self.ai_id, self.def_id) })
    }

    pub fn capturable(&self) -> Result<bool, Box<dyn Error>> {
        let capturable_func = get_callback!(self.ai_id, UnitDef_isCapturable)?;
        Ok(unsafe { capturable_func(self.ai_id, self.def_id) })
    }

    pub fn x_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_x_size_func = get_callback!(self.ai_id, UnitDef_getXSize)?;
        Ok(unsafe { get_x_size_func(self.ai_id, self.def_id) } * 8)
    }

    pub fn z_size(&self) -> Result<i32, Box<dyn Error>> {
        let get_z_size_func = get_callback!(self.ai_id, UnitDef_getZSize)?;
        Ok(unsafe { get_z_size_func(self.ai_id, self.def_id) } * 8)
    }

    pub fn max_this_unit(&self) -> Result<i32, Box<dyn Error>> {
        let get_max_this_unit_func = get_callback!(self.ai_id, UnitDef_getMaxThisUnit)?;
        Ok(unsafe { get_max_this_unit_func(self.ai_id, self.def_id) })
    }

    pub fn all(&self) -> Result<UnitMiscellaneousAll, Box<dyn Error>> {
        Ok(UnitMiscellaneousAll {
            idle_time: self.idle_time()?,
            power: self.power()?,
            health: self.health()?,
            category_id: self.category_id()?,
            category_string: self.category_string()?,
            mass: self.mass()?,
            upright: self.upright()?,
            collide: self.collide()?,
            push_resistant: self.push_resistant()?,
            min_collision_speed: self.min_collision_speed()?,
            slide_tolerance: self.slide_tolerance()?,
            waterline: self.waterline()?,
            tooltip: self.tooltip()?,
            active_when_built: self.active_when_built()?,
            on_offable: self.on_offable()?,
            full_health_factory: self.full_health_factory()?,
            factory_heading_take_off: self.factory_heading_take_off()?,
            reclaimable: self.reclaimable()?,
            auto_reclaimable: self.auto_reclaimable()?,
            capturable: self.capturable()?,
            x_size: self.x_size()?,
            z_size: self.z_size()?,
            max_this_unit: self.max_this_unit()?,
        })
    }
}
