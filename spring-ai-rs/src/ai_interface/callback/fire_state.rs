use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum FireState {
    HoldFire,
    ReturnFire,
    FireAtWill,
}

impl Into<i32> for FireState {
    fn into(self) -> i32 {
        match self {
            FireState::HoldFire => 0,
            FireState::ReturnFire => 1,
            FireState::FireAtWill => 2,
        }
    }
}

impl Into<FireState> for i32 {
    fn into(self) -> FireState {
        match self {
            0 => FireState::HoldFire,
            1 => FireState::ReturnFire,
            2 => FireState::FireAtWill,
            _ => unimplemented!(),
        }
    }
}
