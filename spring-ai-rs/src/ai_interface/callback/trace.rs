use std::error::Error;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::ai_interface::{
    callback::{
        command::{
            command_data::{
                trace::{FeatureTraceRayCommandData, TraceRayCommandData},
                CommandData,
            },
            command_topic::CommandTopic,
        },
        engine::handle_command,
        feature::Feature,
        unit::Unit,
    },
    AIInterface,
};

pub struct Trace {
    ai_id: i32,
}

impl AIInterface {
    fn trace(&self) -> Trace {
        Trace { ai_id: self.ai_id }
    }
}

impl Trace {
    pub fn ray(
        &self,
        position: [f32; 3],
        direction: [f32; 3],
        length: f32,
        source_unit_id: i32,
        flags: i32,
    ) -> Result<Unit, Box<dyn Error>> {
        let mut command_c_data = TraceRayCommandData {
            ray_position: position,
            ray_direction: direction,
            ray_length: length,
            source_unit_id,
            flags,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::TraceRay.into(),
            &mut command_c_data,
        )?;

        Ok(Unit {
            ai_id: self.ai_id,
            unit_id: command_c_data.ret_hitUnitId,
        })
    }

    pub fn feature_ray(
        &self,
        position: [f32; 3],
        direction: [f32; 3],
        length: f32,
        source_unit_id: i32,
        flags: i32,
    ) -> Result<Feature, Box<dyn Error>> {
        let mut command_c_data = FeatureTraceRayCommandData {
            ray_position: position,
            ray_direction: direction,
            ray_length: length,
            source_unit_id,
            flags,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::TraceRayFeature.into(),
            &mut command_c_data,
        )?;

        Ok(Feature {
            ai_id: self.ai_id,
            feature_id: command_c_data.ret_hitFeatureId,
        })
    }
}
