use std::error::Error;

use spring_ai_sys::COMMAND_TO_ID_ENGINE;

use crate::{
    ai_interface::{
        callback::{
            command::{
                command_data::{
                    cheats::{
                        GiveMeNewUnitCheatCommandData, GiveMeResourceCheatCommandData,
                        SetMyIncomeMultiplierCheatCommandData,
                    },
                    CommandData,
                },
                command_topic::CommandTopic,
            },
            engine::handle_command,
            resource::Resource,
            unit::Unit,
            unit_def::UnitDef,
        },
        AIInterface,
    },
    get_callback,
};

#[derive(Debug, Copy, Clone)]
pub struct Cheat {
    ai_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct CheatAll {
    status: bool,
    will_desync: bool,
}

impl AIInterface {
    pub fn cheat(&self) -> Cheat {
        Cheat { ai_id: self.ai_id }
    }
}

impl Cheat {
    pub fn enable(&self) -> Result<(), Box<dyn Error>> {
        let set_cheat = get_callback!(self.ai_id, Cheats_setEnabled)?;
        unsafe { set_cheat(self.ai_id, true) };
        Ok(())
    }

    pub fn enable_event(&self) -> Result<(), Box<dyn Error>> {
        let set_cheat_event = get_callback!(self.ai_id, Cheats_setEventsEnabled)?;
        unsafe { set_cheat_event(self.ai_id, true) };
        Ok(())
    }

    pub fn disable(&self) -> Result<(), Box<dyn Error>> {
        let set_cheat = get_callback!(self.ai_id, Cheats_setEnabled)?;
        unsafe { set_cheat(self.ai_id, false) };
        Ok(())
    }

    pub fn disable_event(&self) -> Result<(), Box<dyn Error>> {
        let set_cheat_event = get_callback!(self.ai_id, Cheats_setEventsEnabled)?;
        unsafe { set_cheat_event(self.ai_id, false) };
        Ok(())
    }

    pub fn status(&self) -> Result<bool, Box<dyn Error>> {
        let get_cheat = get_callback!(self.ai_id, Cheats_isEnabled)?;
        Ok(unsafe { get_cheat(self.ai_id) })
    }

    pub fn will_desync(&self) -> Result<bool, Box<dyn Error>> {
        let is_passive = get_callback!(self.ai_id, Cheats_isOnlyPassive)?;
        Ok(unsafe { is_passive(self.ai_id) })
    }

    pub fn give_unit(&self, unit_def: UnitDef, position: [f32; 3]) -> Result<Unit, String> {
        let mut command_c_data = GiveMeNewUnitCheatCommandData {
            unit_def_id: unit_def.def_id,
            position,
        }
        .c_data();

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::CheatsGiveMeNewUnit.into(),
            &mut command_c_data,
        )?;

        Ok(Unit {
            ai_id: self.ai_id,
            unit_id: command_c_data.ret_newUnitId,
        })
    }

    pub fn give_resource(&self, resource: Resource, amount: f32) -> Result<(), String> {
        let mut command_data = GiveMeResourceCheatCommandData {
            resource_id: resource.resource_id,
            amount,
        };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::CallLuaRules.into(),
            &mut command_data.c_data(),
        )?;

        Ok(())
    }

    pub fn set_income_multiplier(&self, factor: f32) -> Result<(), String> {
        let mut command_data = SetMyIncomeMultiplierCheatCommandData { factor };

        handle_command(
            self.ai_id,
            COMMAND_TO_ID_ENGINE,
            -1,
            CommandTopic::CallLuaRules.into(),
            &mut command_data.c_data(),
        )?;

        Ok(())
    }

    pub fn all(&self) -> Result<CheatAll, Box<dyn Error>> {
        Ok(CheatAll {
            status: self.status()?,
            will_desync: self.will_desync()?,
        })
    }
}
