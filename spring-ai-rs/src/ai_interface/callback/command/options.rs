use spring_ai_sys::{
    UnitCommandOptions as UnitCommandOptions_Sys, UnitCommandOptions_UNIT_COMMAND_OPTION_ALT_KEY,
    UnitCommandOptions_UNIT_COMMAND_OPTION_CONTROL_KEY,
    UnitCommandOptions_UNIT_COMMAND_OPTION_INTERNAL_ORDER,
    UnitCommandOptions_UNIT_COMMAND_OPTION_RIGHT_MOUSE_KEY,
    UnitCommandOptions_UNIT_COMMAND_OPTION_SHIFT_KEY,
};

#[derive(Copy, Clone, Debug)]
pub enum UnitCommandOptions {
    AltKey,
    ControlKey,
    ShiftKey,
    InternalOrder,
    RightMouseKey,
}

#[allow(non_upper_case_globals)]
impl From<UnitCommandOptions_Sys> for UnitCommandOptions {
    fn from(command_topic_sys: UnitCommandOptions_Sys) -> Self {
        match command_topic_sys {
            UnitCommandOptions_UNIT_COMMAND_OPTION_ALT_KEY => UnitCommandOptions::AltKey,
            UnitCommandOptions_UNIT_COMMAND_OPTION_CONTROL_KEY => UnitCommandOptions::ControlKey,
            UnitCommandOptions_UNIT_COMMAND_OPTION_SHIFT_KEY => UnitCommandOptions::ShiftKey,
            UnitCommandOptions_UNIT_COMMAND_OPTION_INTERNAL_ORDER => {
                UnitCommandOptions::InternalOrder
            }
            UnitCommandOptions_UNIT_COMMAND_OPTION_RIGHT_MOUSE_KEY => {
                UnitCommandOptions::RightMouseKey
            }
            _ => unimplemented!(),
        }
    }
}

#[allow(non_upper_case_globals)]
impl From<UnitCommandOptions> for UnitCommandOptions_Sys {
    fn from(command_topic_sys: UnitCommandOptions) -> Self {
        match command_topic_sys {
            UnitCommandOptions::AltKey => UnitCommandOptions_UNIT_COMMAND_OPTION_ALT_KEY,
            UnitCommandOptions::ControlKey => UnitCommandOptions_UNIT_COMMAND_OPTION_CONTROL_KEY,
            UnitCommandOptions::ShiftKey => UnitCommandOptions_UNIT_COMMAND_OPTION_SHIFT_KEY,
            UnitCommandOptions::InternalOrder => {
                UnitCommandOptions_UNIT_COMMAND_OPTION_INTERNAL_ORDER
            }
            UnitCommandOptions::RightMouseKey => {
                UnitCommandOptions_UNIT_COMMAND_OPTION_RIGHT_MOUSE_KEY
            }
        }
    }
}
