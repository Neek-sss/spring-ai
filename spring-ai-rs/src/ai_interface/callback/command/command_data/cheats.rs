use spring_ai_sys::{
    SGiveMeNewUnitCheatCommand, SGiveMeResourceCheatCommand, SSetMyIncomeMultiplierCheatCommand,
};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Give Unit Cheat data
pub struct GiveMeNewUnitCheatCommandData {
    pub unit_def_id: i32,
    pub position: [f32; 3],
}

impl CommandData for GiveMeNewUnitCheatCommandData {
    type CDataType = SGiveMeNewUnitCheatCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGiveMeNewUnitCheatCommand {
            unitDefId: self.unit_def_id,
            pos_posF3: self.position.as_mut_ptr(),
            ret_newUnitId: 0,
        }
    }
}

impl CData for SGiveMeNewUnitCheatCommand {}

// Give Resource Cheat data
pub struct GiveMeResourceCheatCommandData {
    pub resource_id: i32,
    pub amount: f32,
}

impl CommandData for GiveMeResourceCheatCommandData {
    type CDataType = SGiveMeResourceCheatCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGiveMeResourceCheatCommand {
            resourceId: self.resource_id,
            amount: self.amount,
        }
    }
}

impl CData for SGiveMeResourceCheatCommand {}

// Set Income Multiplier Cheat data
pub struct SetMyIncomeMultiplierCheatCommandData {
    pub factor: f32,
}

impl CommandData for SetMyIncomeMultiplierCheatCommandData {
    type CDataType = SSetMyIncomeMultiplierCheatCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetMyIncomeMultiplierCheatCommand {
            factor: self.factor,
        }
    }
}

impl CData for SSetMyIncomeMultiplierCheatCommand {}
