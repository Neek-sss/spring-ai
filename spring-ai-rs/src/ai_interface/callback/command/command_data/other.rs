use std::ffi::CString;

use spring_ai_sys::{SPauseCommand, SSetLastPosMessageCommand};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Set Last Message Position data
pub struct SetLastPosMessageCommandData {
    pub position: [f32; 3],
}

impl CommandData for SetLastPosMessageCommandData {
    type CDataType = SSetLastPosMessageCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetLastPosMessageCommand {
            pos_posF3: self.position.as_mut_ptr(),
        }
    }
}

impl CData for SSetLastPosMessageCommand {}

// Pause data
pub struct PauseCommandData {
    pub enable: bool,
    pub reason: CString,
}

impl CommandData for PauseCommandData {
    type CDataType = SPauseCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SPauseCommand {
            enable: self.enable,
            reason: self.reason.as_ptr(),
        }
    }
}

impl CData for SPauseCommand {}
