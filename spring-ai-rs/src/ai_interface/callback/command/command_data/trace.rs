use spring_ai_sys::{SFeatureTraceRayCommand, STraceRayCommand};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Trace Ray command data
pub struct TraceRayCommandData {
    pub ray_position: [f32; 3],
    pub ray_direction: [f32; 3],
    pub ray_length: f32,
    pub source_unit_id: i32,
    pub flags: i32,
}

impl CommandData for TraceRayCommandData {
    type CDataType = STraceRayCommand;

    fn c_data(&mut self) -> Self::CDataType {
        STraceRayCommand {
            rayPos_posF3: self.ray_position.as_mut_ptr(),
            rayDir_posF3: self.ray_direction.as_mut_ptr(),
            rayLen: self.ray_length,
            srcUnitId: self.source_unit_id,
            ret_hitUnitId: 0,
            flags: self.flags,
        }
    }
}

impl CData for STraceRayCommand {}

// Feature Trace Ray command data
pub struct FeatureTraceRayCommandData {
    pub ray_position: [f32; 3],
    pub ray_direction: [f32; 3],
    pub ray_length: f32,
    pub source_unit_id: i32,
    pub flags: i32,
}

impl CommandData for FeatureTraceRayCommandData {
    type CDataType = SFeatureTraceRayCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SFeatureTraceRayCommand {
            rayPos_posF3: self.ray_position.as_mut_ptr(),
            rayDir_posF3: self.ray_direction.as_mut_ptr(),
            rayLen: self.ray_length,
            srcUnitId: self.source_unit_id,
            ret_hitFeatureId: 0,
            flags: self.flags,
        }
    }
}

impl CData for SFeatureTraceRayCommand {}
