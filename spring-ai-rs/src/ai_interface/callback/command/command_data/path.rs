use spring_ai_sys::{
    SFreePathCommand, SGetApproximateLengthPathCommand, SGetNextWaypointPathCommand,
    SInitPathCommand,
};

use crate::ai_interface::callback::command::command_data::{CData, CommandData};

// Free Path command data
pub struct FreePathCommandData {
    pub path_id: i32,
}

impl CommandData for FreePathCommandData {
    type CDataType = SFreePathCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SFreePathCommand {
            pathId: self.path_id,
        }
    }
}

impl CData for SFreePathCommand {}

// Path Length command data
pub struct GetApproximateLengthPathCommandData {
    pub start_pos: [f32; 3],
    pub end_pos: [f32; 3],
    pub path_type: i32,
    pub goal_radius: f32,
}

impl CommandData for GetApproximateLengthPathCommandData {
    type CDataType = SGetApproximateLengthPathCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGetApproximateLengthPathCommand {
            start_posF3: self.start_pos.as_mut_ptr(),
            end_posF3: self.end_pos.as_mut_ptr(),
            pathType: self.path_type,
            goalRadius: self.goal_radius,
            ret_approximatePathLength: 0.0,
        }
    }
}

impl CData for SGetApproximateLengthPathCommand {}

// Path Next Waypoint command data
pub struct InitPathCommandData {
    pub start_pos: [f32; 3],
    pub end_pos: [f32; 3],
    pub path_type: i32,
    pub goal_radius: f32,
}

impl CommandData for InitPathCommandData {
    type CDataType = SInitPathCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SInitPathCommand {
            start_posF3: self.start_pos.as_mut_ptr(),
            end_posF3: self.end_pos.as_mut_ptr(),
            pathType: self.path_type,
            goalRadius: self.goal_radius,
            ret_pathId: 0,
        }
    }
}

impl CData for SInitPathCommand {}

// Init Path command data
pub struct GetNextWaypointPathCommandData {
    pub path_id: i32,
    pub next_waypoint_position: [f32; 3],
}

impl CommandData for GetNextWaypointPathCommandData {
    type CDataType = SGetNextWaypointPathCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGetNextWaypointPathCommand {
            pathId: self.path_id,
            ret_nextWaypoint_posF3_out: self.next_waypoint_position.as_mut_ptr(),
        }
    }
}

impl CData for SGetNextWaypointPathCommand {}
