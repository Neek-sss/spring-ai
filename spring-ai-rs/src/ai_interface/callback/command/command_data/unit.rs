use spring_ai_sys::{
    SAiSelectUnitCommand, SAttackAreaUnitCommand, SAttackUnitCommand, SBuildUnitCommand,
    SCaptureAreaUnitCommand, SCaptureUnitCommand, SCloakUnitCommand, SCustomUnitCommand,
    SDGunPosUnitCommand, SDGunUnitCommand, SDeathWaitUnitCommand, SFightUnitCommand,
    SGatherWaitUnitCommand, SGuardUnitCommand, SLoadOntoUnitCommand, SLoadUnitsAreaUnitCommand,
    SLoadUnitsUnitCommand, SMoveUnitCommand, SPatrolUnitCommand, SReclaimAreaUnitCommand,
    SReclaimFeatureUnitCommand, SReclaimUnitUnitCommand, SRepairUnitCommand,
    SRestoreAreaUnitCommand, SResurrectAreaUnitCommand, SResurrectUnitCommand,
    SSelfDestroyUnitCommand, SSetAutoRepairLevelUnitCommand, SSetBaseUnitCommand,
    SSetFireStateUnitCommand, SSetIdleModeUnitCommand, SSetMoveStateUnitCommand,
    SSetOnOffUnitCommand, SSetRepeatUnitCommand, SSetTrajectoryUnitCommand, SSquadWaitUnitCommand,
    SStockpileUnitCommand, SStopUnitCommand, STimeWaitUnitCommand, SUnloadUnitCommand,
    SUnloadUnitsAreaUnitCommand, SWaitUnitCommand, UnitCommandOptions as UnitCommandOptions_Sys,
};

use crate::ai_interface::callback::{
    command::{
        command_data::{CData, CommandData},
        options::UnitCommandOptions,
    },
    facing::Facing,
    fire_state::FireState,
    idle_mode::IdleMode,
    move_state::MoveState,
    trajectory::Trajectory,
    unit::Unit,
};

// AI Select Unit command data
pub struct AiSelectUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for AiSelectUnitCommandData {
    type CDataType = SAiSelectUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAiSelectUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SAiSelectUnitCommand {}

// Unit Attack command data
pub struct AttackUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub target_unit_id: i32,
}

impl CommandData for AttackUnitCommandData {
    type CDataType = SAttackUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAttackUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toAttackUnitId: self.target_unit_id,
        }
    }
}

impl CData for SAttackUnitCommand {}

// Unit Attack Area command data
pub struct AttackAreaUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub attack_position: [f32; 3],
    pub radius: f32,
}

impl CommandData for AttackAreaUnitCommandData {
    type CDataType = SAttackAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAttackAreaUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toAttackPos_posF3: self.attack_position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SAttackAreaUnitCommand {}

// Unit Build command data
pub struct BuildUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_build_unit_def_id: i32,
    pub build_position: [f32; 3],
    pub facing: Facing,
}

impl CommandData for BuildUnitCommandData {
    type CDataType = SBuildUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SBuildUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toBuildUnitDefId: self.to_build_unit_def_id,
            buildPos_posF3: self.build_position.as_mut_ptr(),
            facing: self.facing.into(),
        }
    }
}

impl CData for SBuildUnitCommand {}

// Unit Capture command data
pub struct CaptureUnitCommanddData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_capture_unit_id: i32,
}

impl CommandData for CaptureUnitCommanddData {
    type CDataType = SCaptureUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCaptureUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toCaptureUnitId: self.to_capture_unit_id,
        }
    }
}

impl CData for SCaptureUnitCommand {}

// Unit Capture Area command data
pub struct CaptureAreaUnitCommanddData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub position: [f32; 3],
    pub radius: f32,
}

impl CommandData for CaptureAreaUnitCommanddData {
    type CDataType = SCaptureAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCaptureAreaUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            pos_posF3: self.position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SCaptureAreaUnitCommand {}

// Unit Cloak command data
pub struct CloakUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub cloak: bool,
}

impl CommandData for CloakUnitCommandData {
    type CDataType = SCloakUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCloakUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            cloak: self.cloak,
        }
    }
}

impl CData for SCloakUnitCommand {}

// Unit Custom command data
pub struct CustomUnitCommanddData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub command_id: i32,
    pub parameters: Vec<f32>,
}

impl CommandData for CustomUnitCommanddData {
    type CDataType = SCustomUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCustomUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            cmdId: self.command_id,
            params: self.parameters.as_mut_ptr(),
            params_size: self.parameters.len() as i32,
        }
    }
}

impl CData for SCustomUnitCommand {}

// DGun Unit command data
pub struct DGunUnitCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub toAttackUnitId: i32,
}

impl CommandData for DGunUnitCommandData {
    type CDataType = SDGunUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDGunUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            toAttackUnitId: self.toAttackUnitId,
        }
    }
}

impl CData for SDGunUnitCommand {}

// DGun Position Unit command data
pub struct DGunPosUnitCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub position: [f32; 3],
}

impl CommandData for DGunPosUnitCommandData {
    type CDataType = SDGunPosUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDGunPosUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            pos_posF3: self.position.as_mut_ptr(),
        }
    }
}

impl CData for SDGunPosUnitCommand {}

// Unit Fight command data
pub struct FightUnitCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub position: [f32; 3],
}

impl CommandData for FightUnitCommandData {
    type CDataType = SFightUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SFightUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            toPos_posF3: self.position.as_mut_ptr(),
        }
    }
}

impl CData for SFightUnitCommand {}

// Unit Guard command data
pub struct GuardUnitCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub to_guard_unit_id: i32,
}

impl CommandData for GuardUnitCommandData {
    type CDataType = SGuardUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGuardUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            toGuardUnitId: self.to_guard_unit_id,
        }
    }
}

impl CData for SGuardUnitCommand {}

// Unit Load Onto command data
pub struct LoadOntoUnitCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub transporter_unit_id: i32,
}

impl CommandData for LoadOntoUnitCommandData {
    type CDataType = SLoadOntoUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SLoadOntoUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            transporterUnitId: self.transporter_unit_id,
        }
    }
}

impl CData for SLoadOntoUnitCommand {}

// Unit Load Units command data
pub struct LoadUnitsUnitCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub to_load_unit_ids: Vec<i32>,
}

impl CommandData for LoadUnitsUnitCommandData {
    type CDataType = SLoadUnitsUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SLoadUnitsUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            toLoadUnitIds: self.to_load_unit_ids.as_mut_ptr(),
            toLoadUnitIds_size: self.to_load_unit_ids.len() as i32,
        }
    }
}

impl CData for SLoadUnitsUnitCommand {}

// Unit Load Units in Area command data
pub struct LoadUnitsAreaCommandData {
    pub unitId: i32,
    pub groupId: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeOut: i32,
    pub position: [f32; 3],
    pub radius: f32,
}

impl CommandData for LoadUnitsAreaCommandData {
    type CDataType = SLoadUnitsAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SLoadUnitsAreaUnitCommand {
            unitId: self.unitId,
            groupId: self.groupId,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeOut,
            pos_posF3: self.position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SLoadUnitsAreaUnitCommand {}

// Move Unit command data
pub struct MoveUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_pos: [f32; 3],
}

impl CommandData for MoveUnitCommandData {
    type CDataType = SMoveUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SMoveUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toPos_posF3: self.to_pos.as_mut_ptr(),
        }
    }
}

impl CData for SMoveUnitCommand {}

// Unit Patrol command data
pub struct PatrolUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_pos: [f32; 3],
}

impl CommandData for PatrolUnitCommandData {
    type CDataType = SPatrolUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SPatrolUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toPos_posF3: self.to_pos.as_mut_ptr(),
        }
    }
}

impl CData for SPatrolUnitCommand {}

// Unit Reclaim Area command data
pub struct ReclaimAreaUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub position: [f32; 3],
    pub radius: f32,
}

impl CommandData for ReclaimAreaUnitCommandData {
    type CDataType = SReclaimAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SReclaimAreaUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            pos_posF3: self.position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SReclaimAreaUnitCommand {}

// Unit Reclaim Feature command data
pub struct ReclaimFeatureUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_reclaim_feature_id: i32,
}

impl CommandData for ReclaimFeatureUnitCommandData {
    type CDataType = SReclaimFeatureUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SReclaimFeatureUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toReclaimFeatureId: self.to_reclaim_feature_id,
        }
    }
}

impl CData for SReclaimFeatureUnitCommand {}

// Unit Reclaim Unit command data
pub struct ReclaimUnitUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_reclaim_unit_id: i32,
}

impl CommandData for ReclaimUnitUnitCommandData {
    type CDataType = SReclaimUnitUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SReclaimUnitUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toReclaimUnitId: self.to_reclaim_unit_id,
        }
    }
}

impl CData for SReclaimUnitUnitCommand {}

// Unit Repair Unit command data
pub struct RepairUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_repair_unit_id: i32,
}

impl CommandData for RepairUnitCommandData {
    type CDataType = SRepairUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SRepairUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toRepairUnitId: self.to_repair_unit_id,
        }
    }
}

impl CData for SRepairUnitCommand {}

// Unit Restore Area command data
pub struct RestoreAreaUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub position: [f32; 3],
    pub radius: f32,
}

impl CommandData for RestoreAreaUnitCommandData {
    type CDataType = SRestoreAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SRestoreAreaUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            pos_posF3: self.position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SRestoreAreaUnitCommand {}

// Unit Resurrect command data
pub struct ResurrectUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_resurrect_feature_id: i32,
}

impl CommandData for ResurrectUnitCommandData {
    type CDataType = SResurrectUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SResurrectUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toResurrectFeatureId: self.to_resurrect_feature_id,
        }
    }
}

impl CData for SResurrectUnitCommand {}

// Unit Resurrect Area command data
pub struct ResurrectAreaUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub position: [f32; 3],
    pub radius: f32,
}

impl CommandData for ResurrectAreaUnitCommandData {
    type CDataType = SResurrectAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SResurrectAreaUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            pos_posF3: self.position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SResurrectAreaUnitCommand {}

// Unit Self Destroy command data
pub struct SelfDestroyUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for SelfDestroyUnitCommandData {
    type CDataType = SSelfDestroyUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSelfDestroyUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SSelfDestroyUnitCommand {}

// Unit Set Auto Repair Level command data
pub struct SetAutoRepairLevelUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub auto_repair_level: i32,
}

impl CommandData for SetAutoRepairLevelUnitCommandData {
    type CDataType = SSetAutoRepairLevelUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetAutoRepairLevelUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            autoRepairLevel: self.auto_repair_level,
        }
    }
}

impl CData for SSetAutoRepairLevelUnitCommand {}

// Unit Set Base command data
pub struct SetBaseUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub base_position: [f32; 3],
}

impl CommandData for SetBaseUnitCommandData {
    type CDataType = SSetBaseUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetBaseUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            basePos_posF3: self.base_position.as_mut_ptr(),
        }
    }
}

impl CData for SSetBaseUnitCommand {}

// Unit Set Fire State command data
pub struct SetFireUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub fire_state: FireState,
}

impl CommandData for SetFireUnitCommandData {
    type CDataType = SSetFireStateUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetFireStateUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            fireState: self.fire_state.into(),
        }
    }
}

impl CData for SSetFireStateUnitCommand {}

// Unit Set Idle Mode command data
pub struct SetIdleModeCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub idle_mode: IdleMode,
}

impl CommandData for SetIdleModeCommandData {
    type CDataType = SSetIdleModeUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetIdleModeUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            idleMode: self.idle_mode.into(),
        }
    }
}

impl CData for SSetIdleModeUnitCommand {}

// Unit Set Move State command data
pub struct SetMoveStateCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub move_state: MoveState,
}

impl CommandData for SetMoveStateCommandData {
    type CDataType = SSetMoveStateUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetMoveStateUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            moveState: self.move_state.into(),
        }
    }
}

impl CData for SSetMoveStateUnitCommand {}

// Unit Set On/Off command data
pub struct SetUnitOnOffCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub on: bool,
}

impl CommandData for SetUnitOnOffCommandData {
    type CDataType = SSetOnOffUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetOnOffUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            on: self.on,
        }
    }
}

impl CData for SSetOnOffUnitCommand {}

// Unit Set Repeat command data
pub struct SetUnitRepeatCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub repeat: bool,
}

impl CommandData for SetUnitRepeatCommandData {
    type CDataType = SSetRepeatUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetRepeatUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            repeat: self.repeat,
        }
    }
}

impl CData for SSetRepeatUnitCommand {}

// Unit Set Trajectory command data
pub struct SetUnitTrajectoryCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub trajectory: Trajectory,
}

impl CommandData for SetUnitTrajectoryCommandData {
    type CDataType = SSetTrajectoryUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetTrajectoryUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            trajectory: self.trajectory.into(),
        }
    }
}

impl CData for SSetTrajectoryUnitCommand {}

// Unit Stockpile command data
pub struct SetUnitStockpileCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for SetUnitStockpileCommandData {
    type CDataType = SStockpileUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SStockpileUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SStockpileUnitCommand {}

// Unit Stop command data
pub struct SetUnitStopCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for SetUnitStopCommandData {
    type CDataType = SStopUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SStopUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SStopUnitCommand {}

// Unit Unload command data
pub struct SetUnitUnloadCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub position: [f32; 3],
    pub to_unload_unit_id: i32,
}

impl CommandData for SetUnitUnloadCommandData {
    type CDataType = SUnloadUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SUnloadUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toPos_posF3: self.position.as_mut_ptr(),
            toUnloadUnitId: self.to_unload_unit_id,
        }
    }
}

impl CData for SUnloadUnitCommand {}

// Unit Unload Area command data
pub struct SetUnitUnloadAreaCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub position: [f32; 3],
    pub radius: f32,
}

impl CommandData for SetUnitUnloadAreaCommandData {
    type CDataType = SUnloadUnitsAreaUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SUnloadUnitsAreaUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toPos_posF3: self.position.as_mut_ptr(),
            radius: self.radius,
        }
    }
}

impl CData for SUnloadUnitsAreaUnitCommand {}

// Unit Wait command data
pub struct SetUnitWaitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for SetUnitWaitCommandData {
    type CDataType = SWaitUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SWaitUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SWaitUnitCommand {}

// Unit Wait Death command data
pub struct SetUnitWaitDeathCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_die_unit: Unit,
}

impl CommandData for SetUnitWaitDeathCommandData {
    type CDataType = SDeathWaitUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDeathWaitUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toDieUnitId: 0,
        }
    }
}

impl CData for SDeathWaitUnitCommand {}

// Unit Wait Gather command data
pub struct SetUnitWaitGatherCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for SetUnitWaitGatherCommandData {
    type CDataType = SGatherWaitUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGatherWaitUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SGatherWaitUnitCommand {}

// Unit Wait Squad command data
pub struct SetUnitWaitSquadCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub num_units: i32,
}

impl CommandData for SetUnitWaitSquadCommandData {
    type CDataType = SSquadWaitUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSquadWaitUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            numUnits: self.num_units,
        }
    }
}

impl CData for SSquadWaitUnitCommand {}

// Unit Wait Time command data
pub struct SetUnitWaitTimeCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub time: i32,
}

impl CommandData for SetUnitWaitTimeCommandData {
    type CDataType = STimeWaitUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        STimeWaitUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            time: self.time,
        }
    }
}

impl CData for STimeWaitUnitCommand {}
