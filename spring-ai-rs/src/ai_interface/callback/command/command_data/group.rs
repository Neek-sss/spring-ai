use spring_ai_sys::{
    SCreateGroupCommand, SEraseGroupCommand, SGroupAddUnitCommand, SGroupClearUnitCommand,
    UnitCommandOptions as UnitCommandOptions_Sys,
};

use crate::ai_interface::callback::command::{
    command_data::{CData, CommandData},
    options::UnitCommandOptions,
};

// Group Add Unit data
pub struct GroupAddUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
    pub to_group_id: i32,
}

impl CommandData for GroupAddUnitCommandData {
    type CDataType = SGroupAddUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGroupAddUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
            toGroupId: self.to_group_id,
        }
    }
}

impl CData for SGroupAddUnitCommand {}

// Group Create data
pub struct CreateGroupCommandData {}

impl CommandData for CreateGroupCommandData {
    type CDataType = SCreateGroupCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCreateGroupCommand { ret_groupId: 0 }
    }
}

impl CData for SCreateGroupCommand {}

// Group Erase data
pub struct EraseGroupCommandData {
    pub group_id: i32,
}

impl CommandData for EraseGroupCommandData {
    type CDataType = SEraseGroupCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SEraseGroupCommand {
            groupId: self.group_id,
        }
    }
}

impl CData for SEraseGroupCommand {}

// Group Clear Unit data
pub struct GroupClearUnitCommandData {
    pub unit_id: i32,
    pub group_id: i32,
    pub options: Vec<UnitCommandOptions>,
    pub timeout: i32,
}

impl CommandData for GroupClearUnitCommandData {
    type CDataType = SGroupClearUnitCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SGroupClearUnitCommand {
            unitId: self.unit_id,
            groupId: self.group_id,
            options: self
                .options
                .iter()
                .map(|&uco| UnitCommandOptions_Sys::from(uco))
                .sum::<i32>() as libc::c_short,
            timeOut: self.timeout,
        }
    }
}

impl CData for SGroupClearUnitCommand {}
