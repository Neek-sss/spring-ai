use std::ffi::CString;

use spring_ai_sys::{
    SAddLineDrawCommand, SAddNotificationDrawerCommand, SAddPointDrawCommand,
    SBreakPathDrawerCommand, SCreateLineFigureDrawerCommand, SCreateSplineFigureDrawerCommand,
    SDeleteFigureDrawerCommand, SDrawIconAtLastPosPathDrawerCommand,
    SDrawLineAndIconPathDrawerCommand, SDrawLinePathDrawerCommand, SDrawUnitDrawerCommand,
    SFinishPathDrawerCommand, SRemovePointDrawCommand, SRestartPathDrawerCommand,
    SSetColorFigureDrawerCommand, SStartPathDrawerCommand,
};

use crate::ai_interface::callback::{
    command::command_data::{CData, CommandData},
    facing::Facing,
};

// Drawer Add Notification data
pub struct AddNotificationDrawerCommandData {
    pub position: [f32; 3],
    pub color: [i16; 3],
    pub alpha: i16,
}

impl CommandData for AddNotificationDrawerCommandData {
    type CDataType = SAddNotificationDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAddNotificationDrawerCommand {
            pos_posF3: self.position.as_mut_ptr(),
            color_colorS3: self.color.as_mut_ptr(),
            alpha: self.alpha,
        }
    }
}

impl CData for SAddNotificationDrawerCommand {}

// Drawer Draw Unit data
pub struct DrawUnitDrawerCommandData {
    pub draw_unit_def_id: i32,
    pub position: [f32; 3],
    pub rotation: f32,
    pub lifetime: i32,
    pub team_id: i32,
    pub transparent: bool,
    pub draw_border: bool,
    pub facing: Facing,
}

impl CommandData for DrawUnitDrawerCommandData {
    type CDataType = SDrawUnitDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDrawUnitDrawerCommand {
            toDrawUnitDefId: self.draw_unit_def_id,
            pos_posF3: self.position.as_mut_ptr(),
            rotation: self.rotation,
            lifeTime: self.lifetime,
            teamId: self.team_id,
            transparent: self.transparent,
            drawBorder: self.draw_border,
            facing: self.facing.into(),
        }
    }
}

impl CData for SDrawUnitDrawerCommand {}

// Drawer Figure Create Line data
pub struct CreateLineFigureDrawerCommandData {
    pub position_1: [f32; 3],
    pub position_2: [f32; 3],
    pub width: f32,
    pub arrow: bool,
    pub lifetime: i32,
    pub figure_group_id: i32,
}

impl CommandData for CreateLineFigureDrawerCommandData {
    type CDataType = SCreateLineFigureDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCreateLineFigureDrawerCommand {
            pos1_posF3: self.position_1.as_mut_ptr(),
            pos2_posF3: self.position_2.as_mut_ptr(),
            width: self.width,
            arrow: self.arrow,
            lifeTime: self.lifetime,
            figureGroupId: self.figure_group_id,
            ret_newFigureGroupId: 0,
        }
    }
}

impl CData for SCreateLineFigureDrawerCommand {}

// Drawer Figure Create Spline data
pub struct CreateSplineFigureDrawerCommandData {
    pub position_1: [f32; 3],
    pub position_2: [f32; 3],
    pub position_3: [f32; 3],
    pub position_4: [f32; 3],
    pub width: f32,
    pub arrow: bool,
    pub lifetime: i32,
    pub figure_group_id: i32,
}

impl CommandData for CreateSplineFigureDrawerCommandData {
    type CDataType = SCreateSplineFigureDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SCreateSplineFigureDrawerCommand {
            pos1_posF3: self.position_1.as_mut_ptr(),
            pos2_posF3: self.position_2.as_mut_ptr(),
            pos3_posF3: self.position_3.as_mut_ptr(),
            pos4_posF3: self.position_4.as_mut_ptr(),
            width: self.width,
            arrow: self.arrow,
            lifeTime: self.lifetime,
            figureGroupId: self.figure_group_id,
            ret_newFigureGroupId: 0,
        }
    }
}

impl CData for SCreateSplineFigureDrawerCommand {}

// Drawer Figure Delete data
pub struct DeleteFigureDrawerCommandData {
    pub figure_group_id: i32,
}

impl CommandData for DeleteFigureDrawerCommandData {
    type CDataType = SDeleteFigureDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDeleteFigureDrawerCommand {
            figureGroupId: self.figure_group_id,
        }
    }
}

impl CData for SDeleteFigureDrawerCommand {}

// Drawer Figure Set Color data
pub struct SetColorFigureDrawerCommandData {
    pub figure_group_id: i32,
    pub color: [i16; 3],
    pub alpha: i16,
}

impl CommandData for SetColorFigureDrawerCommandData {
    type CDataType = SSetColorFigureDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SSetColorFigureDrawerCommand {
            figureGroupId: self.figure_group_id,
            color_colorS3: self.color.as_mut_ptr(),
            alpha: self.alpha,
        }
    }
}

impl CData for SSetColorFigureDrawerCommand {}

// Drawer Add Line data
pub struct AddLineDrawCommandData {
    pub to: [f32; 3],
    pub from: [f32; 3],
}

impl CommandData for AddLineDrawCommandData {
    type CDataType = SAddLineDrawCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAddLineDrawCommand {
            posFrom_posF3: self.from.as_mut_ptr(),
            posTo_posF3: self.to.as_mut_ptr(),
        }
    }
}

impl CData for SAddLineDrawCommand {}

// Drawer Break Path data
pub struct BreakPathDrawerCommandData {
    pub end_position: [f32; 3],
    pub color: [i16; 3],
    pub alpha: i16,
}

impl CommandData for BreakPathDrawerCommandData {
    type CDataType = SBreakPathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SBreakPathDrawerCommand {
            endPos_posF3: self.end_position.as_mut_ptr(),
            color_colorS3: self.color.as_mut_ptr(),
            alpha: self.alpha,
        }
    }
}

impl CData for SBreakPathDrawerCommand {}

// Drawer Path Draw Icon at Last Position data
pub struct DrawIconAtLastPosPathDrawerCommandData {
    pub command_id: i32,
}

impl CommandData for DrawIconAtLastPosPathDrawerCommandData {
    type CDataType = SDrawIconAtLastPosPathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDrawIconAtLastPosPathDrawerCommand {
            cmdId: self.command_id,
        }
    }
}

impl CData for SDrawIconAtLastPosPathDrawerCommand {}

// Drawer Draw Path Line data
pub struct DrawLinePathDrawerCommandData {
    pub end_position: [f32; 3],
    pub color: [i16; 3],
    pub alpha: i16,
}

impl CommandData for DrawLinePathDrawerCommandData {
    type CDataType = SDrawLinePathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDrawLinePathDrawerCommand {
            endPos_posF3: self.end_position.as_mut_ptr(),
            color_colorS3: self.color.as_mut_ptr(),
            alpha: self.alpha,
        }
    }
}

impl CData for SDrawLinePathDrawerCommand {}

// Drawer Draw Path Line and Icon data
pub struct DrawLineAndIconPathDrawerCommandData {
    pub command_id: i32,
    pub position: [f32; 3],
    pub color: [i16; 3],
    pub alpha: i16,
}

impl CommandData for DrawLineAndIconPathDrawerCommandData {
    type CDataType = SDrawLineAndIconPathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SDrawLineAndIconPathDrawerCommand {
            cmdId: self.command_id,
            endPos_posF3: self.position.as_mut_ptr(),
            color_colorS3: self.color.as_mut_ptr(),
            alpha: self.alpha,
        }
    }
}

impl CData for SDrawLineAndIconPathDrawerCommand {}

// Drawer Finish Path data
pub struct FinishPathDrawerCommandData {
    pub i_am_useless: bool,
}

impl CommandData for FinishPathDrawerCommandData {
    type CDataType = SFinishPathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SFinishPathDrawerCommand {
            iAmUseless: self.i_am_useless,
        }
    }
}

impl CData for SFinishPathDrawerCommand {}

// Drawer Path Restart data
pub struct RestartPathDrawerCommandData {
    pub same_color: bool,
}

impl CommandData for RestartPathDrawerCommandData {
    type CDataType = SRestartPathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SRestartPathDrawerCommand {
            sameColor: self.same_color,
        }
    }
}

impl CData for SRestartPathDrawerCommand {}

// Drawer Start Path data
pub struct StartPathDrawerCommandData {
    pub position: [f32; 3],
    pub color: [i16; 3],
    pub alpha: i16,
}

impl CommandData for StartPathDrawerCommandData {
    type CDataType = SStartPathDrawerCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SStartPathDrawerCommand {
            pos_posF3: self.position.as_mut_ptr(),
            color_colorS3: self.color.as_mut_ptr(),
            alpha: self.alpha,
        }
    }
}

impl CData for SStartPathDrawerCommand {}

// Drawer Add Point data
pub struct AddPointDrawCommandData {
    pub position: [f32; 3],
    pub label: CString,
}

impl CommandData for AddPointDrawCommandData {
    type CDataType = SAddPointDrawCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SAddPointDrawCommand {
            pos_posF3: self.position.as_mut_ptr(),
            label: self.label.as_ptr(),
        }
    }
}

impl CData for SAddPointDrawCommand {}

// Drawer Remove Point data
pub struct RemovePointDrawCommandData {
    pub position: [f32; 3],
}

impl CommandData for RemovePointDrawCommandData {
    type CDataType = SRemovePointDrawCommand;

    fn c_data(&mut self) -> Self::CDataType {
        SRemovePointDrawCommand {
            pos_posF3: self.position.as_mut_ptr(),
        }
    }
}

impl CData for SRemovePointDrawCommand {}
