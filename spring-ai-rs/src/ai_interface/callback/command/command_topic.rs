use spring_ai_sys::{
    CommandTopic as CommandTopic_Sys, CommandTopic_COMMAND_CALL_LUA_RULES,
    CommandTopic_COMMAND_CALL_LUA_UI, CommandTopic_COMMAND_CHEATS_GIVE_ME_NEW_UNIT,
    CommandTopic_COMMAND_CHEATS_GIVE_ME_RESOURCE,
    CommandTopic_COMMAND_CHEATS_SET_MY_INCOME_MULTIPLIER,
    CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_ADD_POINT,
    CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_DELETE_POINTS,
    CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_COLOR,
    CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_LABEL,
    CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_SET_POS,
    CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_SET_SIZE,
    CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_ADD,
    CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_DELETE,
    CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_LABEL,
    CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_POS,
    CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_SIZE,
    CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_UPDATE,
    CommandTopic_COMMAND_DRAWER_ADD_NOTIFICATION, CommandTopic_COMMAND_DRAWER_DRAW_UNIT,
    CommandTopic_COMMAND_DRAWER_FIGURE_CREATE_LINE,
    CommandTopic_COMMAND_DRAWER_FIGURE_CREATE_SPLINE, CommandTopic_COMMAND_DRAWER_FIGURE_DELETE,
    CommandTopic_COMMAND_DRAWER_FIGURE_SET_COLOR, CommandTopic_COMMAND_DRAWER_LINE_ADD,
    CommandTopic_COMMAND_DRAWER_PATH_BREAK, CommandTopic_COMMAND_DRAWER_PATH_DRAW_ICON_AT_LAST_POS,
    CommandTopic_COMMAND_DRAWER_PATH_DRAW_LINE,
    CommandTopic_COMMAND_DRAWER_PATH_DRAW_LINE_AND_ICON, CommandTopic_COMMAND_DRAWER_PATH_FINISH,
    CommandTopic_COMMAND_DRAWER_PATH_RESTART, CommandTopic_COMMAND_DRAWER_PATH_START,
    CommandTopic_COMMAND_DRAWER_POINT_ADD, CommandTopic_COMMAND_DRAWER_POINT_REMOVE,
    CommandTopic_COMMAND_GROUP_ADD_UNIT, CommandTopic_COMMAND_GROUP_CREATE,
    CommandTopic_COMMAND_GROUP_ERASE, CommandTopic_COMMAND_GROUP_REMOVE_UNIT,
    CommandTopic_COMMAND_NULL, CommandTopic_COMMAND_PATH_FREE,
    CommandTopic_COMMAND_PATH_GET_APPROXIMATE_LENGTH, CommandTopic_COMMAND_PATH_GET_NEXT_WAYPOINT,
    CommandTopic_COMMAND_PATH_INIT, CommandTopic_COMMAND_PAUSE,
    CommandTopic_COMMAND_SEND_RESOURCES, CommandTopic_COMMAND_SEND_START_POS,
    CommandTopic_COMMAND_SEND_TEXT_MESSAGE, CommandTopic_COMMAND_SEND_UNITS,
    CommandTopic_COMMAND_SET_LAST_POS_MESSAGE, CommandTopic_COMMAND_TRACE_RAY,
    CommandTopic_COMMAND_TRACE_RAY_FEATURE, CommandTopic_COMMAND_UNIT_AI_SELECT,
    CommandTopic_COMMAND_UNIT_ATTACK, CommandTopic_COMMAND_UNIT_ATTACK_AREA,
    CommandTopic_COMMAND_UNIT_BUILD, CommandTopic_COMMAND_UNIT_CAPTURE,
    CommandTopic_COMMAND_UNIT_CAPTURE_AREA, CommandTopic_COMMAND_UNIT_CLOAK,
    CommandTopic_COMMAND_UNIT_CUSTOM, CommandTopic_COMMAND_UNIT_D_GUN,
    CommandTopic_COMMAND_UNIT_D_GUN_POS, CommandTopic_COMMAND_UNIT_FIGHT,
    CommandTopic_COMMAND_UNIT_GROUP_ADD, CommandTopic_COMMAND_UNIT_GROUP_CLEAR,
    CommandTopic_COMMAND_UNIT_GUARD, CommandTopic_COMMAND_UNIT_LOAD_ONTO,
    CommandTopic_COMMAND_UNIT_LOAD_UNITS, CommandTopic_COMMAND_UNIT_LOAD_UNITS_AREA,
    CommandTopic_COMMAND_UNIT_MOVE, CommandTopic_COMMAND_UNIT_PATROL,
    CommandTopic_COMMAND_UNIT_RECLAIM_AREA, CommandTopic_COMMAND_UNIT_RECLAIM_FEATURE,
    CommandTopic_COMMAND_UNIT_RECLAIM_UNIT, CommandTopic_COMMAND_UNIT_REPAIR,
    CommandTopic_COMMAND_UNIT_RESTORE_AREA, CommandTopic_COMMAND_UNIT_RESURRECT,
    CommandTopic_COMMAND_UNIT_RESURRECT_AREA, CommandTopic_COMMAND_UNIT_SELF_DESTROY,
    CommandTopic_COMMAND_UNIT_SET_AUTO_REPAIR_LEVEL, CommandTopic_COMMAND_UNIT_SET_BASE,
    CommandTopic_COMMAND_UNIT_SET_FIRE_STATE, CommandTopic_COMMAND_UNIT_SET_IDLE_MODE,
    CommandTopic_COMMAND_UNIT_SET_MOVE_STATE, CommandTopic_COMMAND_UNIT_SET_ON_OFF,
    CommandTopic_COMMAND_UNIT_SET_REPEAT, CommandTopic_COMMAND_UNIT_SET_TRAJECTORY,
    CommandTopic_COMMAND_UNIT_SET_WANTED_MAX_SPEED, CommandTopic_COMMAND_UNIT_STOCKPILE,
    CommandTopic_COMMAND_UNIT_STOP, CommandTopic_COMMAND_UNIT_UNLOAD_UNIT,
    CommandTopic_COMMAND_UNIT_UNLOAD_UNITS_AREA, CommandTopic_COMMAND_UNIT_WAIT,
    CommandTopic_COMMAND_UNIT_WAIT_DEATH, CommandTopic_COMMAND_UNIT_WAIT_GATHER,
    CommandTopic_COMMAND_UNIT_WAIT_SQUAD, CommandTopic_COMMAND_UNIT_WAIT_TIME,
    CommandTopic_COMMAND_UNUSED_0, CommandTopic_COMMAND_UNUSED_1,
};

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum CommandTopic {
    // Call Lua
    CallLuaRules,
    CallLuaUI,

    // Cheats
    CheatsGiveMeNewUnit,
    CheatsGiveMeResource,
    CheatsSetMyIncomeMultiplier,

    // Debug Drawer
    DebugDrawerGraphLineAddPoint,
    DebugDrawerGraphLineDeletePoints,
    DebugDrawerGraphLineSetColor,
    DebugDrawerGraphLineSetLabel,
    DebugDrawerGraphSetPOS,
    DebugDrawerGraphSetSize,
    DebugDrawerOverlaytextureAdd,
    DebugDrawerOverlaytextureDelete,
    DebugDrawerOverlaytextureSetLabel,
    DebugDrawerOverlaytextureSetPOS,
    DebugDrawerOverlaytextureSetSize,
    DebugDrawerOverlaytextureUpdate,

    // Drawer
    DrawerAddNotification,
    DrawerDrawUnit,
    DrawerFigureCreateLine,
    DrawerFigureCreateSpline,
    DrawerFigureDelete,
    DrawerFigureSetColor,
    DrawerLineAdd,
    DrawerPathBreak,
    DrawerPathDrawIconAtLastPOS,
    DrawerPathDrawLine,
    DrawerPathDrawLineAndIcon,
    DrawerPathFinish,
    DrawerPathRestart,
    DrawerPathStart,
    DrawerPointAdd,
    DrawerPointRemove,

    // Group
    GroupAddUnit,    // TODO: Unused
    GroupCreate,     // TODO: Unused
    GroupErase,      // TODO: Unused
    GroupRemoveUnit, // TODO: Unused

    // Path
    PathFree,
    PathGetApproximateLength,
    PathGetNextWaypoint,
    PathInit,

    // Send
    SendResources,
    SendStartPOS,
    SendTextMessage,
    SendUnits,

    // Trace
    TraceRay,
    TraceRayFeature,

    // Unit
    UnitAISelect,
    UnitAttack,
    UnitAttackArea,
    UnitBuild,
    UnitCapture,
    UnitCaptureArea,
    UnitCloak,
    UnitCustom,
    UnitDGun,
    UnitDGunPOS,
    UnitFight,
    UnitGroupAdd,
    UnitGroupClear,
    UnitGuard,
    UnitLoadOnto,
    UnitLoadUnits,
    UnitLoadUnitsArea,
    UnitMove,
    UnitPatrol,
    UnitReclaimArea,
    UnitReclaimFeature,
    UnitReclaimUnit,
    UnitRepair,
    UnitRestoreArea,
    UnitResurrect,
    UnitResurrectArea,
    UnitSelfDestroy,
    UnitSetAutoRepairLevel,
    UnitSetBase,
    UnitSetFireState,
    UnitSetIdleMode,
    UnitSetMoveState,
    UnitSetOnOff,
    UnitSetRepeat,
    UnitSetTrajectory,
    UnitSetWantedMaxSpeed, // TODO: Unused
    UnitStockpile,
    UnitStop,
    UnitUnloadUnit,
    UnitUnloadUnitsArea,
    UnitWait,
    UnitWaitDeath,
    UnitWaitGather,
    UnitWaitSquad,
    UnitWaitTime,

    // Other
    SetLastPOSMessage,
    Pause,
    Null,
    Unused0, // TODO: Unused
    Unused1, // TODO: Unused
}

#[allow(non_upper_case_globals)]
impl From<CommandTopic_Sys> for CommandTopic {
    fn from(command_topic_sys: CommandTopic_Sys) -> Self {
        match command_topic_sys {
            // Call Lua
            CommandTopic_COMMAND_CALL_LUA_RULES => CommandTopic::CallLuaRules,
            CommandTopic_COMMAND_CALL_LUA_UI => CommandTopic::CallLuaUI,
            // Cheats
            CommandTopic_COMMAND_CHEATS_GIVE_ME_NEW_UNIT => CommandTopic::CheatsGiveMeNewUnit,
            CommandTopic_COMMAND_CHEATS_GIVE_ME_RESOURCE => CommandTopic::CheatsGiveMeResource,
            CommandTopic_COMMAND_CHEATS_SET_MY_INCOME_MULTIPLIER => {
                CommandTopic::CheatsSetMyIncomeMultiplier
            }
            // Debug Drawer
            CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_ADD_POINT => {
                CommandTopic::DebugDrawerGraphLineAddPoint
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_DELETE_POINTS => {
                CommandTopic::DebugDrawerGraphLineDeletePoints
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_COLOR => {
                CommandTopic::DebugDrawerGraphLineSetColor
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_LABEL => {
                CommandTopic::DebugDrawerGraphLineSetLabel
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_SET_POS => CommandTopic::DebugDrawerGraphSetPOS,
            CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_SET_SIZE => {
                CommandTopic::DebugDrawerGraphSetSize
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_ADD => {
                CommandTopic::DebugDrawerOverlaytextureAdd
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_DELETE => {
                CommandTopic::DebugDrawerOverlaytextureDelete
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_LABEL => {
                CommandTopic::DebugDrawerOverlaytextureSetLabel
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_POS => {
                CommandTopic::DebugDrawerOverlaytextureSetPOS
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_SIZE => {
                CommandTopic::DebugDrawerOverlaytextureSetSize
            }
            CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_UPDATE => {
                CommandTopic::DebugDrawerOverlaytextureUpdate
            }
            // Drawer
            CommandTopic_COMMAND_DRAWER_ADD_NOTIFICATION => CommandTopic::DrawerAddNotification,
            CommandTopic_COMMAND_DRAWER_DRAW_UNIT => CommandTopic::DrawerDrawUnit,
            CommandTopic_COMMAND_DRAWER_FIGURE_CREATE_LINE => CommandTopic::DrawerFigureCreateLine,
            CommandTopic_COMMAND_DRAWER_FIGURE_CREATE_SPLINE => {
                CommandTopic::DrawerFigureCreateSpline
            }
            CommandTopic_COMMAND_DRAWER_FIGURE_DELETE => CommandTopic::DrawerFigureDelete,
            CommandTopic_COMMAND_DRAWER_FIGURE_SET_COLOR => CommandTopic::DrawerFigureSetColor,
            CommandTopic_COMMAND_DRAWER_LINE_ADD => CommandTopic::DrawerLineAdd,
            CommandTopic_COMMAND_DRAWER_PATH_BREAK => CommandTopic::DrawerPathBreak,
            CommandTopic_COMMAND_DRAWER_PATH_DRAW_ICON_AT_LAST_POS => {
                CommandTopic::DrawerPathDrawIconAtLastPOS
            }
            CommandTopic_COMMAND_DRAWER_PATH_DRAW_LINE => CommandTopic::DrawerPathDrawLine,
            CommandTopic_COMMAND_DRAWER_PATH_DRAW_LINE_AND_ICON => {
                CommandTopic::DrawerPathDrawLineAndIcon
            }
            CommandTopic_COMMAND_DRAWER_PATH_FINISH => CommandTopic::DrawerPathFinish,
            CommandTopic_COMMAND_DRAWER_PATH_RESTART => CommandTopic::DrawerPathRestart,
            CommandTopic_COMMAND_DRAWER_PATH_START => CommandTopic::DrawerPathStart,
            CommandTopic_COMMAND_DRAWER_POINT_ADD => CommandTopic::DrawerPointAdd,
            CommandTopic_COMMAND_DRAWER_POINT_REMOVE => CommandTopic::DrawerPointRemove,
            // Group
            CommandTopic_COMMAND_GROUP_ADD_UNIT => CommandTopic::GroupAddUnit,
            CommandTopic_COMMAND_GROUP_CREATE => CommandTopic::GroupCreate,
            CommandTopic_COMMAND_GROUP_ERASE => CommandTopic::GroupErase,
            CommandTopic_COMMAND_GROUP_REMOVE_UNIT => CommandTopic::GroupRemoveUnit,
            // Path
            CommandTopic_COMMAND_PATH_FREE => CommandTopic::PathFree,
            CommandTopic_COMMAND_PATH_GET_APPROXIMATE_LENGTH => {
                CommandTopic::PathGetApproximateLength
            }
            CommandTopic_COMMAND_PATH_GET_NEXT_WAYPOINT => CommandTopic::PathGetNextWaypoint,
            CommandTopic_COMMAND_PATH_INIT => CommandTopic::PathInit,
            // Send
            CommandTopic_COMMAND_SEND_RESOURCES => CommandTopic::SendResources,
            CommandTopic_COMMAND_SEND_START_POS => CommandTopic::SendStartPOS,
            CommandTopic_COMMAND_SEND_TEXT_MESSAGE => CommandTopic::SendTextMessage,
            CommandTopic_COMMAND_SEND_UNITS => CommandTopic::SendUnits,
            // Trace
            CommandTopic_COMMAND_TRACE_RAY => CommandTopic::TraceRay,
            CommandTopic_COMMAND_TRACE_RAY_FEATURE => CommandTopic::TraceRayFeature,
            // Unit
            CommandTopic_COMMAND_UNIT_AI_SELECT => CommandTopic::UnitAISelect,
            CommandTopic_COMMAND_UNIT_ATTACK => CommandTopic::UnitAttack,
            CommandTopic_COMMAND_UNIT_ATTACK_AREA => CommandTopic::UnitAttackArea,
            CommandTopic_COMMAND_UNIT_BUILD => CommandTopic::UnitBuild,
            CommandTopic_COMMAND_UNIT_CAPTURE => CommandTopic::UnitCapture,
            CommandTopic_COMMAND_UNIT_CAPTURE_AREA => CommandTopic::UnitCaptureArea,
            CommandTopic_COMMAND_UNIT_CLOAK => CommandTopic::UnitCloak,
            CommandTopic_COMMAND_UNIT_CUSTOM => CommandTopic::UnitCustom,
            CommandTopic_COMMAND_UNIT_D_GUN => CommandTopic::UnitDGun,
            CommandTopic_COMMAND_UNIT_D_GUN_POS => CommandTopic::UnitDGunPOS,
            CommandTopic_COMMAND_UNIT_FIGHT => CommandTopic::UnitFight,
            CommandTopic_COMMAND_UNIT_GROUP_ADD => CommandTopic::UnitGroupAdd,
            CommandTopic_COMMAND_UNIT_GROUP_CLEAR => CommandTopic::UnitGroupClear,
            CommandTopic_COMMAND_UNIT_GUARD => CommandTopic::UnitGuard,
            CommandTopic_COMMAND_UNIT_LOAD_ONTO => CommandTopic::UnitLoadOnto,
            CommandTopic_COMMAND_UNIT_LOAD_UNITS => CommandTopic::UnitLoadUnits,
            CommandTopic_COMMAND_UNIT_LOAD_UNITS_AREA => CommandTopic::UnitLoadUnitsArea,
            CommandTopic_COMMAND_UNIT_MOVE => CommandTopic::UnitMove,
            CommandTopic_COMMAND_UNIT_PATROL => CommandTopic::UnitPatrol,
            CommandTopic_COMMAND_UNIT_RECLAIM_AREA => CommandTopic::UnitReclaimArea,
            CommandTopic_COMMAND_UNIT_RECLAIM_FEATURE => CommandTopic::UnitReclaimFeature,
            CommandTopic_COMMAND_UNIT_RECLAIM_UNIT => CommandTopic::UnitReclaimUnit,
            CommandTopic_COMMAND_UNIT_REPAIR => CommandTopic::UnitRepair,
            CommandTopic_COMMAND_UNIT_RESTORE_AREA => CommandTopic::UnitRestoreArea,
            CommandTopic_COMMAND_UNIT_RESURRECT => CommandTopic::UnitResurrect,
            CommandTopic_COMMAND_UNIT_RESURRECT_AREA => CommandTopic::UnitResurrectArea,
            CommandTopic_COMMAND_UNIT_SELF_DESTROY => CommandTopic::UnitSelfDestroy,
            CommandTopic_COMMAND_UNIT_SET_AUTO_REPAIR_LEVEL => CommandTopic::UnitSetAutoRepairLevel,
            CommandTopic_COMMAND_UNIT_SET_BASE => CommandTopic::UnitSetBase,
            CommandTopic_COMMAND_UNIT_SET_FIRE_STATE => CommandTopic::UnitSetFireState,
            CommandTopic_COMMAND_UNIT_SET_IDLE_MODE => CommandTopic::UnitSetIdleMode,
            CommandTopic_COMMAND_UNIT_SET_MOVE_STATE => CommandTopic::UnitSetMoveState,
            CommandTopic_COMMAND_UNIT_SET_ON_OFF => CommandTopic::UnitSetOnOff,
            CommandTopic_COMMAND_UNIT_SET_REPEAT => CommandTopic::UnitSetRepeat,
            CommandTopic_COMMAND_UNIT_SET_TRAJECTORY => CommandTopic::UnitSetTrajectory,
            CommandTopic_COMMAND_UNIT_SET_WANTED_MAX_SPEED => CommandTopic::UnitSetWantedMaxSpeed,
            CommandTopic_COMMAND_UNIT_STOCKPILE => CommandTopic::UnitStockpile,
            CommandTopic_COMMAND_UNIT_STOP => CommandTopic::UnitStop,
            CommandTopic_COMMAND_UNIT_UNLOAD_UNIT => CommandTopic::UnitUnloadUnit,
            CommandTopic_COMMAND_UNIT_UNLOAD_UNITS_AREA => CommandTopic::UnitUnloadUnitsArea,
            CommandTopic_COMMAND_UNIT_WAIT => CommandTopic::UnitWait,
            CommandTopic_COMMAND_UNIT_WAIT_DEATH => CommandTopic::UnitWaitDeath,
            CommandTopic_COMMAND_UNIT_WAIT_GATHER => CommandTopic::UnitWaitGather,
            CommandTopic_COMMAND_UNIT_WAIT_SQUAD => CommandTopic::UnitWaitSquad,
            CommandTopic_COMMAND_UNIT_WAIT_TIME => CommandTopic::UnitWaitTime,
            // Other
            CommandTopic_COMMAND_SET_LAST_POS_MESSAGE => CommandTopic::SetLastPOSMessage,
            CommandTopic_COMMAND_PAUSE => CommandTopic::Pause,
            CommandTopic_COMMAND_NULL => CommandTopic::Null,
            CommandTopic_COMMAND_UNUSED_0 => CommandTopic::Unused0,
            CommandTopic_COMMAND_UNUSED_1 => CommandTopic::Unused1,
            _ => CommandTopic::Null,
        }
    }
}

#[allow(non_upper_case_globals)]
impl From<CommandTopic> for CommandTopic_Sys {
    fn from(command_topic_sys: CommandTopic) -> Self {
        match command_topic_sys {
            // Call Lua
            CommandTopic::CallLuaRules => CommandTopic_COMMAND_CALL_LUA_RULES,
            CommandTopic::CallLuaUI => CommandTopic_COMMAND_CALL_LUA_UI,
            // Cheats
            CommandTopic::CheatsGiveMeNewUnit => CommandTopic_COMMAND_CHEATS_GIVE_ME_NEW_UNIT,
            CommandTopic::CheatsGiveMeResource => CommandTopic_COMMAND_CHEATS_GIVE_ME_RESOURCE,
            CommandTopic::CheatsSetMyIncomeMultiplier => {
                CommandTopic_COMMAND_CHEATS_SET_MY_INCOME_MULTIPLIER
            }
            // Debug Drawer
            CommandTopic::DebugDrawerGraphLineAddPoint => {
                CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_ADD_POINT
            }
            CommandTopic::DebugDrawerGraphLineDeletePoints => {
                CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_DELETE_POINTS
            }
            CommandTopic::DebugDrawerGraphLineSetColor => {
                CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_COLOR
            }
            CommandTopic::DebugDrawerGraphLineSetLabel => {
                CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_LABEL
            }
            CommandTopic::DebugDrawerGraphSetPOS => CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_SET_POS,
            CommandTopic::DebugDrawerGraphSetSize => {
                CommandTopic_COMMAND_DEBUG_DRAWER_GRAPH_SET_SIZE
            }
            CommandTopic::DebugDrawerOverlaytextureAdd => {
                CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_ADD
            }
            CommandTopic::DebugDrawerOverlaytextureDelete => {
                CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_DELETE
            }
            CommandTopic::DebugDrawerOverlaytextureSetLabel => {
                CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_LABEL
            }
            CommandTopic::DebugDrawerOverlaytextureSetPOS => {
                CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_POS
            }
            CommandTopic::DebugDrawerOverlaytextureSetSize => {
                CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_SIZE
            }
            CommandTopic::DebugDrawerOverlaytextureUpdate => {
                CommandTopic_COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_UPDATE
            }
            // Drawer
            CommandTopic::DrawerAddNotification => CommandTopic_COMMAND_DRAWER_ADD_NOTIFICATION,
            CommandTopic::DrawerDrawUnit => CommandTopic_COMMAND_DRAWER_DRAW_UNIT,
            CommandTopic::DrawerFigureCreateLine => CommandTopic_COMMAND_DRAWER_FIGURE_CREATE_LINE,
            CommandTopic::DrawerFigureCreateSpline => {
                CommandTopic_COMMAND_DRAWER_FIGURE_CREATE_SPLINE
            }
            CommandTopic::DrawerFigureDelete => CommandTopic_COMMAND_DRAWER_FIGURE_DELETE,
            CommandTopic::DrawerFigureSetColor => CommandTopic_COMMAND_DRAWER_FIGURE_SET_COLOR,
            CommandTopic::DrawerLineAdd => CommandTopic_COMMAND_DRAWER_LINE_ADD,
            CommandTopic::DrawerPathBreak => CommandTopic_COMMAND_DRAWER_PATH_BREAK,
            CommandTopic::DrawerPathDrawIconAtLastPOS => {
                CommandTopic_COMMAND_DRAWER_PATH_DRAW_ICON_AT_LAST_POS
            }
            CommandTopic::DrawerPathDrawLine => CommandTopic_COMMAND_DRAWER_PATH_DRAW_LINE,
            CommandTopic::DrawerPathDrawLineAndIcon => {
                CommandTopic_COMMAND_DRAWER_PATH_DRAW_LINE_AND_ICON
            }
            CommandTopic::DrawerPathFinish => CommandTopic_COMMAND_DRAWER_PATH_FINISH,
            CommandTopic::DrawerPathRestart => CommandTopic_COMMAND_DRAWER_PATH_RESTART,
            CommandTopic::DrawerPathStart => CommandTopic_COMMAND_DRAWER_PATH_START,
            CommandTopic::DrawerPointAdd => CommandTopic_COMMAND_DRAWER_POINT_ADD,
            CommandTopic::DrawerPointRemove => CommandTopic_COMMAND_DRAWER_POINT_REMOVE,
            // Group
            CommandTopic::GroupAddUnit => CommandTopic_COMMAND_GROUP_ADD_UNIT,
            CommandTopic::GroupCreate => CommandTopic_COMMAND_GROUP_CREATE,
            CommandTopic::GroupErase => CommandTopic_COMMAND_GROUP_ERASE,
            CommandTopic::GroupRemoveUnit => CommandTopic_COMMAND_GROUP_REMOVE_UNIT,
            // Path
            CommandTopic::PathFree => CommandTopic_COMMAND_PATH_FREE,
            CommandTopic::PathGetApproximateLength => {
                CommandTopic_COMMAND_PATH_GET_APPROXIMATE_LENGTH
            }
            CommandTopic::PathGetNextWaypoint => CommandTopic_COMMAND_PATH_GET_NEXT_WAYPOINT,
            CommandTopic::PathInit => CommandTopic_COMMAND_PATH_INIT,
            // Send
            CommandTopic::SendResources => CommandTopic_COMMAND_SEND_RESOURCES,
            CommandTopic::SendStartPOS => CommandTopic_COMMAND_SEND_START_POS,
            CommandTopic::SendTextMessage => CommandTopic_COMMAND_SEND_TEXT_MESSAGE,
            CommandTopic::SendUnits => CommandTopic_COMMAND_SEND_UNITS,
            // Trace
            CommandTopic::TraceRay => CommandTopic_COMMAND_TRACE_RAY,
            CommandTopic::TraceRayFeature => CommandTopic_COMMAND_TRACE_RAY_FEATURE,
            // Unit
            CommandTopic::UnitAISelect => CommandTopic_COMMAND_UNIT_AI_SELECT,
            CommandTopic::UnitAttack => CommandTopic_COMMAND_UNIT_ATTACK,
            CommandTopic::UnitAttackArea => CommandTopic_COMMAND_UNIT_ATTACK_AREA,
            CommandTopic::UnitBuild => CommandTopic_COMMAND_UNIT_BUILD,
            CommandTopic::UnitCapture => CommandTopic_COMMAND_UNIT_CAPTURE,
            CommandTopic::UnitCaptureArea => CommandTopic_COMMAND_UNIT_CAPTURE_AREA,
            CommandTopic::UnitCloak => CommandTopic_COMMAND_UNIT_CLOAK,
            CommandTopic::UnitCustom => CommandTopic_COMMAND_UNIT_CUSTOM,
            CommandTopic::UnitDGun => CommandTopic_COMMAND_UNIT_D_GUN,
            CommandTopic::UnitDGunPOS => CommandTopic_COMMAND_UNIT_D_GUN_POS,
            CommandTopic::UnitFight => CommandTopic_COMMAND_UNIT_FIGHT,
            CommandTopic::UnitGroupAdd => CommandTopic_COMMAND_UNIT_GROUP_ADD,
            CommandTopic::UnitGroupClear => CommandTopic_COMMAND_UNIT_GROUP_CLEAR,
            CommandTopic::UnitGuard => CommandTopic_COMMAND_UNIT_GUARD,
            CommandTopic::UnitLoadOnto => CommandTopic_COMMAND_UNIT_LOAD_ONTO,
            CommandTopic::UnitLoadUnits => CommandTopic_COMMAND_UNIT_LOAD_UNITS,
            CommandTopic::UnitLoadUnitsArea => CommandTopic_COMMAND_UNIT_LOAD_UNITS_AREA,
            CommandTopic::UnitMove => CommandTopic_COMMAND_UNIT_MOVE,
            CommandTopic::UnitPatrol => CommandTopic_COMMAND_UNIT_PATROL,
            CommandTopic::UnitReclaimArea => CommandTopic_COMMAND_UNIT_RECLAIM_AREA,
            CommandTopic::UnitReclaimFeature => CommandTopic_COMMAND_UNIT_RECLAIM_FEATURE,
            CommandTopic::UnitReclaimUnit => CommandTopic_COMMAND_UNIT_RECLAIM_UNIT,
            CommandTopic::UnitRepair => CommandTopic_COMMAND_UNIT_REPAIR,
            CommandTopic::UnitRestoreArea => CommandTopic_COMMAND_UNIT_RESTORE_AREA,
            CommandTopic::UnitResurrect => CommandTopic_COMMAND_UNIT_RESURRECT,
            CommandTopic::UnitResurrectArea => CommandTopic_COMMAND_UNIT_RESURRECT_AREA,
            CommandTopic::UnitSelfDestroy => CommandTopic_COMMAND_UNIT_SELF_DESTROY,
            CommandTopic::UnitSetAutoRepairLevel => CommandTopic_COMMAND_UNIT_SET_AUTO_REPAIR_LEVEL,
            CommandTopic::UnitSetBase => CommandTopic_COMMAND_UNIT_SET_BASE,
            CommandTopic::UnitSetFireState => CommandTopic_COMMAND_UNIT_SET_FIRE_STATE,
            CommandTopic::UnitSetIdleMode => CommandTopic_COMMAND_UNIT_SET_IDLE_MODE,
            CommandTopic::UnitSetMoveState => CommandTopic_COMMAND_UNIT_SET_MOVE_STATE,
            CommandTopic::UnitSetOnOff => CommandTopic_COMMAND_UNIT_SET_ON_OFF,
            CommandTopic::UnitSetRepeat => CommandTopic_COMMAND_UNIT_SET_REPEAT,
            CommandTopic::UnitSetTrajectory => CommandTopic_COMMAND_UNIT_SET_TRAJECTORY,
            CommandTopic::UnitSetWantedMaxSpeed => CommandTopic_COMMAND_UNIT_SET_WANTED_MAX_SPEED,
            CommandTopic::UnitStockpile => CommandTopic_COMMAND_UNIT_STOCKPILE,
            CommandTopic::UnitStop => CommandTopic_COMMAND_UNIT_STOP,
            CommandTopic::UnitUnloadUnit => CommandTopic_COMMAND_UNIT_UNLOAD_UNIT,
            CommandTopic::UnitWait => CommandTopic_COMMAND_UNIT_WAIT,
            CommandTopic::UnitWaitDeath => CommandTopic_COMMAND_UNIT_WAIT_DEATH,
            CommandTopic::UnitWaitGather => CommandTopic_COMMAND_UNIT_WAIT_GATHER,
            CommandTopic::UnitWaitSquad => CommandTopic_COMMAND_UNIT_WAIT_SQUAD,
            CommandTopic::UnitWaitTime => CommandTopic_COMMAND_UNIT_WAIT_TIME,
            CommandTopic::UnitUnloadUnitsArea => CommandTopic_COMMAND_UNIT_UNLOAD_UNITS_AREA,
            // Other
            CommandTopic::SetLastPOSMessage => CommandTopic_COMMAND_SET_LAST_POS_MESSAGE,
            CommandTopic::Pause => CommandTopic_COMMAND_PAUSE,
            CommandTopic::Null => CommandTopic_COMMAND_NULL,
            CommandTopic::Unused0 => CommandTopic_COMMAND_UNUSED_0,
            CommandTopic::Unused1 => CommandTopic_COMMAND_UNUSED_1,
        }
    }
}
