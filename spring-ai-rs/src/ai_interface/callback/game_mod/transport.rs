use std::error::Error;

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct GameModTransport {
    pub ai_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct GameModTransportAll {
    ground: bool,
    hover: bool,
    ship: bool,
    air: bool,
}

impl GameModTransport {
    pub fn ground(&self) -> Result<bool, Box<dyn Error>> {
        let get_transport_ground = get_callback!(self.ai_id, Mod_getTransportGround)?;

        Ok(unsafe { get_transport_ground(self.ai_id) } == 1)
    }

    pub fn hover(&self) -> Result<bool, Box<dyn Error>> {
        let get_transport_hover = get_callback!(self.ai_id, Mod_getTransportHover)?;

        Ok(unsafe { get_transport_hover(self.ai_id) } == 1)
    }

    pub fn ship(&self) -> Result<bool, Box<dyn Error>> {
        let get_transport_ship = get_callback!(self.ai_id, Mod_getTransportShip)?;

        Ok(unsafe { get_transport_ship(self.ai_id) } == 1)
    }

    pub fn air(&self) -> Result<bool, Box<dyn Error>> {
        let get_transport_air = get_callback!(self.ai_id, Mod_getTransportAir)?;

        Ok(unsafe { get_transport_air(self.ai_id) } == 1)
    }

    pub fn all(&self) -> Result<GameModTransportAll, Box<dyn Error>> {
        Ok(GameModTransportAll {
            ground: self.ground()?,
            hover: self.hover()?,
            ship: self.ship()?,
            air: self.air()?,
        })
    }
}
