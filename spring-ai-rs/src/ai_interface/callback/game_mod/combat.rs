use std::error::Error;

use crate::{ai_interface::callback::unit_def::flanking::FlankingMode, get_callback};

#[derive(Debug, Copy, Clone)]
pub struct GameModCombat {
    pub ai_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct GameModCombatAll {
    fire_at_killed: bool,
    fire_at_crashing: bool,
    flanking_bonus_mode_default: FlankingMode,
}

impl GameModCombat {
    pub fn fire_at_killed(&self) -> Result<bool, Box<dyn Error>> {
        let get_fire_at_killed = get_callback!(self.ai_id, Mod_getFireAtKilled)?;

        Ok(unsafe { get_fire_at_killed(self.ai_id) } == 1)
    }

    pub fn fire_at_crashing(&self) -> Result<bool, Box<dyn Error>> {
        let get_fire_at_crashing = get_callback!(self.ai_id, Mod_getFireAtCrashing)?;

        Ok(unsafe { get_fire_at_crashing(self.ai_id) } == 1)
    }

    pub fn flanking_bonus_mode_default(&self) -> Result<FlankingMode, Box<dyn Error>> {
        let get_fire_at_crashing = get_callback!(self.ai_id, Mod_getFireAtCrashing)?;

        Ok(FlankingMode::from(unsafe {
            get_fire_at_crashing(self.ai_id)
        }))
    }

    pub fn all(&self) -> Result<GameModCombatAll, Box<dyn Error>> {
        Ok(GameModCombatAll {
            fire_at_killed: self.fire_at_killed()?,
            fire_at_crashing: self.fire_at_crashing()?,
            flanking_bonus_mode_default: self.flanking_bonus_mode_default()?,
        })
    }
}
