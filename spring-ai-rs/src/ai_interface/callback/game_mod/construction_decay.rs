use std::error::Error;

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct GameModConstructionDecay {
    pub ai_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct GameModConstructionDecayAll {
    is_enabled: bool,
    delay: i32,
    speed: f32,
}

impl GameModConstructionDecay {
    pub fn is_enabled(&self) -> Result<bool, Box<dyn Error>> {
        let get_construction_decay = get_callback!(self.ai_id, Mod_getConstructionDecay)?;

        Ok(unsafe { get_construction_decay(self.ai_id) })
    }

    pub fn delay(&self) -> Result<i32, Box<dyn Error>> {
        let get_construction_decay_time = get_callback!(self.ai_id, Mod_getConstructionDecayTime)?;

        Ok(unsafe { get_construction_decay_time(self.ai_id) })
    }

    pub fn speed(&self) -> Result<f32, Box<dyn Error>> {
        let get_construction_decay_speed =
            get_callback!(self.ai_id, Mod_getConstructionDecaySpeed)?;

        Ok(unsafe { get_construction_decay_speed(self.ai_id) })
    }

    pub fn all(&self) -> Result<GameModConstructionDecayAll, Box<dyn Error>> {
        Ok(GameModConstructionDecayAll {
            is_enabled: self.is_enabled()?,
            delay: self.delay()?,
            speed: self.speed()?,
        })
    }
}
