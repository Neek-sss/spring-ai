use std::{error::Error, ffi::CStr};

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct GameModInfo {
    pub ai_id: i32,
}

#[derive(Debug, Clone)]
pub struct GameModInfoAll {
    file_name: String,
    hash: i32,
    human_name: String,
    short_name: String,
    version: String,
    mutator: String,
    description: String,
}

impl GameModInfo {
    pub fn file_name(&self) -> Result<String, Box<dyn Error>> {
        let get_file_name = get_callback!(self.ai_id, Mod_getFileName)?;

        Ok(String::from(
            unsafe { CStr::from_ptr(get_file_name(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn hash(&self) -> Result<i32, Box<dyn Error>> {
        let get_hash = get_callback!(self.ai_id, Mod_getHash)?;

        Ok(unsafe { get_hash(self.ai_id) })
    }

    pub fn human_name(&self) -> Result<String, Box<dyn Error>> {
        let get_human_name = get_callback!(self.ai_id, Mod_getHumanName)?;

        Ok(String::from(
            unsafe { CStr::from_ptr(get_human_name(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn short_name(&self) -> Result<String, Box<dyn Error>> {
        let get_short_name = get_callback!(self.ai_id, Mod_getShortName)?;

        Ok(String::from(
            unsafe { CStr::from_ptr(get_short_name(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn version(&self) -> Result<String, Box<dyn Error>> {
        let get_version = get_callback!(self.ai_id, Mod_getVersion)?;

        Ok(String::from(
            unsafe { CStr::from_ptr(get_version(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn mutator(&self) -> Result<String, Box<dyn Error>> {
        let get_mutator = get_callback!(self.ai_id, Mod_getMutator)?;

        Ok(String::from(
            unsafe { CStr::from_ptr(get_mutator(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn description(&self) -> Result<String, Box<dyn Error>> {
        let get_description = get_callback!(self.ai_id, Mod_getDescription)?;

        Ok(String::from(
            unsafe { CStr::from_ptr(get_description(self.ai_id)) }.to_str()?,
        ))
    }

    pub fn all(&self) -> Result<GameModInfoAll, Box<dyn Error>> {
        Ok(GameModInfoAll {
            file_name: self.file_name()?,
            hash: self.hash()?,
            human_name: self.human_name()?,
            short_name: self.short_name()?,
            version: self.version()?,
            mutator: self.mutator()?,
            description: self.description()?,
        })
    }
}
