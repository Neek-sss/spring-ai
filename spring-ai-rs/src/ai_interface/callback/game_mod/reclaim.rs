use std::error::Error;

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct GameModReclaim {
    pub ai_id: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct GameModReclaimAll {
    multi_reclaim: i32,
    method: ReclaimMethod,
    unit_method: ReclaimUnitMethod,
    unit_energy_cost_factor: f32,
    efficiency: f32,
    feature_energy_cost_factor: f32,
    allow_enemies: bool,
    allow_allies: bool,
}

#[derive(Debug, Copy, Clone)]
pub enum ReclaimMethod {
    Gradual,
    AtEnd,
    Chunk(i32),
}

#[derive(Debug, Copy, Clone)]
pub enum ReclaimUnitMethod {
    WireframeGradualReclaim,
    SubtractHPMetalAtEnd,
}

impl GameModReclaim {
    pub fn multi_reclaim(&self) -> Result<i32, Box<dyn Error>> {
        let get_multi_reclaim = get_callback!(self.ai_id, Mod_getMultiReclaim)?;

        Ok(unsafe { get_multi_reclaim(self.ai_id) })
    }

    pub fn method(&self) -> Result<ReclaimMethod, Box<dyn Error>> {
        let get_reclaim_method = get_callback!(self.ai_id, Mod_getReclaimMethod)?;

        let reclaim_method = unsafe { get_reclaim_method(self.ai_id) };

        Ok(match reclaim_method {
            0 => ReclaimMethod::Gradual,
            1 => ReclaimMethod::AtEnd,
            c => ReclaimMethod::Chunk(c),
        })
    }

    pub fn unit_method(&self) -> Result<ReclaimUnitMethod, Box<dyn Error>> {
        let get_reclaim_unit_method = get_callback!(self.ai_id, Mod_getReclaimUnitMethod)?;

        let reclaim_unit_method = unsafe { get_reclaim_unit_method(self.ai_id) };

        Ok(match reclaim_unit_method {
            0 => ReclaimUnitMethod::WireframeGradualReclaim,
            _ => ReclaimUnitMethod::SubtractHPMetalAtEnd,
        })
    }

    pub fn unit_energy_cost_factor(&self) -> Result<f32, Box<dyn Error>> {
        let get_reclaim_energy_cost_factor =
            get_callback!(self.ai_id, Mod_getReclaimUnitEnergyCostFactor)?;

        Ok(unsafe { get_reclaim_energy_cost_factor(self.ai_id) })
    }

    pub fn efficiency(&self) -> Result<f32, Box<dyn Error>> {
        let get_reclaim_energy_cost_factor =
            get_callback!(self.ai_id, Mod_getReclaimUnitEfficiency)?;

        Ok(unsafe { get_reclaim_energy_cost_factor(self.ai_id) })
    }

    pub fn feature_energy_cost_factor(&self) -> Result<f32, Box<dyn Error>> {
        let get_reclaim_feature_energy_cost_factor =
            get_callback!(self.ai_id, Mod_getReclaimFeatureEnergyCostFactor)?;

        Ok(unsafe { get_reclaim_feature_energy_cost_factor(self.ai_id) })
    }

    pub fn allow_enemies(&self) -> Result<bool, Box<dyn Error>> {
        let get_reclaim_allow_enemies = get_callback!(self.ai_id, Mod_getReclaimAllowEnemies)?;

        Ok(unsafe { get_reclaim_allow_enemies(self.ai_id) })
    }

    pub fn allow_allies(&self) -> Result<bool, Box<dyn Error>> {
        let get_reclaim_allow_allies = get_callback!(self.ai_id, Mod_getReclaimAllowAllies)?;

        Ok(unsafe { get_reclaim_allow_allies(self.ai_id) })
    }

    pub fn all(&self) -> Result<GameModReclaimAll, Box<dyn Error>> {
        Ok(GameModReclaimAll {
            multi_reclaim: self.multi_reclaim()?,
            method: self.method()?,
            unit_method: self.unit_method()?,
            unit_energy_cost_factor: self.unit_energy_cost_factor()?,
            efficiency: self.efficiency()?,
            feature_energy_cost_factor: self.feature_energy_cost_factor()?,
            allow_enemies: self.allow_enemies()?,
            allow_allies: self.allow_allies()?,
        })
    }
}
