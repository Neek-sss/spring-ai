use std::error::Error;

use crate::get_callback;

#[derive(Debug, Copy, Clone)]
pub struct GameModMipLevel {
    pub ai_id: i32,
}

#[derive(Debug, Clone)]
pub struct GameModMipLevelAll {
    los: i32,
    air: i32,
    radar: i32,
}

impl GameModMipLevel {
    pub fn los(&self) -> Result<i32, Box<dyn Error>> {
        let get_los_level = get_callback!(self.ai_id, Mod_getLosMipLevel)?;

        Ok(unsafe { get_los_level(self.ai_id) })
    }

    pub fn air(&self) -> Result<i32, Box<dyn Error>> {
        let get_air_level = get_callback!(self.ai_id, Mod_getAirMipLevel)?;

        Ok(unsafe { get_air_level(self.ai_id) })
    }

    pub fn radar(&self) -> Result<i32, Box<dyn Error>> {
        let get_radar_level = get_callback!(self.ai_id, Mod_getRadarMipLevel)?;

        Ok(unsafe { get_radar_level(self.ai_id) })
    }

    pub fn all(&self) -> Result<GameModMipLevelAll, Box<dyn Error>> {
        Ok(GameModMipLevelAll {
            los: self.los()?,
            air: self.air()?,
            radar: self.radar()?,
        })
    }
}
