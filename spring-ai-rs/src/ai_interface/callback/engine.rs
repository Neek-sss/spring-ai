use std::{error::Error, ffi::CStr};

use libc::{c_int, c_void};

use crate::{
    ai_interface::{callback::command::command_data::CData, AIInterface},
    get_callback,
};

pub fn handle_command<CD: CData>(
    ai_id: c_int,
    to_id: c_int,
    command_id: c_int,
    command_topic: c_int,
    command_data: &mut CD,
) -> Result<(), &'static str> {
    let handle_command = get_callback!(ai_id, Engine_handleCommand)?;

    let ret = unsafe {
        handle_command(
            ai_id,
            to_id,
            command_id,
            command_topic,
            command_data.c_void(),
        )
    };

    if ret == 0 {
        Ok(())
    } else {
        Err("Failed to run the command")
    }
}

pub fn execute_command(
    ai_id: c_int,
    unit_id: c_int,
    group_id: c_int,
    command_data: *mut c_void,
) -> Result<(), &'static str> {
    let execute_command = get_callback!(ai_id, Engine_executeCommand)?;

    if unsafe { execute_command(ai_id, unit_id, group_id, command_data) } != 0 {
        Err("Failed to run the command")
    } else {
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Version {
    major: String,
    minor: String,
    patch: String,
    commits: String,
    hash: String,
    branch: String,
    additional: String,
}

impl AIInterface {
    pub fn engine_version(&self) -> Result<Version, Box<dyn Error>> {
        let version_major = get_callback!(self.ai_id, Engine_Version_getMajor)?;
        let version_minor = get_callback!(self.ai_id, Engine_Version_getMinor)?;
        let version_patch = get_callback!(self.ai_id, Engine_Version_getPatchset)?;
        let version_commits = get_callback!(self.ai_id, Engine_Version_getCommits)?;
        let version_hash = get_callback!(self.ai_id, Engine_Version_getHash)?;
        let version_branch = get_callback!(self.ai_id, Engine_Version_getBranch)?;
        let version_additional = get_callback!(self.ai_id, Engine_Version_getAdditional)?;

        Ok(Version {
            major: String::from(unsafe { CStr::from_ptr(version_major(self.ai_id)) }.to_str()?),
            minor: String::from(unsafe { CStr::from_ptr(version_minor(self.ai_id)) }.to_str()?),
            patch: String::from(unsafe { CStr::from_ptr(version_patch(self.ai_id)) }.to_str()?),
            commits: String::from(unsafe { CStr::from_ptr(version_commits(self.ai_id)) }.to_str()?),
            hash: String::from(unsafe { CStr::from_ptr(version_hash(self.ai_id)) }.to_str()?),
            branch: String::from(unsafe { CStr::from_ptr(version_branch(self.ai_id)) }.to_str()?),
            additional: String::from(
                unsafe { CStr::from_ptr(version_additional(self.ai_id)) }.to_str()?,
            ),
        })
    }
}
