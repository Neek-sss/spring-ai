#![allow(unused_variables, non_snake_case, dead_code)]

pub use inventory;

pub extern crate libc;
pub extern crate slog;
pub extern crate spring_ai_sys;
pub use std::error::Error;

pub use spring_ai_sys::{
    SCommandFinishedEvent, SEnemyCreatedEvent, SEnemyDamagedEvent, SEnemyDestroyedEvent,
    SEnemyEnterLOSEvent, SEnemyEnterRadarEvent, SEnemyFinishedEvent, SEnemyLeaveLOSEvent,
    SEnemyLeaveRadarEvent, SLoadEvent, SLuaMessageEvent, SPlayerCommandEvent, SSaveEvent,
    SSeismicPingEvent, SSkirmishAICallback, SUnitCapturedEvent, SUnitCreatedEvent,
    SUnitDamagedEvent, SUnitDestroyedEvent, SUnitFinishedEvent, SUnitGivenEvent, SUnitIdleEvent,
    SUnitMoveFailedEvent, SUpdateEvent, SWeaponFiredEvent,
};

pub mod ai_interface;
pub mod ai_logger;
pub mod ai_macro;
pub mod event;
pub mod skirmish_ai;
