pub use spring_ai_rs_macro::ai_func;

#[macro_export]
macro_rules! activate_ai {
    () => {
        #[no_mangle]
        pub extern "C" fn handleEvent(
            skirmishAIId: spring_ai_rs::libc::c_int,
            topic: spring_ai_rs::spring_ai_sys::EventTopic,
            data: *const spring_ai_rs::libc::c_void,
        ) -> spring_ai_rs::libc::c_int {
            use spring_ai_rs::slog::Drain;

            let logger = spring_ai_rs::slog::Logger::root(
                spring_ai_rs::ai_logger::AILogger::new(skirmishAIId)
                    .with_level(spring_ai_rs::slog::Level::Trace)
                    .with_file_info(true)
                    .fuse(),
                slog::o!(),
            );

            let f = || -> Result<(), Box<dyn spring_ai_rs::Error>> {
                match spring_ai_rs::event::event_topic::EventTopic::from(topic) {
                    spring_ai_rs::event::event_topic::EventTopic::Init => {
                        for init_wrapper in
                            spring_ai_rs::inventory::iter::<spring_ai_rs::event::other::InitWrapper>
                        {
                            spring_ai_rs::event::other::init_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &init_wrapper.init_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::Release => {
                        for release_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::ReleaseWrapper,
                        > {
                            spring_ai_rs::event::other::release_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &release_wrapper.release_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::Message => {
                        for message_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::MessageWrapper,
                        > {
                            spring_ai_rs::event::other::message_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &message_wrapper.message_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::CommandFinished => {
                        for command_finished_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::CommandFinishedWrapper,
                        > {
                            spring_ai_rs::event::other::command_finished_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &command_finished_wrapper.command_finished_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyCreated => {
                        for enemy_created_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyCreatedWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_created_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_created_wrapper.enemy_created_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyDamaged => {
                        for enemy_damaged_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyDamagedWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_damaged_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_damaged_wrapper.enemy_damaged_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyDestroyed => {
                        for enemy_destroyed_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyDestroyedWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_destroyed_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_destroyed_wrapper.enemy_destroyed_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyEnterLOS => {
                        for enemy_enter_los_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyEnterLOSWrapper,
                        > {
                            let enemy_enter_los_data =
                                spring_ai_rs::event::enemy::enemy_enter_los_wrapper(
                                    &logger,
                                    skirmishAIId,
                                    data,
                                    &enemy_enter_los_wrapper.enemy_enter_los_func,
                                )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyEnterRadar => {
                        for enemy_enter_radar_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyEnterRadarWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_enter_radar_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_enter_radar_wrapper.enemy_enter_radar_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyFinished => {
                        for enemy_finished_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyFinishedWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_finished_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_finished_wrapper.enemy_finished_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyLeaveLOS => {
                        for enemy_leave_los_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyLeaveLOSWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_leave_los_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_leave_los_wrapper.enemy_leave_los_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::EnemyLeaveRadar => {
                        for enemy_leave_radar_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::enemy::EnemyLeaveRadarWrapper,
                        > {
                            spring_ai_rs::event::enemy::enemy_leave_radar_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &enemy_leave_radar_wrapper.enemy_leave_radar_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::Load => {
                        for load_wrapper in
                            spring_ai_rs::inventory::iter::<spring_ai_rs::event::other::LoadWrapper>
                        {
                            spring_ai_rs::event::other::load_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &load_wrapper.load_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::LuaMessage => {
                        for lua_message_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::LuaMessageWrapper,
                        > {
                            spring_ai_rs::event::other::lua_message_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &lua_message_wrapper.lua_message_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::Null => {
                        for null_wrapper in
                            spring_ai_rs::inventory::iter::<spring_ai_rs::event::other::NullWrapper>
                        {
                            spring_ai_rs::event::other::null_wrapper(
                                &logger,
                                skirmishAIId,
                                &null_wrapper.null_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::PlayerCommand => {
                        for player_command_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::PlayerCommandWrapper,
                        > {
                            spring_ai_rs::event::other::player_command_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &player_command_wrapper.player_command_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::Save => {
                        for save_wrapper in
                            spring_ai_rs::inventory::iter::<spring_ai_rs::event::other::SaveWrapper>
                        {
                            spring_ai_rs::event::other::save_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &save_wrapper.save_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::SeismicPing => {
                        for seismic_ping_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::SeismicPingWrapper,
                        > {
                            spring_ai_rs::event::other::seismic_ping_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &seismic_ping_wrapper.seismic_ping_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitCaptured => {
                        for unit_captured_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitCapturedWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_captured_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_captured_wrapper.unit_captured_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitCreated => {
                        for unit_created_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitCreatedWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_created_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_created_wrapper.unit_created_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitDamaged => {
                        for unit_damaged_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitDamagedWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_damaged_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_damaged_wrapper.unit_damaged_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitDestroyed => {
                        for unit_destroyed_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitDestroyedWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_destroyed_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_destroyed_wrapper.unit_destroyed_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitFinished => {
                        for unit_finished_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitFinishedWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_finished_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_finished_wrapper.unit_finished_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitGiven => {
                        for unit_given_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitGivenWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_given_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_given_wrapper.unit_given_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitIdle => {
                        for unit_idle_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitIdleWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_idle_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_idle_wrapper.unit_idle_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::UnitMoveFailed => {
                        for unit_move_failed_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::unit::UnitMoveFailedWrapper,
                        > {
                            spring_ai_rs::event::unit::unit_move_failed_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &unit_move_failed_wrapper.unit_move_failed_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::Update => {
                        for update_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::UpdateWrapper,
                        > {
                            spring_ai_rs::event::other::update_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &update_wrapper.update_func,
                            )?;
                        }
                    }
                    spring_ai_rs::event::event_topic::EventTopic::WeaponFired => {
                        for weapon_fired_wrapper in spring_ai_rs::inventory::iter::<
                            spring_ai_rs::event::other::WeaponFiredWrapper,
                        > {
                            spring_ai_rs::event::other::weapon_fired_wrapper(
                                &logger,
                                skirmishAIId,
                                data,
                                &weapon_fired_wrapper.weapon_fired_func,
                            )?;
                        }
                    }
                    _ => {
                        //
                    }
                }
                Ok(())
            };

            let ret = f();
            if ret.is_ok() {
                0
            } else {
                spring_ai_rs::slog::error!(logger, "{:?}", ret);
                -1
            }
        }
    };
}
